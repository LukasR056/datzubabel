\section{Steuerung}

Damit ein Turm mit mehreren Objekten automatisch gebaut werden kann, müssen diverse Voraussetzungen erfüllt werden.
\begin{itemize}
    \item Der Bereich, indem der Turm gebaut werden soll, der in Betracht der Reichweite der Schrittmotoren eingegrenzt ist, muss bekannt sein.
    \item Die Reihenfolge der Objekte in der sie gestapelt werden sollen und deren Mittelpunkte müssen übermittelt werden.
    \item Der Status der Endschalter der Schrittmotoren bzw. die Anzahl der Schritte im Allgemeinen müssen überwacht werden.
    \item Das Bauen des Turms muss als Abfolge einzelner, einfacher Teilaktivitäten (wie etwa das Bewegen einer Achse, das Aktivieren des Magneten, etc.) beschrieben werden können.
    \item Der Arduino muss gezielt Befehle entgegennehmen können, um oben genannte Aktivitäten umsetzen zu können.
\end{itemize}

\subsection{Konzepte}
Für die Umsetzung dieser Voraussetzungen wurden folgende Konzepte in der Programmiersprache Python implementiert.
\\

\paragraph{API zum Arduino}
Die Schnittstelle zum Arduino wurde auf der Steuerungsebene als serielle Verbindung realisiert. Das definierte Schnittstellenprotokoll wurde umgesetzt, sodass alle Aktoren als Objekt einer Geräte-Klasse repräsentiert werden, wobei jedes Gerät spezielle Funktionen besitzt. Die Statusmeldungen werden ebenfalls von der \emph{API} abgefangen und bearbeitet.

\paragraph{Kalibrierung}
Die Kalibrierung ist notwendig um das Spielfeld (der Bereich in dem der Turm gebaut werden kann) einzumessen. Das passiert indem jeder Schrittmotor, ausgehend von der Nullposition, die Endposition anfährt.
Die Schritte zwischen der Nullposition und der Endposition werden ausgewertet und persistiert. Die Nullposition befindet sich in der Ecke, in der der Schrittmotor für die x-Achse positioniert ist (siehe Abbildung \ref{fig:hardware_xy_konzept}). Für die z-Achse ist der Nullpunkt am Boden.

\paragraph{Turmbauprozess}
Damit eine bestimmte wiederkehrende Abfolge von Aktivitäten umgesetzt werden kann, wurden folgende Aktionen definiert und implementiert.
\begin{itemize}
    \item \emph{MoveXY} (Anfahren bestimmter Position in x- und y-Richtung)
    \item \emph{PickObject} (Objekt aufheben)
    \item \emph{Measure} (Höhe des Objektes messen)
    \item \emph{StackObject} (Objekt auf den bestehenden Turm stapeln)
\end{itemize}

Jeder dieser Aktionen beinhaltet weiters eigene Tasks, die erfüllt werden müssen, damit diese Aktionen abgeschlossen werden kann.
Ein Task für die Aktion \emph{MoveXY} ist zum Beispiel das Erreichen der x-Position. Das Ausrichten des Magnets wäre ein Task für die Aktion \emph{PickObject}.
\\
Für einen konkreten Turmbauprozess werden nun diese Aktionen in bestimmter Reihenfolge aufgrund der Informationen aus den Daten der Objekte ausgeführt.
Die Mittelpunkte der Objekte werden als relative Werte vom Nullpunkt übergeben und können daher einfach angesteuert werden.


\subsection{Python-Klassen}
Damit Modularität gewährleistet werden kann, wurden die meisten Codesegmente mit Klassen umgesetzt.
In der Abbildung~\ref{fig:python_classes} sind die wichtigsten Klassen zu sehen, deren Aufgaben in weiterer Folge genauer erklärt werden.

\begin{figure}[h]
    \centering
    \captionsetup{justification=centering}
    \includegraphics[width=0.6\textwidth]{images/important_classes.drawio.png}
    \caption{\label{fig:python_classes} Die wichtigsten Klassen für die Implementierung der Logik zum Turmbau.}
\end{figure}

\paragraph{ArduinoApi}
Die Klasse \emph{ArduinoApi} stellt die Schnittstelle zum Arduino dar.
Es wird hier die serielle Schnittstelle zum Arduino aufgebaut über die einerseits Befehle abgesetzt 
und andererseits Statusmeldungen über die \emph{callback}-Funktion entgegengenommen werden.
Die Callback-Funktion filtert die Statusmeldungen und verarbeitet diese abhängig von der Art.
Damit mehrere Aktoren in einem Befehl angesteuert werden können wurde ein Transaktionshandling implementiert.
Mit diesem Transaktionshandling wird weiters gewährleistet, dass keine Meldungen verloren gehen.
Durch die Ausführung der \emph{execute}-Funktion wird für die jeweilige Transaktion das Kommando zusammengebaut und über die serielle Schnittstelle an den Arduino übertragen.


\paragraph{Device, Stepmotor, Magnet}
Die Klasse \emph{Device} repräsentiert eine generische Klasse für die Aktoren. 
Die spezifischen Aktoren sind als eigene Klassen (\emph{StepMotor} und \emph{Magnet}) implementiert und erben von der \emph{Device} Klasse.
In den jeweiligen spezifischen Klassen wurden alle Funktionen umgesetzt, die für die direkt Steuerung der Aktoren notwendig sind.
So gibt es zum Beispiel die Funktionen \emph{run\_to\_max} oder \emph{move\_steps}, um den Schrittmotor zu bewegen.
Damit im konkreten Fall zum Beispiel der Motor der x-Achse angesteuert wird, muss zuerst eine neue Instanz der Klasse \emph{StepMotor} mit der Bezeichnung \emph{m2} erzeugt werden.
Mit dieser Instanz kann dann in weiterer Folge weitergearbeitet werden und über das Transaktionshandling die x-Achse auf die Nullposition gesteuert werden, wie im folgenden Codebeispiel zu sehen ist.

\begin{lstlisting}[language=python]
    tid = self.get_arduino1().get_next_transaction_id()
    self.motor_x.run_to_min(tid)
    self.get_arduino1().execute(tid)
\end{lstlisting}

\paragraph{Gamepad}
Für die manuelle Steuerung mit einem Gamepad wurde die Klasse \emph{Gamepad} erstellt.
In dieser Klasse sind die Funktionen der Tastenbelegungen und die Funktionen des Joysticks umgesetzt.

\paragraph{PlayField}
Die \emph{Playfield}-Klasse ist das Herzstück der Steuerung, da dort die eigentliche Logik implementiert wurde.
Alle direkten Abhängigkeiten, die für den Turmbau bzw. der Kalibrierung des Spielfeldes benötigt werden, sind hier enthalten.
So werden der Klasse die Daten der verfügbaren Objekte bereitgestellt und für den Turmbau-Prozess verarbeitet.
Mit der inkludierten \emph{Polling}-Funktion wird das Event-Handling durchgeführt, 
wo einerseits der Status der Aktoren überwacht wird und andererseits Events für den Turmbau bzw. der Kalibrierung ausgelöst werden.
Außerdem liefert die \emph{Playfield}-Klasse die Informationen, die für die grafische Benutzeroberfläche benötigt werden.








