\setlength{\parindent}{0pt}

\section{Arduino}
\label{arduino}
Für die Ansteuerung der elektrischen Bauteile, die in dieser Projektarbeit verwendet wurden, ist ein \emph{Arduino Uno R3} im Einsatz.
Der Arduino Uno wird mit einer operativen Spannung von 5 Volt betrieben und hat 14 digitale \glqq I/O\grqq{}-Pins, wovon 6 davon über eine Pulsweitenmodulation (PWM) gesteuert werden.

Die Schrittmotoren, die für die Bewegung der Achsen verwendet werden, sind über zwei \emph{Adafruit Motor Shield V2} angebunden.
Das Motor Shield verwendet hierbei die PWM-Ports. 
Damit die digitalen \glqq I/O\grqq{}-Pins direkt über Steckverbindungen angesteuert werden können, wurde das \emph{Base Shield} von \emph{Grove} verwendet.
Die erwähnten Shields sind auch in der Abbildung \ref{fig:arduino_board} ersichtlich.

\subsection{Bauteile und Shields}

In der Tabelle \ref{tab:arduino_components} sind alle elektrische Bauteile und deren Funktionen aufgeschlüsselt, die für die Funktionsweise des Portalkrans notwendig sind. 
\begin{table}[h]
    \begin{tabular}{|l|l|l|}
    \hline
    \multicolumn{1}{|c|}{\textbf{Bauteil}} & \multicolumn{1}{c|}{\textbf{Bezeichnung}} & \multicolumn{1}{c|}{\textbf{Funktion}} \\ \hline
    Schrittmotor NEMA14-01                 & m1                                        & Steuerung der z-Achse                             \\ \hline
    Schrittmotor NEMA14-01                 & m2                                        & Steuerung der x-Achse                             \\ \hline
    Schrittmotor NEMA14-01                 & m3                                        & Steuerung der y-Achse                             \\ \hline
    Grove Elektromagnet                    & mg1                                       & Anheben und Absenken der Objekte in z-Richtung    \\ \hline
    Taster                                 & ms1 min                                   & Endschalter für die Nullposition der z-Achse      \\ \hline
    Taster                                 & ms1 max                                   & Endschalter für die Endposition der z-Achse       \\ \hline
    Taster                                 & ms2 min                                   & Endschalter für die Nullposition der x-Achse      \\ \hline
    Taster                                 & ms2 max                                   & Endschalter für die Endposition der x-Achse       \\ \hline
    Taster                                 & ms3 min                                   & Endschalter für die Nullposition der y-Achse      \\ \hline
    Taster                                 & ms3 max                                   & Endschalter für die Endposition der y-Achse       \\ \hline
    \end{tabular}
    \caption{Verwendete Elektronik-Komponenten.}
    \label{tab:arduino_components} 
\end{table}


Diese Bauteile sind entweder am \emph{Motor Shield} bzw. am \emph{Grove Shield} über Steckverbindungen angeschlossen. Die Shields sind weiters direkt mit dem \emph{Arduino Uno} verbunden. Dadurch können alle diese Bauteile über den Arduino angesteuert bzw. deren Signale eingelesen werden. In der Abbildung \ref{fig:arduino_board} sind alle Bestandteile und deren Zusammenhang in grafischer Form dargestellt. Die Pin- bzw. Anschluss-Belegung der Bauteile am Arduino ist in detaillierter Form der Tabelle \ref{tab:arduino_connections} zu entnehmen.

\begin{figure}[h]
    \centering
    \captionsetup{justification=centering}
    \includegraphics[width=\textwidth]{images/arduino_board.png}
    \caption{\label{fig:arduino_board} Grafische Darstellung der Anschlussbelegung aller Bestandteile via \emph{fritzing}.}
\end{figure}


\begin{table}[h]
    \begin{tabular}{|l|l|}
    \hline
    \multicolumn{1}{|c|}{\textbf{Bauteilbezeichnung}} & \multicolumn{1}{c|}{\textbf{Anschluss}} \\ \hline
    m1                                     & Adafruit Motorshield 0x60 M1/M2         \\ \hline
    m2                                     & Adafruit Motorshield 0x61 M1/M2         \\ \hline
    m3                                     & Adafruit Motorshield 0x61 M3/M4         \\ \hline
    mg1                                    & digitaler Ausgang D7                    \\ \hline
    ms1 min                                & digitaler Eingang D2                    \\ \hline
    ms1 max                                & digitaler Eingang D3                    \\ \hline
    ms2 min                                & digitaler Eingang D8                    \\ \hline
    ms2 max                                & digitaler Eingang D6                    \\ \hline
    ms3 min                                & digitaler Eingang D5                    \\ \hline
    ms3 max                                & digitaler Eingang D4                    \\ \hline
    \end{tabular}
    \caption{Anschlussbelegung Arduino.}
    \label{tab:arduino_connections}
\end{table}

\subsection{Software und Bibliotheken}

Die Programmierung des Arduinos erfolgt über eine eigene IDE und basiert auf die Programmiersprache C/C++.
Für komplexere Aktoren, wie zum Beispiel Schrittmotoren werden Bibliotheken für die Arduino-Programmierung zu Verfügung gestellt.
Die Bibliotheken, die für dieses Projekt zusätzlich benutzt wurden, können in der Tabelle \ref{tab:arduino_libs} nachgelesen werden.

\begin{table}[h]
    \begin{tabular}{|l|l|l|}
    \hline
    \multicolumn{1}{|c|}{\textbf{Bibliothek}} & \multicolumn{1}{c|}{\textbf{Anwendung}} & \multicolumn{1}{c|}{\textbf{Version}} \\ \hline
    Adafruit Motor Shield V2 Library          & Anbindung Schrittmotoren                & 1.0.11                                \\ \hline
    \end{tabular}
    \caption{Verwendete Arduino Libraries.}
    \label{tab:arduino_libs}
\end{table}

Auf dem Arduino befindet sich keine aufwendige Programmlogik und dieser besitzt keinerlei Informationen und Zustände über den Turmbauprozess. 
Das Arduino-Programm ist nur darauf ausgelegt Anweisungen entgegenzunehmen, diese zu verarbeiten und an die Aktoren weiterzugeben.
Die Logik der Motorendschalter ist jedoch direkt im Arduino-Programm integriert, damit ein sofortiges Stoppen der Motoren möglich ist. 
Etwaige Informationen, wie der Status der Endschalter oder die zurückgelegten Schritte der Motoren, werden über definierte Statusmeldungen zurückgemeldet.

\subsection{Schnittstelle}
Für die Befehlsweitergabe von bestimmen Aktionen wurde eine Schnittstelle zum Arduino spezifiziert.
Mit diesen definierten Kommandos wird eine Abkapselung und eine Abgrenzung zwischen den Turmbauprozess und die Steuerung der Aktoren ermöglicht. 

Um ein Kommando identifizieren zu können wird am Anfang einer Zeichenkette das Zeichen ``\emph{<}'' und am Ende das Zeichen ``\emph{>}'' übertragen.
Der erste Parameter innerhalb dieser Zeichenfolge gilt als Indikator einer Aktion, die ausgeführt werden soll. 
Der zweite Parameter gibt an welcher Aktor diese Aktion durchgeführt werden soll.
Die Parameter drei und vier sind nicht bei jedem Kommando definiert. 
Bei bestimmten Kommandos sind diese jedoch für eine Parametrisierung notwendig bzw. optional.
Ein Beispiel-Kommando würde wie folgt ausschauen. 
\begin{lstlisting}[language=bash]
    <SPEED m2 5>
\end{lstlisting}


Mit dieser Schnittstelle ist es auch möglich mehrere Aktoren in einem Kommando gleichzeitig anzusprechen.
Die Syntax hierbei ist so definiert, dass der erste Parameter fix ist und die weiteren mit dem zweiten Parameter iterieren.
Wie zum Beispiel:
\begin{lstlisting}[language=bash]
    <RUN m1 -1 2 m2 1 6>
\end{lstlisting}

In der Tabelle~\ref{tab:arduino_interface} sind alle Aktionen mit den zugehörigen Parametern beschrieben.

Für Meldungen die vom Arduino gesendet werden ist ebenfalls ein vordefiniertes Protokoll vorhanden.
Jede Statusmeldung die nach \glqq außen\grqq{} gelangt beginnt mit dem Wortlaut ``\emph{STATUS}'', gefolgt von der Bezeichnung des Aktors.
Für den dritten Parameter ist die Art des Status reserviert, die angibt um welchen Status es sich handelt. 
Am Ende und als letzter Paramater wird der aktuelle Wert des Status angegeben.
Ein Beispiel dieser Statusmeldung ist folgender String, der über die serielle Schnittstelle übertragen wird.
\begin{lstlisting}[language=bash]
    <STATUS m2 position-reached 345>
\end{lstlisting}

Dieses Beispiel sagt aus, dass der Schrittmotor \emph{m2} gerade 345 Schritte zurückgelegt hat.
In der Tabelle~\ref{tab:arduino_status} sind alle Statusarten aufgegliedert und beschrieben.

\begin{table}[]
    \begin{tabular}{|l|l|l|l|l|}
    \hline
    \multicolumn{1}{|c|}{\textbf{Aktion}} & \multicolumn{1}{c|}{\textbf{Parameter 1}}                          & \multicolumn{1}{c|}{\textbf{Parameter 2}}                                                                                                  & \multicolumn{1}{c|}{\textbf{Parameter 3}}                                   & \multicolumn{1}{c|}{\textbf{Beschreibung}}                                                                                \\ \hline
    \textit{RUN}                          & \begin{tabular}[c]{@{}l@{}}Schrittmotor\\ m1, m2, m3\end{tabular}  & \begin{tabular}[c]{@{}l@{}}Laufrichtung \\ -1: rückwärts \\ 1: vorwärts\end{tabular}                   & \begin{tabular}[c]{@{}l@{}}Geschwindigkeit\\ $[1-6]$(optional)\end{tabular} & \begin{tabular}[c]{@{}l@{}}Starten des Motors \\ (läuft solange bis\\  er gestoppt wird)\end{tabular}                     \\ \hline
    \textit{STOP}                         & \begin{tabular}[c]{@{}l@{}}Schrittmotor\\ m1, m2, m3\end{tabular}  &                                                                                                                                            &                                                                             & Stoppen des Motors                                                                                                        \\ \hline
    \textit{SPEED}                        & \begin{tabular}[c]{@{}l@{}}Schrittmotor\\ m1, m2, m3\end{tabular}  & \begin{tabular}[c]{@{}l@{}}Geschwindigkeit\\ $[1-6]$\end{tabular}                                                                          &                                                                             & \begin{tabular}[c]{@{}l@{}}Parametrisierung der \\ Geschwindigkeit \\ des Motors\end{tabular}                             \\ \hline
    \textit{POS}                          & \begin{tabular}[c]{@{}l@{}}Schrittmotor\\ m1, m2, m3\end{tabular}  & \begin{tabular}[c]{@{}l@{}}Schrittweite\\ als Integer\\ (mit einer negative\\ Schrittweite \\ wird die Richtung \\ umgedreht)\end{tabular} & \begin{tabular}[c]{@{}l@{}}Geschwindigkeit\\ $[1-6]$(optional)\end{tabular} & \begin{tabular}[c]{@{}l@{}}Der Motor läuft so \\ lange bis die \\ angegebene Schrittweite \\ erreicht wurde.\end{tabular} \\ \hline
    \textit{MAGON}                        & \begin{tabular}[c]{@{}l@{}}Magnet\\ mg1\end{tabular}               &                                                                                                                                            &                                                                             & Magnet einschalten                                                                                                        \\ \hline
    \textit{MAGOFF}                       & \begin{tabular}[c]{@{}l@{}}Magnet\\ mg1\end{tabular}               &                                                                                                                                            &                                                                             & Magnet ausschalten                                                                                                        \\ \hline
    \textit{CALIBRATION}                  & \begin{tabular}[c]{@{}l@{}}Schrittmotor\\ m1, m2, m3\end{tabular} &                                                                                                                                            &                                                                             & \begin{tabular}[c]{@{}l@{}}Kalibrierungsmodus der \\ Motoren \\ (zusätzliche \\ Statusmeldungen)\end{tabular}             \\ \hline
    \end{tabular}
    \caption{Definierte Befehle im Arduino-Protokoll.}
    \label{tab:arduino_interface}
    \end{table}


\begin{table}[]
    \begin{tabular}{|l|l|l|l|}
    \hline
    \textbf{Statusart}                & \textbf{Aktor} & \textbf{Parameter}                                                                                                                                                                                                 & \textbf{Beschreibung}                                                                                                       \\ \hline
    \textit{position-reached}         & m1, m2, m3     & \begin{tabular}[c]{@{}l@{}}Schrittweite\\ als Integer\\ negative Werte sind \\ Schritte zur Nullposition;\\ positive Werte sind \\ Schritte zur Endposition;\end{tabular}                                                & \begin{tabular}[c]{@{}l@{}}liefert die aktuelle \\ Schrittweite  zurück, \\ die der Motor \\ zurückgelegt hat.\end{tabular} \\ \hline
    \textit{motor-state}              & m1, m2, m3     & \begin{tabular}[c]{@{}l@{}}Motorstatus\\ 1: Motor bewegt sich \\ Richtung Endposition\\ 0: Motor steht still \\ -1: Motor bewegt sich \\ Richtung Nullposition\end{tabular} & \begin{tabular}[c]{@{}l@{}}liefert den aktuellen \\ Status des Motors \\ zurück.\end{tabular}                               \\ \hline
    \textit{calibration-done}         & m1, m2, m3     & \begin{tabular}[c]{@{}l@{}}Schrittweite\\ als Integer\end{tabular}                                                                                                                                                     & \begin{tabular}[c]{@{}l@{}}liefert die maximale \\ Schrittweite des \\ Motors zurück\end{tabular}                           \\ \hline
    \textit{min-motor-switch-changed} & m1, m2, m3     & \begin{tabular}[c]{@{}l@{}}Endschalterstatus\\ 0: Endschalter ist \\ nicht gedrückt\\ 1: Endschalter ist \\ gedrückt\end{tabular}                                                        & \begin{tabular}[c]{@{}l@{}}liefert den Status der \\ Endschalter auf der \\ Nullposition zurück\end{tabular}                \\ \hline
    \textit{max-motor-switch-changed} & m1, m2, m3     & \begin{tabular}[c]{@{}l@{}}Endschalterstatus\\ 0: Endschalter ist \\ nicht gedrückt\\ 1: Endschalter ist \\ gedrückt\end{tabular}                                                        & \begin{tabular}[c]{@{}l@{}}liefert den Status \\ der Endschalter \\ auf der Endposition \\ zurück\end{tabular}              \\ \hline
    \textit{magnet-state}             & mg1            & \begin{tabular}[c]{@{}l@{}}Magnetstatus\\ 0: Magnet ist nicht aktiv\\ 1: Magnet ist aktiv\end{tabular}                                                                                   & \begin{tabular}[c]{@{}l@{}}liefert den Status des \\ Magnets zurück\end{tabular}                                            \\ \hline
    \end{tabular}
    \caption{Definierte Statusmeldungen im Arduino-Protokoll.}
    \label{tab:arduino_status}
\end{table}

