from XInput import *

from Unet.unet_class import Unet
from Utilities.utilitites import JsonWriteReader
from Arduino.arduino_api import StepMotor, Magnet, Laser, ArduinoUno, arduino_callback
# import Arduino Enums
from Arduino.arduino_api import StepMotorLabel
from Gamepad.gamepad import GamePadHandler
from PlayField.play_field import PlayField
# yolov5
from ObjectDetection.yolo5 import Yolo5
# Necessary Packages for GUI
from GUI.gui import Gui
from GUI.main_window import UiMainWindow
from GUI.camera import Camera
from PyQt5 import QtWidgets
import sys
import ctypes
from Utilities.logger import LoggerManager, Logger

log = LoggerManager.getLogger(Logger.DefaultLogger)


def main():
    # create own app id to show custom taskbar icon instead of Pythonw.exe standard icon
    my_app_id = u'mycompany.myproduct.subproduct.version' # arbitrary unicode string
    ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(my_app_id)

    arduino1_com_port, arduino1_baudrate, arduino1_sleep_time, play_field_sleep_time = None, 0, 0, 0
    json_read_writer = JsonWriteReader("Configuration.json")
    if json_read_writer.json_exists():
        content = json_read_writer.read_json()
        arduino1_com_port = content["Arduino1"]["com-port"]
        arduino1_baudrate = content["Arduino1"]["baudrate"]
        arduino1_sleep_time = content["Arduino1"]["poll-sleep-time"]
        play_field_sleep_time = content["PlayField"]["poll-sleep-time"]
    else:
        log.error("Configuration File does not exists!")
        exit()

    # initialize used devices
    motor_x = StepMotor("m2", label=StepMotorLabel.X_AXIS)
    motor_y = StepMotor("m3", label=StepMotorLabel.Y_AXIS)
    motor_z = StepMotor("m1", label=StepMotorLabel.Z_AXIS)
    mg1 = Magnet("mg1")

    # initialize Arduino tread
    arduino1 = ArduinoUno(port=arduino1_com_port, baudrate=arduino1_baudrate, sleep_time=arduino1_sleep_time)
    arduino1.register_callback(arduino_callback)

    motor_x.set_current_speed(6)
    motor_y.set_current_speed(6)
    motor_z.set_current_speed(6)

    # link devices to arduino
    arduino1.add_device(motor_x)
    arduino1.add_device(motor_y)
    arduino1.add_device(mg1)
    arduino1.add_device(motor_z)

    # initialize yolov5 model
    yolo_model = Yolo5("best_pro_500.pt")

    # initialize UNet model
    unet_model = Unet(weight="multiclass_unet_04.hdf5")

    # initialize camera
    cam = Camera(yolo_model=yolo_model, unet_model=unet_model)

    # initialize GUI (before gamepad initialization to transfer gui)
    app = QtWidgets.QApplication(sys.argv)
    app.setStyle("Fusion")
    gui = Gui(camera=cam, yolo_model=yolo_model, unet_model=unet_model)

    # add gui to stepmotor instances
    motor_x.gui = gui
    motor_y.gui = gui
    motor_z.gui = gui

    # initialize playfield and add it to gui
    play_field = PlayField(arduinos=[arduino1], sleep_time=play_field_sleep_time, gui=gui)
    setattr(gui, "play_field", play_field)

    # initialize xbox gamepad thread
    button_handler = GamePadHandler(0, play_field=play_field)
    xbox_filter = STICK_LEFT + BUTTON_START + BUTTON_A + BUTTON_B + BUTTON_X + BUTTON_Y + BUTTON_LEFT_SHOULDER + BUTTON_RIGHT_SHOULDER + BUTTON_BACK
    button_handler.set_filter(xbox_filter)
    my_gamepad_thread = GamepadThread(button_handler)

    log.warning("APP is running")
    # start GUI
    sys.exit(app.exec_())


if __name__ == '__main__':
    log.info("Application started")
    main()
