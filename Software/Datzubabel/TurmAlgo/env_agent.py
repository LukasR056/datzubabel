import math
import os
import random
import numpy as np
from shapely.geometry import Polygon
from shapely.geometry.point import Point
from shapely import affinity
import matplotlib.pyplot as plt
import geopandas as gpd
from Utilities.utilitites import JsonWriteReader
from Utilities.logger import LoggerManager, Logger
import matplotlib as mpl

os.environ["KMP_DUPLICATE_LIB_OK"] = "TRUE"
mpl.use("Qt5Agg")

log = LoggerManager.getLogger(Logger.DefaultLogger)
json_read_writer = JsonWriteReader("Configuration.json")

fig, ax = plt.subplots()

if json_read_writer.json_exists():
    content = json_read_writer.read_json()
    x_max = content["PlayField"]["max-x-steps"]
    y_max = content["PlayField"]["max-y-steps"]
else:
    log.error("Configuration File does not exists!")


class Shape:
    """
    Class for creating and manipulating the shapes/objects that can be used to create a tower.
    """

    def __init__(self, x_max, y_max, ax=None, level=None):
        self.lower_left_x = None
        self.lower_left_y = None
        self.x_max = x_max
        self.y_max = y_max
        self.poly_df = None
        self.ax = ax
        self.level = level
        self.frame_iteration = 0
        self.old_distance = x_max
        self.score = 0

    def reset(self):
        self.move_shape(-1000, -1000)
        self.score = 0
        self.old_distance = x_max
        self.frame_iteration = 0

    def draw_polygon(self, points, rotation_angle):
        error = False

        for point in points:
            if point[0] < 0 or point[0] > self.x_max or point[1] < 0 or point[1] > self.y_max:
                log.error("Point out of bounds")
                error = True
                break

        if error is False:
            poly = Polygon(points)
            poly = affinity.rotate(poly, rotation_angle, "center")
            for point in list(poly.exterior.coords):
                if point[0] < 0 or point[0] > self.x_max or point[1] < 0 or point[1] > self.y_max:
                    log.warning("Rotation was reverted, because points were out of bounds")
                    error = True
                    break
            if error:
                poly = affinity.rotate(poly, -rotation_angle, "center")

        self.poly_df = gpd.GeoSeries(poly)

    def draw_circle(self, center, radius):
        c = Point(center)
        circle = c.buffer(radius)
        self.draw_polygon(list(circle.exterior.coords), 0)

    def is_collision(self):
        if float(self.poly_df.bounds["minx"]) < 0.0 or float(self.poly_df.bounds["miny"]) < 0.0 or float(
                self.poly_df.bounds["maxx"]) > self.x_max or float(self.poly_df.bounds["maxy"]) > self.y_max:
            return True
        else:
            return False

    def move_shape(self, x_rel, y_rel):
        self.poly_df = self.poly_df.translate(xoff=x_rel, yoff=y_rel)

        if self.is_collision() and 1 == 1:
            self.poly_df = self.poly_df.translate(xoff=-x_rel, yoff=-y_rel)
            log.warning("Movement was reverted, because points were out of bounds")

    def rotate_shape(self, rotation_angle):
        self.poly_df = self.poly_df.rotate(angle=rotation_angle)

    def print_shape(self):
        colors = "bgrcmykw"
        self.poly_df.plot(ax=self.ax, color=colors[self.level])
        self.ax.plot(self.poly_df.centroid.x, self.poly_df.centroid.y, "ko")
        plt.ion()
        plt.show()
        plt.pause(0.001)

    def get_distance_to_environment_center(self):
        distance = math.sqrt(
            (self.x_max / 2 - self.poly_df.centroid.x) ** 2 + (self.y_max / 2 - self.poly_df.centroid.y) ** 2)
        return distance

    def get_x_distance_to_environment_center(self):
        x_distance = self.x_max / 2 - self.poly_df.centroid.x
        return x_distance

    def get_y_distance_to_environment_center(self):
        y_distance = self.y_max / 2 - self.poly_df.centroid.y
        return y_distance

    def get_shape_area(self):
        area = self.poly_df.area
        return area

    def set_shape_level(self, level):
        self.level = level

    def get_shape_level(self):
        level = self.level
        return level

    def move(self, action):
        if action is None:
            self.move_shape(random.randint(-100, 100), random.randint(-100, 100))
        elif np.array_equal(action, [1, 0, 0, 0]):  # UP
            self.move_shape(0, 30)
        elif np.array_equal(action, [0, 1, 0, 0]):  # LEFT
            self.move_shape(-30, 0)
        elif np.array_equal(action, [0, 0, 1, 0]):  # RIGHT
            self.move_shape(30, 0)
        else:  # [0, 0, 0, 1] DOWN
            self.move_shape(0, -30)
        self.print_shape()


def play_step(objects, cur_level, action=None):
    draw_env()

    for o in objects:
        if o.level != cur_level:
            o.print_shape()

    self = objects[cur_level]

    game_over = False
    self.move(action)
    self.score += 1

    if self.old_distance < self.get_distance_to_environment_center():
        reward = -1
    else:
        reward = 0

    if self.get_distance_to_environment_center() < 40:
        game_over = True
        reward = 0

    self.old_distance = self.get_distance_to_environment_center()

    return reward, game_over, self.score


def get_shape_level_list_function(list_elem):
    return list_elem.get_shape_level()


def draw_env():
    ax.cla()
    plt.xlim([0, x_max])
    plt.ylim([0, y_max])
    ax.plot(x_max / 2, y_max / 2, "ko")


def init():
    ax.cla()
    plt.xlim([0, x_max])
    plt.ylim([0, y_max])
    ax.plot(x_max / 2, y_max / 2, "ko")

    triangle = Shape(x_max=x_max, y_max=y_max, ax=ax, level=0)
    triangle.draw_polygon([(1000, 1500), (1000, 500), (3000, 500)], -10)
    triangle.print_shape()

    circle = Shape(x_max=x_max, y_max=y_max, ax=ax, level=2)
    circle.draw_circle((500, 500), 200)
    circle.print_shape()

    rectangle = Shape(x_max=x_max, y_max=y_max, ax=ax, level=1)
    rectangle.draw_polygon([(2000, 200), (2500, 200), (2500, 800), (2000, 800)], 0)
    rectangle.print_shape()

    objects = list([triangle, circle, rectangle])
    objects.sort(key=get_shape_level_list_function)

    return objects


if __name__ == "__main__":
    Shape = init()
    Shape = Shape[0]
    for n in range(100):
        Shape.play_step()
