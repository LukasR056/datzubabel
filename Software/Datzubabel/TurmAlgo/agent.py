import torch
import os
import random
import numpy as np
from collections import deque
from env_agent import init, play_step
from Utilities.utilitites import JsonWriteReader
from Utilities.logger import LoggerManager, Logger
from model import Linear_QNet, QTrainer

os.environ["KMP_DUPLICATE_LIB_OK"] = "TRUE"

log = LoggerManager.getLogger(Logger.DefaultLogger)
json_read_writer = JsonWriteReader("Configuration.json")

if json_read_writer.json_exists():
    content = json_read_writer.read_json()
    x_max = content["PlayField"]["max-x-steps"]
    y_max = content["PlayField"]["max-y-steps"]
else:
    log.error("Configuration File does not exists!")

MAX_MEMORY = content["QL"]["max_memory"]
BATCH_SIZE = content["QL"]["batch_size"]
LR = content["QL"]["lr"]
USE_TRAINED = content["QL"]["use_trained"]


# MAX_MEMORY = 100_000

class Agent:

    def __init__(self):
        self.n_games = 0
        self.epsilon = 0  # randomness
        self.gamma = 0.9  # discount rate
        self.memory = deque(maxlen=MAX_MEMORY)  # popleft()
        self.model = Linear_QNet(4, 256, 4)
        self.trainer = QTrainer(self.model, lr=LR, gamma=self.gamma)

    def get_state(self, game):
        shape_x = (game.x_max / 2)
        shape_y = (game.y_max / 2)
        cent_x = game.poly_df.centroid.x
        cent_y = game.poly_df.centroid.y

        test = (cent_x < shape_x)[0]

        state = [
            (cent_x < shape_x)[0],  # food left
            (cent_x > shape_x)[0],  # food right
            (cent_y < shape_y)[0],  # food up
            (cent_y > shape_y)[0]  # food down
        ]
        state = np.array(state, dtype=int)
        return state

    def remember(self, state, action, reward, next_state, done):
        self.memory.append((state, action, reward, next_state, done))  # popleft if MAX_MEMORY is reached

    def train_long_memory(self):
        if len(self.memory) > BATCH_SIZE:
            mini_sample = random.sample(self.memory, BATCH_SIZE)  # list of tuples
        else:
            mini_sample = self.memory

        states, actions, rewards, next_states, dones = zip(*mini_sample)
        self.trainer.train_step(states, actions, rewards, next_states, dones)
        # for state, action, reward, nexrt_state, done in mini_sample:
        #    self.trainer.train_step(state, action, reward, next_state, done)

    def train_short_memory(self, state, action, reward, next_state, done):
        self.trainer.train_step(state, action, reward, next_state, done)

    def get_action(self, state):
        # random moves: tradeoff exploration / exploitation
        self.epsilon = 80 - self.n_games
        final_move = [0, 0, 0, 0]
        if random.randint(0, 200) < self.epsilon:
            move = random.randint(0, 3)
            final_move[move] = 1
        else:
            state0 = torch.tensor(state, dtype=torch.float)
            prediction = self.model(state0)
            move = torch.argmax(prediction).item()
            final_move[move] = 1

        return final_move


def train():
    plot_scores = []
    plot_mean_scores = []
    total_score = 0
    record = 1000
    agent = Agent()
    objects = init()
    level = 0
    active_obj = objects[level]

    if USE_TRAINED == 1:
        agent.model.load()

    while True:
        # get old state
        state_old = agent.get_state(active_obj)

        # get move
        final_move = agent.get_action(state_old)

        # perform move and get new state
        reward, done, score = play_step(objects, level, final_move)
        state_new = agent.get_state(active_obj)

        # train short memory
        agent.train_short_memory(state_old, final_move, reward, state_new, done)

        # remember
        agent.remember(state_old, final_move, reward, state_new, done)

        if done:
            # train long memory, plot result
            if USE_TRAINED == 1:
                level += 1
                if level == len(objects):
                    for o in objects:
                        o.reset()
                    level = 0
                active_obj = objects[level]
            active_obj.reset()
            agent.train_long_memory()
            agent.n_games += 1

            if score < record:
                record = score
                agent.model.save()

            print('Game', agent.n_games, 'Score', score, 'Record:', record)

            plot_scores.append(score)
            total_score += score
            mean_score = total_score / agent.n_games
            plot_mean_scores.append(mean_score)


if __name__ == '__main__':
    train()
