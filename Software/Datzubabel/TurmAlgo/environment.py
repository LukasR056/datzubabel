import math
import random

import matplotlib
import numpy as np

import matplotlib as mpl
#matplotlib.interactive(True)

mpl.use("Qt5Agg")

from shapely.geometry import Polygon
from shapely.geometry.point import Point
from shapely import affinity
import matplotlib.pyplot as plt
import geopandas as gpd
from Utilities.utilitites import JsonWriteReader
from Utilities.logger import LoggerManager, Logger
from PIL import Image


class Shape:
    """
    Class for creating and manipulating the shapes/objects that can be used to create a tower.
    """

    def __init__(self, x_max, y_max, ax=None, level=None):
        self.lower_left_x = None
        self.lower_left_y = None
        self.x_max = x_max
        self.y_max = y_max
        self.poly_df = None
        self.ax = ax
        self.level = level

    def draw_polygon(self, points, rotation_angle):
        error = False

        for point in points:
            if point[0] < 0 or point[0] > self.x_max or point[1] < 0 or point[1] > self.y_max:
                #log.error("Point out of bounds")
                error = True
                break

        if error is False:
            poly = Polygon(points)
            poly = affinity.rotate(poly, rotation_angle, "center")
            for point in list(poly.exterior.coords):
                if point[0] < 0 or point[0] > self.x_max or point[1] < 0 or point[1] > self.y_max:
                    #log.warning("Rotation was reverted, because points were out of bounds")
                    error = True
                    break
            if error:
                poly = affinity.rotate(poly, -rotation_angle, "centroid")

        self.poly_df = gpd.GeoSeries(poly)

    def draw_circle(self, center, radius):
        c = Point(center)
        circle = c.buffer(radius)
        self.draw_polygon(list(circle.exterior.coords), 0)

    def move_shape(self, x_rel, y_rel):
        self.poly_df = self.poly_df.translate(xoff=x_rel, yoff=y_rel)
        if float(self.poly_df.bounds["minx"]) < 0.0 or float(self.poly_df.bounds["miny"]) < 0.0 or float(
                self.poly_df.bounds["maxx"]) > self.x_max or float(self.poly_df.bounds["maxy"]) > self.y_max:
            self.poly_df = self.poly_df.translate(xoff=-x_rel, yoff=-y_rel)
            #log.warning("Movement was reverted, because points were out of bounds")

    def rotate_shape(self, rotation_angle):
        self.poly_df = self.poly_df.rotate(angle=rotation_angle, origin="centroid")

    def print_shape(self):
        colors = "bgrcmykw"
        self.poly_df.plot(ax=self.ax, color=colors[self.level])
        self.ax.plot(self.poly_df.centroid.x, self.poly_df.centroid.y, "ko")
        #plt.draw()
        #plt.pause(0.01)

    def get_distance_to_environment_center(self):
        distance = math.sqrt(
            (self.x_max / 2 - self.poly_df.centroid.x) ** 2 + (self.y_max / 2 - self.poly_df.centroid.y) ** 2)
        return distance

    def get_x_distance_to_environment_center(self):
        x_distance = self.x_max / 2 - self.poly_df.centroid.x
        return x_distance

    def get_y_distance_to_environment_center(self):
        y_distance = self.y_max / 2 - self.poly_df.centroid.y
        return y_distance

    def get_shape_area(self):
        area = self.poly_df.area
        return area

    def set_shape_level(self, level):
        self.level = level

    def get_shape_level(self):
        level = self.level
        return level


# extracted functions from gui for cleaner code
# def prepare_simulation(unet_model_img):
#     fig, ax = plt.subplots()
#     plt.xlim([0, np.shape(unet_model_img[:, :, 0])[1]])
#     plt.ylim([0, np.shape(unet_model_img[:, :, 0])[0]])
#     simulated_img_prep = get_shapes_from_unet_image(unet_model_img[:, :, 0])
#     create_environment(simulated_img_prep, np.shape(unet_model_img[:, :, 0])[1],
#                        np.shape(unet_model_img[:, :, 0])[0], ax, level=1)
#     fig.canvas.draw()
#     simulated_img = np.frombuffer(fig.canvas.tostring_rgb(), dtype=np.uint8)
#     simulated_img = simulated_img.reshape(fig.canvas.get_width_height()[::-1] + (3,))
#     return simulated_img
#
#
# def run_simulation(unet_model_img):
#     fig, ax = plt.subplots()
#     plt.xlim([0, np.shape(unet_model_img[:, :, 0])[1]])
#     plt.ylim([0, np.shape(unet_model_img[:, :, 0])[0]])
#     simulated_img_prep = get_shapes_from_unet_image(unet_model_img[:, :, 0])
#     objects, object_rotation = optimize_rotation(simulated_img_prep, x_max=np.shape(unet_model_img[:, :, 0])[1],
#                                                  y_max=np.shape(unet_model_img[:, :, 0])[0], ax=ax, isImage=True)
#     ax.cla()
#     plt.gca().invert_yaxis()
#
#     for o in objects:
#         o.print_shape()
#
#     fig.canvas.draw()
#     simulated_img = np.frombuffer(fig.canvas.tostring_rgb(), dtype=np.uint8)
#     simulated_img = simulated_img.reshape(fig.canvas.get_width_height()[::-1] + (3,))
#     return simulated_img


# 64 Cuboid, 128 Cylinder, 192 Prism
def get_shapes_from_unet_image(image, cuboid_value=64, cylinder_value=128, prism_value=192):
    # image = image[:, :, 0]
    cuboid_exists = np.any(image[:, :] == cuboid_value)
    cylinder_exists = np.any(image[:, :] == cylinder_value)
    prism_exists = np.any(image[:, :] == prism_value)

    cuboid_corners = list()
    cylinder_corners = list()
    prism_corners = list()
    return_list = list()

    cuboid_break = False
    cylinder_break = False
    prism_break = False

    # from top to bottom
    for y in range(len(image[:, 0])):
        for x in range(len(image[0, :])):
            if cuboid_exists and not cuboid_break:
                if image[y, x] == cuboid_value:
                    cuboid_corners.append((x, y))
                    cuboid_break = True
            if cylinder_exists and not cylinder_break:
                if image[y, x] == cylinder_value:
                    cylinder_corners.append((x, y))
                    cylinder_break = True
            if prism_exists and not prism_break:
                if image[y, x] == prism_value:
                    prism_corners.append((x, y))
                    prism_break = True

    cuboid_break = False
    cylinder_break = False
    prism_break = False

    # from left to right
    for x in range(len(image[0, :])):
        for y in range(len(image[:, 0])):
            if cuboid_exists and not cuboid_break:
                if image[y, x] == cuboid_value:
                    cuboid_corners.append((x, y))
                    cuboid_break = True
            if cylinder_exists and not cylinder_break:
                if image[y, x] == cylinder_value:
                    cylinder_corners.append((x, y))
                    cylinder_break = True
            if prism_exists and not prism_break:
                if image[y, x] == prism_value:
                    prism_corners.append((x, y))
                    prism_break = True

    cuboid_break = False
    cylinder_break = False
    prism_break = False

    # from bottom to top
    for y in range(len(image[:, 0]) - 1, 0, -1):
        for x in range(len(image[0, :])):
            if cuboid_exists and not cuboid_break:
                if image[y, x] == cuboid_value:
                    cuboid_corners.append((x, y))
                    cuboid_break = True
            if cylinder_exists and not cylinder_break:
                if image[y, x] == cylinder_value:
                    cylinder_corners.append((x, y))
                    cylinder_break = True
            if prism_exists and not prism_break:
                if image[y, x] == prism_value:
                    prism_corners.append((x, y))
                    prism_break = True

    cuboid_break = False
    cylinder_break = False
    prism_break = False

    # from right to left
    for x in range(len(image[0, :]) - 1, 0, -1):
        for y in range(len(image[:, 0])):
            if cuboid_exists and not cuboid_break:
                if image[y, x] == cuboid_value:
                    cuboid_corners.append((x, y))
                    cuboid_break = True
            if cylinder_exists and not cylinder_break:
                if image[y, x] == cylinder_value:
                    cylinder_corners.append((x, y))
                    cylinder_break = True
            if prism_exists and not prism_break:
                if image[y, x] == prism_value:
                    prism_corners.append((x, y))
                    prism_break = True

    if cuboid_exists:
        return_list.append(["Cuboid", cuboid_corners])

    if cylinder_exists:
        cylinder_center = (np.mean([el[0] for el in cylinder_corners]), np.mean([el[1] for el in cylinder_corners]))
        cylinder_radius = (abs(cylinder_corners[1][0] - cylinder_corners[3][0]) + abs(
            cylinder_corners[0][1] - cylinder_corners[2][1])) / 4
        return_list.append(["Cylinder", cylinder_center, cylinder_radius])

    if prism_exists:
        return_list.append(["Prism", prism_corners])

    return return_list


def create_environment(data, x_max, y_max, ax, level):
    plt.gca().invert_yaxis()
    ax.plot(x_max / 2, y_max / 2, "ko")

    objects = [Shape(x_max=x_max, y_max=y_max, ax=ax, level=level) for _ in data]

    for count, o in enumerate(objects):
        if data[count][0] == "Cuboid" or data[count][0] == "Prism":
            o.draw_polygon(data[count][1], 0)
        if data[count][0] == "Cylinder":
            o.draw_circle(data[count][1], data[count][2])
        o.print_shape()
        #plt.pause(0.1)


def get_shape_level_list_function(list):
    return list.get_shape_level()


# This Main function is created for testing purposes of environment movement
""" 
if __name__ == "__main__":
    log = LoggerManager.getLogger(Logger.DefaultLogger)
    json_read_writer = JsonWriteReader("Configuration.json")

    if json_read_writer.json_exists():
        content = json_read_writer.read_json()
        x_max = content["PlayField"]["max-x-steps"]
        y_max = content["PlayField"]["max-y-steps"]
    else:
        log.error("Configuration File does not exists!")

    fig, ax = plt.subplots()
    plt.xlim([0, x_max])
    plt.ylim([0, y_max])

    ax.plot(x_max / 2, y_max / 2, "ko")

    triangle = Shape(x_max=x_max, y_max=y_max, ax=ax, level=0)
    triangle.draw_polygon([(1000, 1500), (1000, 500), (3000, 500)], -10)
    triangle.print_shape()

    circle = Shape(x_max=x_max, y_max=y_max, ax=ax, level=1)
    circle.draw_circle((500, 500), 200)
    circle.print_shape()

    rectangle = Shape(x_max=x_max, y_max=y_max, ax=ax, level=2)
    rectangle.draw_polygon([(2000, 200), (2500, 200), (2500, 800), (2000, 800)], 0)
    rectangle.print_shape()

    objects = list([triangle, circle, rectangle])
    objects.sort(key=get_shape_level_list_function)

    for i in range(100):
        ax.cla()
        plt.xlim([0, x_max])
        plt.ylim([0, y_max])
        ax.plot(x_max / 2, y_max / 2, "ko")

        for o in objects:
            o.move_shape(random.randint(-100, 100), random.randint(-100, 100))
            o.print_shape()
        # plt.pause(0.1)

    input("Press Enter to end program...")
"""

# This Main function is created for testing purposes to save figure as an image
"""
if __name__ == "__main__":
    image = Image.open("TurmAlgo/util/Unet_Maske_Test.png")
    image = np.asarray(image)

    data = get_shapes_from_unet_image(image)

    fig, ax = plt.subplots()
    plt.xlim([0, np.shape(image)[1]])
    plt.ylim([0, np.shape(image)[0]])
    #plt.ion()

    create_environment(data, np.shape(image)[1], np.shape(image)[0], ax, level=1)
    fig.canvas.draw()
    data = np.frombuffer(fig.canvas.tostring_rgb(), dtype=np.uint8)
    data = data.reshape(fig.canvas.get_width_height()[::-1] + (3,))

    #plt.savefig("test")
    #plt.ioff()
    #plt.show()
    #input("Press [Enter] to close plot.")
"""

# This Main function is created for demonstration purposes for NN2 presentation
if __name__ == "__main__":
    image = Image.open("TurmAlgo/util/Unet_Maske_Test.png")
    image = np.asarray(image)

    data = get_shapes_from_unet_image(image)

    fig, ax = plt.subplots()
    plt.xlim([0, np.shape(image)[1]])
    plt.ylim([0, np.shape(image)[0]])

    create_environment(data, np.shape(image)[1], np.shape(image)[0], ax=ax, level=1)

    plt.pause(60)
    input("Press [Enter] to close plot.")