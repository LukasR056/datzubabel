import matplotlib as mpl
mpl.use("Qt5Agg")

from TurmAlgo.environment import Shape, get_shapes_from_unet_image
from Utilities.utilitites import JsonWriteReader
from Utilities.logger import LoggerManager, Logger
import matplotlib.pyplot as plt
from PIL import Image
import numpy as np

"""
This script uses the created simulation environment to find the optimal rotation of each object
in order to maximize the overlapping area of the objects for maximal tower stability.
In order for this script to work as intended the centroids of every object already need to
be stacked on each other.
"""

def get_shape_area_list_function(object_list):
    return int(object_list.get_shape_area())

def optimize_rotation(object_list,x_max, y_max, ax, isImage = True):
    if not isImage:
        object_rotation = [0]
        largest_intersection = 0
        for i in range(1, len(object_list)):
            object_rotation.append(0)
            for phi in range(360):
                object_list[i].rotate_shape(1)
                intersection = object_list[i-1].poly_df.intersection(object_list[i].poly_df).area
                if float(intersection) > largest_intersection:
                    largest_intersection = float(intersection)
                    object_rotation[i] = phi
            object_list[i].rotate_shape(object_rotation[i])
    else:
        objects = [Shape(x_max=x_max, y_max=y_max, ax=ax, level=count) for count, _ in enumerate(object_list)]
        for count, o in enumerate(objects):
            if object_list[count][0] == "Cuboid" or object_list[count][0] == "Prism":
                o.draw_polygon(object_list[count][1], 0)
            if object_list[count][0] == "Cylinder":
                o.draw_circle(object_list[count][1], object_list[count][2])

        objects.sort(key=get_shape_area_list_function, reverse=True)

        for o in objects:
            o.move_shape(o.get_x_distance_to_environment_center(), o.get_y_distance_to_environment_center())

        object_list = objects.copy()
        object_rotation = [0]
        largest_intersection = 0
        for i in range(1, len(object_list)):
            object_rotation.append(0)
            for phi in range(360):
                object_list[i].rotate_shape(1)
                intersection = object_list[i - 1].poly_df.intersection(object_list[i].poly_df).area
                if float(intersection) > largest_intersection:
                    largest_intersection = float(intersection)
                    object_rotation[i] = phi
            object_list[i].rotate_shape(object_rotation[i])
    return object_list, object_rotation

# This main function is created for testing purposes of object rotation with static input
"""
if __name__ == "__main__":
    log = LoggerManager.getLogger(Logger.DefaultLogger)
    json_read_writer = JsonWriteReader("Configuration.json")

    if json_read_writer.json_exists():
        content = json_read_writer.read_json()
        x_max = content["PlayField"]["max-x-steps"]
        y_max = content["PlayField"]["max-y-steps"]
    else:
        log.error("Configuration File does not exists!")

    fig, ax = plt.subplots()
    plt.xlim([0, x_max])
    plt.ylim([0, y_max])

    ax.plot(x_max / 2, y_max / 2, "ko")

    triangle = Shape(x_max=x_max, y_max=y_max, ax=ax, level=0)
    triangle.draw_polygon([(1000, 1500), (1000, 500), (3000, 500)], -10)
    triangle.move_shape(triangle.get_x_distance_to_environment_center(), triangle.get_y_distance_to_environment_center())

    circle = Shape(x_max=x_max, y_max=y_max, ax=ax, level=1)
    circle.draw_circle((500, 500), 150)
    circle.move_shape(circle.get_x_distance_to_environment_center(), circle.get_y_distance_to_environment_center())

    rectangle = Shape(x_max=x_max, y_max=y_max, ax=ax, level=2)
    rectangle.draw_polygon([(2000, 200), (2400, 200), (2400, 1500), (2000, 1500)], 0)
    rectangle.move_shape(rectangle.get_x_distance_to_environment_center(), rectangle.get_y_distance_to_environment_center())

    objects = list([triangle, circle, rectangle])
    objects.sort(key=get_shape_area_list_function, reverse=True)

    # Comment the following line to see the original, unrotated position
    objects = optimize_rotation(objects)
    ax.cla()

    for o in objects:
       o.print_shape()

    #plt.show()

    input("Press Enter to end program...")
"""

# This main function is created for testing purposes ob object rotation with image input
if __name__ == "__main__":
    image = Image.open("TurmAlgo/Util/Unet_Maske_Test.png")
    image = np.asarray(image)
    data = get_shapes_from_unet_image(image)

    fig, ax = plt.subplots()
    plt.xlim([0, np.shape(image)[1]])
    plt.ylim([0, np.shape(image)[0]])

    objects, object_rotation = optimize_rotation(data, x_max=np.shape(image)[1], y_max=np.shape(image)[0], ax=ax, isImage=True)
    ax.cla()
    plt.gca().invert_yaxis()

    for o in objects:
        o.print_shape()

    fig.canvas.draw()
    data = np.frombuffer(fig.canvas.tostring_rgb(), dtype=np.uint8)
    data = data.reshape(fig.canvas.get_width_height()[::-1] + (3,))