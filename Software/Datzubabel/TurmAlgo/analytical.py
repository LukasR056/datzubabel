from Utilities.logger import LoggerManager, Logger
from Utilities.utilitites import JsonWriteReader
import pandas as pd
import numpy as np
import cv2
import os

pd.options.mode.chained_assignment = None

dirname = os.path.dirname(os.path.abspath(__file__))
json_read_writer = JsonWriteReader("Configuration.json")
log = LoggerManager.getLogger(Logger.DefaultLogger)


def prepare_data(df, img, area_calc_f):
    if img is not None:
        crop_img(df, img)
        manipulate_img(df)

    calc_centroid(df)

    if json_read_writer.json_exists() and area_calc_f not in ('bounding_box', 'whitespace'):
        content = json_read_writer.read_json()
        area_calc_f = content["Analytical"]["area_calculation"]

    if area_calc_f == 'bounding_box' or img is None:
        calc_area_bounding_box(df)
    else:
        calc_area_bounding_box(df)
    return df


def manipulate_img(df):
    for index, row in df.iterrows():
        alpha = 3
        beta = 0

        df['image'][index] = cv2.convertScaleAbs(df['image'][index], alpha=alpha, beta=beta)
    return df


def crop_img(df, img):
    df['image'] = None
    for index, row in df.iterrows():
        print(row['ymin'])

        ymin = int(row['ymin'])
        ymax = int(row['ymax'])
        xmax = int(row['xmax'])
        xmin = int(row['xmin'])

        df['image'][index] = img[ymin:ymax, xmin:xmax]
    return df


def calc_centroid(df):
    # skip if center coordinates are provided by yolo environment
    if ('xcenter' in df.columns) and ('ycenter' in df.columns):
        return df
    else:
        df['xcenter'] = None
        df['ycenter'] = None
        for index, row in df.iterrows():
            centroid = (((row['xmin'] + row['xmax']) / 2), ((row['ymin'] + row['ymax']) / 2))
            df['xcenter'][index] = centroid[0]
            df['ycenter'][index] = centroid[1]
    return df


def calc_area_bounding_box(df):
    df['area'] = None
    for index, row in df.iterrows():
        le = row['xmax'] - row['xmin']
        b = row['ymax'] - row['ymin']
        area = le * b
        df['area'][index] = round(area, 2)
    return df


def calc_area_whitespace(df):
    df['area'] = None
    boundaries = [([225, 225, 225], [255, 255, 255])]

    for index, row in df.iterrows():
        img = df['image'][index]

        for (lower, upper) in boundaries:
            lower = np.array(lower, dtype="uint8")
            upper = np.array(upper, dtype="uint8")

            mask = cv2.inRange(img, lower, upper)
            output = cv2.bitwise_and(img, img, mask=mask)

        tot_pixel = output.size
        white_pixels = np.count_nonzero(output)
        percentage = round(((white_pixels * 100 / tot_pixel) * 0.01), 4)

        log.debug("White-ish pixels: " + str(white_pixels))
        log.debug("Total pixels: " + str(tot_pixel))
        log.debug("Percentage: " + str(percentage*100) + "%")

        df['area'][index] = white_pixels
    return df


def maximum_absolute_scaling(df, column):
    df[column] = df[column] / df[column].abs().max()
    return df


def min_max_scaling(df, column):
    df[column] = (df[column] - df[column].min()) / (df[column].max() - df[column].min())
    return df


def scaling_selector(df, column, scaling_func):
    function = eval(scaling_func + "(df, column)")
    return function


def calc_ranking(df, formula):
    df['score'] = None
    fx = 0

    if json_read_writer.json_exists():
        content = json_read_writer.read_json()
        w_area = content["Analytical"]["weight_area"]
        scaling_func = content["Analytical"]["scaling"]
        w_name_dict = content["Analytical"]["Weight_Name"]
    else:
        log.error("calc_ranking: Configuration file not found!")
        raise Exception("calc_ranking: Configuration file not found!")

    df = scaling_selector(df, 'area', scaling_func)
    
    for index, row in df.iterrows():
        w_name = (w_name_dict[row['name']] or 1)
        if formula == 'name':
            fx = w_name * row['confidence']
        if formula == '+area' or formula == 'yolo_yolo':
            fx = (w_area * row['area']) + (w_name + row['confidence'])
        df['score'][index] = fx

    df = scaling_selector(df, 'score', scaling_func)

    return df


def main(df, formula, area_calc_f, img=None):
    if df is None or df.empty:
        log.error("main: df.empty returned true - df values missing")
        raise Exception("main: df.empty returned true - df values missing")

    df = prepare_data(df, img, area_calc_f)
    df = calc_ranking(df, formula)

    if img is None:
        cols = ['name', 'class', 'score', 'xmin_abs', 'ymin_abs', 'xmax_abs', 'ymax_abs',
                'area', 'xcenter', 'ycenter', 'confidence']
    else:
        cols = ['name', 'class', 'score', 'xmin_abs', 'ymin_abs', 'xmax_abs', 'ymax_abs',
                'area', 'xcenter', 'ycenter', 'confidence']
    df = df[cols]

    if formula == 'yolo_yolo':
        df = df.sort_values(by=['score'], ascending=True)
    else:
        df = df.sort_values(by=['score'], ascending=False)

    df = df.reset_index()

    print(df)
    log.debug(df)
    return df
