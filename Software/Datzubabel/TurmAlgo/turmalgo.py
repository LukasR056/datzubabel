from Utilities.logger import LoggerManager, Logger
from matplotlib import pyplot
from ObjectDetection.yolo5 import Yolo5
from TurmAlgo import analytical
import cv2

log = LoggerManager.getLogger(Logger.DefaultLogger)


def select(objects, algorithm='analytical', an_formula='+area', area_calc='bounding_box', img=None):
    log.info(f"select: Entering turmalgo selection with algorithm '{algorithm}' and formula '{an_formula}'")

    if algorithm == 'analytical':
        ranking = analytical.main(df=objects, formula=an_formula, area_calc_f=area_calc, img=None)
    else:
        log.error(f"select: Algorithm '{algorithm}' not found!")
        raise Exception("Algorithm not found!")

    return ranking


def generate_test_data():
    log.warning("select: Using generated test (!) data instead of provided objects - is this intended?")

    yolo_model = Yolo5("best_pro_500.pt")
    img = cv2.imread('ObjectDetection/img/Bild_364.jpg')

    #cv2.imshow("OpenCV Image Reading", img)
    #cv2.waitKey(0)

    yolo_model.detect_objects(img)
    # img2 = yolo_model.detection.render()[0]
    # pyplot.imshow(img2)
    # pyplot.show()
    objects = yolo_model.df_detection_normalized

    select(objects, an_formula='+area', area_calc='bounding_box')

    return objects


if __name__ == "__main__":
    generate_test_data()
