from enum import Enum
import numpy as np


class PickObjectTasks(Enum):
    MOVE_Z_TO_OBJECT = 0
    ADJUST_MAGNET = 1
    ACTIVATE_MAGNET = 2
    PICK_UP_OBJECT = 3


class StackObjectTasks(Enum):
    MOVE_TO_STACK = 0
    DEACTIVATE_MAGNET = 1
    MOVE_TO_Z_MAX = 2


class Action:
    def __init__(self):
        self.action_done = False
        self.tasks = np.array([])

    def set_task_done(self, task):
        if isinstance(task, PickObjectTasks) or isinstance(task, StackObjectTasks):
            self.tasks[task.value][1] = "True"

    def is_task_done(self, task):
        if isinstance(task, PickObjectTasks) or isinstance(task, StackObjectTasks):
            return True if self.tasks[task.value][1] == "True" else False

    def set_done(self):
        self.action_done = True

    def is_done(self):
        return True if self.action_done else False


class ActionMoveXY(Action):
    def __init__(self, xy, x_offset=0):
        super().__init__()
        self.xy = np.array([[xy[0] - x_offset, 0], [xy[1], 0]])
        self.at_position = False

    def get_x_pos(self):
        return self.xy[0][0]

    def get_y_pos(self):
        return self.xy[1][0]

    def set_x_pos_done(self):
        self.xy[0][1] = 1
        self.check_if_is_at_position()

    def set_y_pos_done(self):
        self.xy[1][1] = 1
        self.check_if_is_at_position()

    def is_at_x_pos(self):
        return True if self.xy[0][1] == 1 else False

    def is_at_y_pos(self):
        return True if self.xy[1][1] == 1 else False

    def check_if_is_at_position(self):
        if self.is_at_x_pos() and self.is_at_y_pos():
            self.at_position = True
            self.set_done()

    def __str__(self):
        return "Action MoveXY (" + str(self.get_x_pos()) + "/" + str(self.get_y_pos()) + ")"


class ActionPickObject(Action):
    def __init__(self):
        super().__init__()

        self.tasks = np.array([[PickObjectTasks.MOVE_Z_TO_OBJECT.name, "False"],
                               [PickObjectTasks.ADJUST_MAGNET.name, "False"],
                               [PickObjectTasks.ACTIVATE_MAGNET.name, "False"],
                               [PickObjectTasks.PICK_UP_OBJECT.name, "False"]])

        self.x_adjust = False
        self.z_adjust = False

    def set_magnet_adjusted_done(self):
        self.set_task_done(PickObjectTasks.ADJUST_MAGNET)

    def is_magnet_adjusted(self):
        return self.is_task_done(PickObjectTasks.ADJUST_MAGNET)

    def set_z_moved_to_object_done(self):
        self.set_task_done(PickObjectTasks.MOVE_Z_TO_OBJECT)

    def is_z_moved_to_object(self):
        return self.is_task_done(PickObjectTasks.MOVE_Z_TO_OBJECT)

    def set_magnet_activated_done(self):
        self.set_task_done(PickObjectTasks.ACTIVATE_MAGNET)

    def is_magnet_activated(self):
        return self.is_task_done(PickObjectTasks.ACTIVATE_MAGNET)

    def set_object_picked_up(self):
        self.set_task_done(PickObjectTasks.PICK_UP_OBJECT)
        self.set_done()

    def is_object_picked_up(self):
        return self.is_task_done(PickObjectTasks.PICK_UP_OBJECT)

    def set_x_pos_done(self):
        self.x_adjust = True

    def set_z_pos_done(self):
        self.z_adjust = True

    def is_at_x_pos(self):
        return self.x_adjust

    def is_at_z_pos(self):
        return self.z_adjust

    def is_at_position(self):
        if self.is_at_x_pos() and self.is_at_z_pos():
            return True

    def __str__(self):
        return "Action PickObject"


class ActionMeasure(Action):
    def __init__(self):
        super().__init__()

    def __str__(self):
        return "Action Measure"


class ActionStackObject(Action):
    def __init__(self):
        super().__init__()
        self.tasks = np.array([[StackObjectTasks.MOVE_TO_STACK.name, "False"],
                               [StackObjectTasks.DEACTIVATE_MAGNET.name, "False"],
                               [StackObjectTasks.MOVE_TO_Z_MAX.name, "False"]])

    def set_moved_to_stack_done(self):
        self.set_task_done(StackObjectTasks.MOVE_TO_STACK)

    def is_moved_to_stack(self):
        return self.is_task_done(StackObjectTasks.MOVE_TO_STACK)

    def set_magnet_deactivated_done(self):
        self.set_task_done(StackObjectTasks.DEACTIVATE_MAGNET)

    def is_magnet_deactivated(self):
        return self.is_task_done(StackObjectTasks.DEACTIVATE_MAGNET)

    def set_moved_to_z_max_done(self):
        self.set_task_done(StackObjectTasks.MOVE_TO_Z_MAX)

    def is_moved_to_z_max(self):
        return self.is_task_done(StackObjectTasks.MOVE_TO_Z_MAX)

    def __str__(self):
        return "Action StackObject"
