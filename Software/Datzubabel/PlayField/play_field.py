from Arduino.arduino_api import DeviceStatus, StepMotor, StepMotorLabel, ArduinoUno, \
    StepMotorDirection
from Utilities.utilitites import JsonWriteReader
import threading
import time
from Utilities.logger import LoggerManager, Logger
from PlayField.action import Action, ActionMoveXY, ActionPickObject, ActionMeasure, ActionStackObject
import pandas as pd
from enum import Enum, auto

log = LoggerManager.getLogger(Logger.DefaultLogger)


class CalibrationStatus(Enum):
    NOT_ACTIVE = auto()
    IN_PROGRESS = auto()
    COUNT_Z_STEPS_IN_PROGRESS = auto()
    COUNT_XY_STEPS_IN_PROGRESS = auto()
    DONE = auto()


class MeasureDistance(Enum):
    NOT_INITIALISED = auto()
    IN_PROGRESS = auto()
    DONE = auto()


class GetPoller(threading.Thread):
    """ thread to repeatedly poll
    sleeptime: time to sleep between pollfunc calls
    pollfunc: function to repeatedly call to poll """

    def __init__(self, sleeptime, pollfunc):

        self.sleeptime = sleeptime
        self.pollfunc = pollfunc
        threading.Thread.__init__(self)
        self.runflag = threading.Event()  # clear this to pause thread
        self.runflag.clear()

    def run(self):
        self.runflag.set()
        self.worker()

    def kill(self):
        self.kill()

    def worker(self):
        while (1):
            if self.runflag.is_set():
                self.pollfunc()
                time.sleep(self.sleeptime)
            else:
                time.sleep(0.01)

    def pause(self):
        self.runflag.clear()

    def resume(self):
        self.runflag.set()

    def running(self):
        return self.runflag.is_set()


class PlayField:
    def __init__(self, arduinos, sleep_time, gui):
        self.arduinos = arduinos
        self.motor_x = self.get_arduino1().get_motor_x()
        self.motor_y = self.get_arduino1().get_motor_y()
        self.motor_z = self.get_arduino1().get_motor_z()
        self.magnet = self.get_arduino1().get_magnet()

        # state variables
        self.calibration_status = CalibrationStatus.NOT_ACTIVE
        self.is_referenced = True
        self.object_measure = MeasureDistance.NOT_INITIALISED

        json_read_writer = JsonWriteReader("Configuration.json")
        if json_read_writer.json_exists():
            content = json_read_writer.read_json()
            self.max_x_steps = content["PlayField"]["max-x-steps"]
            self.max_y_steps = content["PlayField"]["max-y-steps"]
            self.max_z_steps = content["PlayField"]["max-z-steps"]
            self.max_z_distance = content["PlayField"]["max-z-distance"]
            self.offset_x_magnet = content["PlayField"]["offset-x-magnet"]
            self.offset_z_magnet = content["PlayField"]["offset-z-magnet"]
            self.offset_z_stack = content["PlayField"]["offset-z-stack"]

        self.x_pos = 0
        self.y_pos = 0
        self.z_pos = self.max_z_steps

        self.max_x_pixel = 0
        self.max_y_pixel = 0

        # action / task stuff
        self.actions = []
        self.current_stack_height = 0
        self.current_object_height = 0

        # gui (temp variable for speed polling)
        self.gui = gui
        self.mot_x_speed = -1
        self.mot_y_speed = -1
        self.mot_z_speed = -1

        # worker stuff
        self.sleep_time = float(sleep_time)
        self.worker = GetPoller(self.sleep_time, self.poll)
        self.worker.start()

    def kill_worker(self):
        self.worker.kill()

    def poll(self):
        """Called repeatedly by thread """
        # print('%s \t "%s" \t "%s" \t"%s" \t"%s" \t' % (self, self.motor_x, self.motor_y, self.motor_z, self.magnet),
        #      end='\n')
        # sys.stdout.flush()
        self.finish_referencing()
        self.check_calibrate()
        self.finish_calibration()
        self.check_motor_end_positions()
        self.check_actions()

        self.set_motor_speed(self.gui)
        self.update_gui(self.gui)

    def get_arduino1(self):
        if isinstance(self.arduinos[0], ArduinoUno):
            return self.arduinos[0]
        return None

    def reference(self):
        self.is_referenced = False

        tid = self.get_arduino1().get_next_transaction_id()
        self.motor_x.run_to_min(tid)
        self.motor_y.run_to_min(tid)
        self.motor_z.run_to_max(tid)
        self.get_arduino1().execute(tid)

    def calibrate(self):
        if self.is_at_reference_point():
            tid = self.get_arduino1().get_next_transaction_id()
            self.motor_z.start_calibrate(tid)
            self.get_arduino1().execute(tid)

            tid = self.get_arduino1().get_next_transaction_id()
            self.motor_z.run_to_min(tid)
            self.get_arduino1().execute(tid)

            self.calibration_status = CalibrationStatus.COUNT_Z_STEPS_IN_PROGRESS
        else:
            log.warning("Play field is not referenced")

    def is_at_reference_point(self):
        return self.motor_x.get_status() == DeviceStatus.STOPPED_AT_MIN \
               and self.motor_y.get_status() == DeviceStatus.STOPPED_AT_MIN \
               and self.motor_z.get_status() == DeviceStatus.STOPPED_AT_MAX

    def finish_referencing(self):
        if self.is_at_reference_point():
            self.is_referenced = True
            self.x_pos = 0
            self.y_pos = 0
            self.z_pos = self.max_z_steps

    def check_calibrate(self):
        # measure distance
        if self.motor_z.get_status() == DeviceStatus.STOPPED_AT_MIN \
                and self.calibration_status == CalibrationStatus.COUNT_Z_STEPS_IN_PROGRESS:
            tid = self.get_arduino1().get_next_transaction_id()
            self.motor_z.run_to_max(tid)
            self.get_arduino1().execute(tid)

            self.calibration_status = CalibrationStatus.IN_PROGRESS

        if self.motor_z.get_status() == DeviceStatus.STOPPED_AT_MAX \
                and self.calibration_status == CalibrationStatus.IN_PROGRESS:
            tid = self.get_arduino1().get_next_transaction_id()
            self.motor_x.start_calibrate(tid)
            self.motor_y.start_calibrate(tid)
            self.get_arduino1().execute(tid)

            tid = self.get_arduino1().get_next_transaction_id()
            self.motor_x.run_to_max(tid)
            self.motor_y.run_to_max(tid)
            self.get_arduino1().execute(tid)

            self.calibration_status = CalibrationStatus.COUNT_XY_STEPS_IN_PROGRESS

        # go back to max position
        if self.calibration_status == CalibrationStatus.COUNT_XY_STEPS_IN_PROGRESS \
                and self.motor_x.get_status() == DeviceStatus.STOPPED_AT_MAX \
                and self.motor_y.get_status() == DeviceStatus.STOPPED_AT_MAX \
                and self.motor_z.get_status() == DeviceStatus.STOPPED_AT_MAX:
            self.calibration_status = CalibrationStatus.DONE

    def finish_calibration(self):
        if self.motor_x.get_status() == DeviceStatus.STOPPED_AT_MAX \
                and self.motor_y.get_status() == DeviceStatus.STOPPED_AT_MAX \
                and self.motor_z.get_status() == DeviceStatus.STOPPED_AT_MAX \
                and self.calibration_status == CalibrationStatus.DONE:

            self.max_x_steps = float(self.motor_x.get_max_steps())
            self.max_y_steps = float(self.motor_y.get_max_steps())
            self.max_z_steps = float(self.motor_z.get_max_steps())

            # persist information to file
            json_read_writer = JsonWriteReader("Configuration.json")
            if json_read_writer.json_exists():
                content = json_read_writer.read_json()
                content["PlayField"]["max-x-steps"] = self.max_x_steps
                content["PlayField"]["max-y-steps"] = self.max_y_steps
                content["PlayField"]["max-z-steps"] = self.max_z_steps
                json_read_writer.write_json(content)

            self.calibration_status = CalibrationStatus.NOT_ACTIVE

            # go back to zero position
            self.reference()

    def check_motor_end_positions(self):
        if self.motor_x.get_status() == DeviceStatus.STOPPED_AT_MAX:
            self.x_pos = self.max_x_steps
            self.gui.l_overview_x.setText("x: " + str(self.x_pos))
        elif self.motor_x.get_status() == DeviceStatus.STOPPED_AT_MIN:
            self.x_pos = 0
            self.gui.l_overview_x.setText("x: " + str(self.x_pos))

        if self.motor_y.get_status() == DeviceStatus.STOPPED_AT_MAX:
            self.y_pos = self.max_y_steps
            self.gui.l_overview_y.setText("y: " + str(self.y_pos))
        elif self.motor_y.get_status() == DeviceStatus.STOPPED_AT_MIN:
            self.y_pos = 0
            self.gui.l_overview_y.setText("y: " + str(self.y_pos))

        if self.motor_z.get_status() == DeviceStatus.STOPPED_AT_MAX:
            self.z_pos = self.max_z_steps
            self.gui.l_overview_z.setText("z: " + str(self.z_pos))
        elif self.motor_z.get_status() == DeviceStatus.STOPPED_AT_MIN:
            self.z_pos = 0
            self.gui.l_overview_z.setText("z: " + str(self.z_pos))

    def build_work_stream(self, objects):
        actions = []
        if isinstance(objects, pd.DataFrame):
            objects = [self.convert_pixel_to_steps((x, y)) for x, y in zip(objects['xcenter'], objects['ycenter'])]
        stack_coordinates = objects[0]
        for i, o in enumerate(objects):
            actions.append(ActionMoveXY(o, x_offset=self.offset_x_magnet))
            if i == 0:
                actions.append(ActionMeasure())
            else:
                actions.append(ActionPickObject())
                actions.append(ActionMoveXY([stack_coordinates[0], stack_coordinates[1]]))
                actions.append(ActionStackObject())
        return actions

    def stop_all(self):
        tid = self.get_arduino1().get_next_transaction_id()
        self.motor_x.stop(tid)
        self.motor_y.stop(tid)
        self.motor_z.stop(tid)
        self.magnet.magnet_off(tid)
        self.get_arduino1().execute(tid)

    def start_game(self, objects):
        # objects = self.gui.yolo_detection_df
        actions = self.build_work_stream(objects)
        self.current_stack_height = 0
        for action in actions:
            log.warning(action)
        self.add_actions(actions)

        if not self.is_at_reference_point:
            log.error("We are not at the zero position, cannot start game")
            # TODO raise exceptions to end game
        else:
            # take first actions from action list at the start
            action = self.get_current_action()
            if action is not None:
                self.handle_actions(action)

    def add_actions(self, actions):
        self.actions = actions

    def get_current_action(self):
        for action in self.actions:
            if isinstance(action, Action):
                if not action.is_done():
                    return action

        return None

    def handle_actions(self, action):
        log.info(action)
        if isinstance(action, ActionMoveXY):
            self.move_to_xy(action.get_x_pos(), action.get_y_pos())
        elif isinstance(action, ActionPickObject):
            self.move_z_to_min()
        elif isinstance(action, ActionMeasure):
            self.measure_distance()
        elif isinstance(action, ActionStackObject):
            self.move_to_z(self.offset_z_stack * -1)

    def update_position(self, motor, current_pos):
        if isinstance(motor, StepMotor):
            motor.position_reached = False
            log.debug("Position reached with %d steps of motor %s (%s) from pos: %s", motor.last_steps,
                      motor.label.name, motor.direction.name, current_pos)

            if motor.last_steps > 0:
                steps = motor.last_steps if motor.direction == StepMotorDirection.FORWARD else motor.last_steps * -1
                new_pos = current_pos + steps

                if motor.label == StepMotorLabel.X_AXIS:
                    self.x_pos = new_pos
                    self.gui.l_overview_x.setText("x: " + str(new_pos))
                elif motor.label == StepMotorLabel.Y_AXIS:
                    self.y_pos = new_pos
                    self.gui.l_overview_y.setText("y: " + str(new_pos))
                elif motor.label == StepMotorLabel.Z_AXIS:
                    self.z_pos = new_pos
                    self.gui.l_overview_z.setText("z: " + str(new_pos))

    def update_stack_height(self):
        log.info("Update stack height from %d to %d", self.current_stack_height, self.current_stack_height + self.current_object_height)
        self.current_stack_height = self.current_stack_height + self.current_object_height

    def set_object_height(self):
        object_height = self.max_z_steps - self.motor_z.last_steps
        log.info("Set object height : %d", object_height)
        self.current_object_height = object_height

    def check_action_done(self, action):
        if isinstance(action, Action):
            if action.is_done():
                new_action = self.get_current_action()
                if new_action is not None:
                    self.handle_actions(new_action)
                else:
                    self.stop_all()
                    log.warning("DatzuBabel successful stacked?")

    def check_actions(self):
        if self.motor_x.position_reached:
            self.update_position(self.motor_x, self.x_pos)
            action = self.get_current_action()
            if isinstance(action, ActionMoveXY):
                log.warning("current action %d", action.xy[0][0])
                action.set_x_pos_done()
                self.check_action_done(action)
                log.warning("ActionMoveXY - set_x_pos_done")
            elif isinstance(action, ActionPickObject):
                if action.is_z_moved_to_object() and not action.is_at_x_pos():
                    action.set_x_pos_done()
                    log.warning("ActionPickObject - set_x_pos_done")
                    self.check_magnet_adjusted_done(action)

        if self.motor_y.position_reached:
            self.update_position(self.motor_y, self.y_pos)
            action = self.get_current_action()
            if isinstance(action, ActionMoveXY):
                log.warning("current action %d", action.xy[1][0])
                action.set_y_pos_done()
                log.warning("ActionMoveXY - y_pos_done")
                self.check_action_done(action)

        if self.motor_z.position_reached:
            self.update_position(self.motor_z, self.z_pos)
            action = self.get_current_action()
            if isinstance(action, ActionPickObject):
                if action.is_z_moved_to_object() and not action.is_at_z_pos():
                    action.set_z_pos_done()
                    log.warning("ActionPickObject - set_z_pos_done")
                    self.check_magnet_adjusted_done(action)

                if action.is_magnet_adjusted() and not action.is_magnet_activated():
                    action.set_magnet_activated_done()
                    log.warning("ActionPickObject - magnet_activated_done")
                    self.move_to_z(int(self.current_stack_height) + self.offset_z_stack)

                if action.is_magnet_activated() and not action.is_object_picked_up():
                    action.set_object_picked_up()
                    log.warning("ActionPickObject - set_object_picked_up")
                    self.check_action_done(action)

            elif isinstance(action, ActionStackObject):
                if not action.is_moved_to_stack() and not action.is_magnet_deactivated():
                    log.warning("ActionStackObject - moved_to_stack_done")
                    action.set_moved_to_stack_done()
                    self.update_stack_height()
                    # deactivate magnet
                    self.deactivate_magnet()
                    action.set_magnet_deactivated_done()
                    log.warning("ActionStackObject - deactivated_done")
                    # move back
                    self.move_z_to_max()

        if self.motor_z.get_status() == DeviceStatus.STOPPED_AT_MIN:
            action = self.get_current_action()
            if isinstance(action, ActionMeasure):
                if self.object_measure == MeasureDistance.IN_PROGRESS:
                    log.warning("ActionMeasure - MeasureDistance.IN_PROGRESS")
                    self.set_object_height()
                    self.object_measure = MeasureDistance.DONE
                    self.move_z_to_max()

            elif isinstance(action, ActionPickObject):
                if not action.is_z_moved_to_object():
                    self.set_object_height()
                    #self.update_stack_height()
                    log.warning("ActionPickObject - moved_to_object_done")
                    action.set_z_moved_to_object_done()
                    self.adjust_magnet()
                elif action.is_magnet_activated() and not action.is_object_picked_up():
                    self.move_to_z((int(self.current_stack_height) + self.offset_z_stack))

        if self.motor_z.get_status() == DeviceStatus.STOPPED_AT_MAX:
            if self.calibration_status == CalibrationStatus.NOT_ACTIVE:
                action = self.get_current_action()
                if isinstance(action, ActionMeasure):
                    if self.object_measure == MeasureDistance.DONE:
                        log.warning("ActionMeasure - set_object_picked_up")
                        self.object_measure = MeasureDistance.DONE
                        if not action.is_done():
                            self.update_stack_height()
                            action.set_done()
                            self.check_action_done(action)
                elif isinstance(action, ActionStackObject):
                    if action.is_magnet_deactivated() and not action.is_moved_to_z_max():
                        log.warning("ActionStackObject - set_moved_to_z_max_done")
                        action.set_moved_to_z_max_done()
                        action.set_done()
                        self.check_action_done(action)

    def move_to_xy(self, x, y):
        log.debug("current position: x: %d y: %d", self.x_pos, self.y_pos)
        log.debug("move to position x: %d y: %d", x, y)
        rel_x = self.x_pos - x
        rel_y = self.y_pos - y

        if rel_x == 0:
            log.debug("is already on requested x position")
        else:
            rel_x = rel_x * -1
            tid = self.get_arduino1().get_next_transaction_id()
            self.motor_x.move_steps(tid, rel_x)
            self.get_arduino1().execute(tid)

        if rel_y == 0:
            log.debug("is already on requested y position")
        else:
            rel_y = rel_y * -1
            tid = self.get_arduino1().get_next_transaction_id()
            self.motor_y.move_steps(tid, rel_y)
            self.get_arduino1().execute(tid)

    def set_motor_speed(self, gui):
        speed_x = gui.slider_x.value()
        speed_y = gui.slider_y.value()
        speed_z = gui.slider_z.value()

        if speed_x != self.mot_x_speed or speed_y != self.mot_y_speed or speed_z != self.mot_z_speed:
            self.mot_x_speed = speed_x
            self.mot_y_speed = speed_y
            self.mot_z_speed = speed_z
            tid = self.get_arduino1().get_next_transaction_id()
            self.motor_x.set_speed(tid, gui.slider_x.value())
            self.motor_y.set_speed(tid, gui.slider_y.value())
            self.motor_z.set_speed(tid, gui.slider_z.value())
            self.get_arduino1().execute(tid)

    def move_to_z(self, steps):
        # with this method we always go down to min
        log.debug("current position: z: %d", self.z_pos)
        log.debug("move steps: %d", steps)

        tid = self.get_arduino1().get_next_transaction_id()
        self.motor_z.move_steps(tid, steps)
        self.get_arduino1().execute(tid)

    def move_z_to_max(self):
        # with this method we always go up to max
        tid = self.get_arduino1().get_next_transaction_id()
        self.motor_z.run_to_max(tid)
        self.get_arduino1().execute(tid)

    def move_z_to_min(self):
        # with this method we always go down to min
        tid = self.get_arduino1().get_next_transaction_id()
        self.motor_z.run_to_min(tid)
        self.get_arduino1().execute(tid)

    def adjust_magnet(self):
        log.debug("move %d to max for align the magnet", self.z_pos)
        tid = self.get_arduino1().get_next_transaction_id()
        self.motor_x.move_steps(tid, self.offset_x_magnet)
        self.motor_z.move_steps(tid, self.offset_z_magnet)
        self.get_arduino1().execute(tid)

    def measure_distance(self):
        self.object_measure = MeasureDistance.IN_PROGRESS
        tid = self.get_arduino1().get_next_transaction_id()
        self.motor_z.run_to_min(tid)
        self.get_arduino1().execute(tid)

    def activate_magnet(self):
        tid = self.get_arduino1().get_next_transaction_id()
        self.magnet.magnet_on(tid)
        self.get_arduino1().execute(tid)

    def deactivate_magnet(self):
        tid = self.get_arduino1().get_next_transaction_id()
        self.magnet.magnet_off(tid)
        self.get_arduino1().execute(tid)

    def check_magnet_adjusted_done(self, action):
        if action.is_at_position():
            action.set_magnet_adjusted_done()
            log.warning("ActionPickObject - magnet_adjusted_done")
            # activate magnet
            self.activate_magnet()
            self.move_to_z(self.offset_z_magnet * -1)

    def update_gui(self, gui):
        # gui.l_tower_prev.append(self.motor_x.status.value)
        # gui.l_tower_prev.append(str(self.motor_x.new_status))
        if self.motor_x.new_status:
            gui.update_motor(self.motor_x)
            self.motor_x.new_status = False
        if self.motor_y.new_status:
            gui.update_motor(self.motor_y)
            self.motor_y.new_status = False
        if self.motor_z.new_status:
            gui.update_motor(self.motor_z)
            self.motor_z.new_status = False
        if self.magnet.new_status:
            gui.update_magnet_state(self.magnet)
            self.magnet.new_status = False
        # TODO just update gui if device.new_status is set and set device.new_status to False

    def convert_pixel_to_steps(self, xy):
        return [round(xy[0] * self.max_x_steps), round(xy[1] * self.max_y_steps)]

    def convert_mm_to_steps(self, z):
        conv_factor_z = self.max_z_steps / self.max_z_distance
        return round(z * conv_factor_z)

    def __str__(self):
        return "Playfield: referenced: {}".format(self.is_referenced)
