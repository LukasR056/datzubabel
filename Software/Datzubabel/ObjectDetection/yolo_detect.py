import os
import sys
import torch
from PIL import Image
import numpy as np
import pandas as pd
import cv2
from matplotlib import pyplot as plt
from sklearn.cluster import KMeans
import math

os.chdir(r"A:\DAT\Projekt\datzubabelgit2\datzubabel\Software\Datzubabel\ObjectDetection") 

# global variables
#dirname = os.path.dirname(os.path.abspath(__file__))
dirname = "A:\DAT\Projekt\datzubabelgit2\datzubabel\Software\Datzubabel\ObjectDetection"
yolo_dataframe = []



# check if cuda is available on the executing machine
def test_cuda():
    return torch.cuda.is_available()


# load the model with custom weights, the weights are located in folder ./weights'
# if no value is given, we simply take the most recent custom weight
def load_model(weight=None):
    weight_path = os.path.join(dirname, 'weights\\')
    yolo_path = os.path.join(dirname, 'yolo5')

    if weight is None:
        model = torch.hub.load(yolo_path, 'custom', path=weight_path + 'best_pro_500.pt', source='local',
                               force_reload=True)
    else:
        model = torch.hub.load(yolo_path, 'custom', path=weight_path + weight, source='local', force_reload=True)
    return model


# load the image to be used for object detection
def load_img(name):
    path = os.path.join(dirname, 'img\\')
    img = Image.open(path+name)
    return img


def main(img_path, weight=None):
    y_model = load_model(weight)
    img = load_img(img_path)

    result = y_model(img)
    df_result = result.pandas()
   

    print("------------------------------------ "+weight)
    columnsName =  df_result.xyxy[0]
    temp_df = df_result.xyxyn[0].iloc[:, :4]

    #yolo_data = pd.concat([df_result.xywhn[0], df_result.xyxyn[0].iloc[:, :4]], axis=1)

    #print(columnsName)
    #print(temp_df)
    yolo_dataframe = df_result.xyxy[0]
    print(str(yolo_dataframe))
    result.show()


    
    

if __name__ == "__main__":
    img_path = "Bild_363.jpg"
    weight = "best_pro_500.pt" # best_pro_300 funkioniert a bissl besser als best_pro_500
    main(img_path, weight)
    
    
    
    
#### ------------- working from here now
# loading yolo
model = torch.hub.load(os.path.join(dirname, 'yolo5'), 'custom', path= dirname + '\weights\\best_pro_500.pt', source='local', force_reload=True)

y_model = load_model('best_pro_500.pt')
img = cv2.imread('Bild_364.jpg',0) #Bild_363jpg
result = y_model(img)
df_result = result.pandas().xyxy[0]
result.show()

result.pandas().xywh[0]

## needed to get rid of the aruco marker in the corners
xmin = df_result['xmin'].min()
ymin = df_result['ymin'].min()
xmax = df_result['xmax'].max()
ymax = df_result['ymax'].max()


type(ymax)

## Class 2 is prisma only 
# optional idea to interate only over the prisma
df_result.loc[df_result['class'] == 2]

## checked number of objects on pics
len(df_result)

## load image
image = cv2.imread('Bild_364.jpg',0) #Bild_363.jpg

# shape
np.shape(image)

## change to black and white
thresh = 225
im_bw = cv2.threshold(img, thresh, 255, cv2.THRESH_BINARY)[1]


import matplotlib
#%matplotlib inline 
## check if its works
plt.imshow(im_bw)
plt.show()
np.unique(im_bw)
np.shape(im_bw)

## get rid of the aruco markers in the corner, otherwise it would change the outcome of the cluster centers
for idx_x, x in enumerate(im_bw):
  for idx_y, y in enumerate(x):
    if y == 255 and (idx_y < xmin  or idx_y > xmax or idx_x < ymin  or idx_x > ymax ):
        im_bw[idx_x,idx_y] = 0

    
## check if it works
imgplot = plt.imshow(im_bw)
plt.show()


##get the coordinates of each pixel where it is not black, so where a object is
coordinates = np.argwhere(im_bw == 255 )
np.shape(coordinates)

# check if it works
plt.scatter(coordinates[:, 1],coordinates[:, 0])

# invert yaxis because y axes is inverted
scatter = plt.scatter(coordinates[:, 1],coordinates[:, 0])
axes = scatter.axes
axes.invert_yaxis()
scatter.show()


## create the classes
from sklearn.cluster import KMeans

## get clusters, number of cluster needs to be known
kmeans = KMeans(n_clusters=len(df_result))
kmeans.fit(coordinates)
y_kmeans = kmeans.predict(coordinates)

# Achtung auf Spaltenindex
plt.scatter(coordinates[:, 1], coordinates[:, 0], c=y_kmeans, s=50, cmap='viridis')


plt.scatter(centers[:, 1], centers[:, 0], c='black', s=200, alpha=0.5);

scatter = plt.scatter(centers[:, 1], centers[:, 0], c='black', s=200, alpha=0.5);
axes = scatter.axes
axes.invert_yaxis()
scatter.show()

## get coordinates of cluster center
centers = kmeans.cluster_centers_

## format points
print(list(centers))

points_new = []

for idx, point in enumerate(list(centers)):
    points_new.append((point[1],point[0]))

print(points_new)






### update YoloDataframe
temp_yolo_results = result.pandas().xywh[0].copy()

for idx,object_yolo in temp_yolo_results.iterrows():
    yolo_point = (object_yolo[0],object_yolo[1])
    print('CHECK distance FROM YOLO points through all points')
    print(yolo_point)
    distance_temp = 100000
    for i,point in enumerate(points_new):
        if math.dist(yolo_point, point) < distance_temp:
            distance_temp = math.dist(yolo_point, point)
            #print(math.dist(yolo_point, point))
            print('BEST POINT: ' + str(point)+ ' Distanz:' + str(distance_temp))
            temp_yolo_results.loc[idx,'xcenter'] = point[0]
            temp_yolo_results.loc[idx,'ycenter'] = point[1]
            


## TODO normaliseren die werte !
## normalize everything
temp_yolo_results_normalize = temp_yolo_results.copy()

   
         
temp_yolo_results_normalize['xcenter'] =  temp_yolo_results_normalize['xcenter'] / 503
      
temp_yolo_results_normalize['ycenter'] =  temp_yolo_results_normalize['ycenter'] / 269


temp_yolo_results_normalize
result.pandas().xywhn[0]       



plt.scatter(centers[:, 1], centers[:, 0], c='black', s=200, alpha=0.5);

scatter = plt.scatter(centers[:, 1], centers[:, 0], c='black', s=200, alpha=0.5);
axes = scatter.axes
axes.invert_yaxis()
scatter.show()

print(centers)

## yolo5
df_result = result.pandas().xyxy[0]

df_result_threshold = df_result[df_result['confidence'] >= 0.98]
###### write to a function together -- implement this function in the GUI

def optimaze_center(yolo_detect_df,yolo_detect_df_wh,image):
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    image = np.asarray(image)
    pic_x_max = np.shape(image)[1]
    pic_y_max = np.shape(image)[0]
    
    ## create binary image
    thresh = 225
    im_bw = cv2.threshold(image, thresh, 255, cv2.THRESH_BINARY)[1]
    
    ## get objects
    xmin = yolo_detect_df['xmin'].min()
    ymin = yolo_detect_df['ymin'].min()
    xmax = yolo_detect_df['xmax'].max()
    ymax = yolo_detect_df['ymax'].max()
    
    ## remove white corners which came from the aruco marks
    for idx_x, x in enumerate(im_bw):
        for idx_y, y in enumerate(x):
            if y == 255 and (idx_y < xmin  or idx_y > xmax or idx_x < ymin  or idx_x > ymax ):
                im_bw[idx_x,idx_y] = 0
                
    coordinates = np.argwhere(im_bw == 255 )
    
    
    kmeans = KMeans(n_clusters=len(yolo_detect_df))
    kmeans.fit(coordinates)
    
    centers = kmeans.cluster_centers_
    
    ## reformating the new center points
    points_new = []
    
    for idx, point in enumerate(list(centers)):
        points_new.append((point[1],point[0]))
    
        
    for idx,object_yolo in yolo_detect_df_wh.iterrows():
        yolo_point = (object_yolo[0],object_yolo[1])
        # print('CHECK distance FROM YOLO points through all points')
        # print(yolo_point)
        distance_temp = 100000
        for i,point in enumerate(points_new):
            if math.dist(yolo_point, point) < distance_temp:
                distance_temp = math.dist(yolo_point, point)
                # print(math.dist(yolo_point, point))
                # print('BEST POINT: ' + str(point)+ ' Distanz:' + str(distance_temp))
                yolo_detect_df_wh.loc[idx,'xcenter'] = point[0]
                yolo_detect_df_wh.loc[idx,'ycenter'] = point[1]
         
    ## normalize values
    yolo_detect_df_wh['xcenter'] =  yolo_detect_df_wh['xcenter'] / pic_x_max
      
    yolo_detect_df_wh['ycenter'] =  yolo_detect_df_wh['ycenter'] / pic_y_max

    print(yolo_detect_df_wh)
    
    
    
# testen


im = cv2.imread('Bild_364.jpg') #Bild_363.jpg
y_model = load_model('best_pro_500.pt')
result = y_model(im)
df_result = result.pandas().xywhn[0]



optimaze_center(result.pandas().xyxy[0],result.pandas().xywh[0] ,im)





print(im)
type(im)
np.shape(im)
    
im = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
result.pandas().xywhn[0]


