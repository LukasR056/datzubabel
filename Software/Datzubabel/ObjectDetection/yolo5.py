import os
import sys
import torch
import pandas as pd
import numpy as np
from sklearn.cluster import KMeans
import math
import cv2
from matplotlib import pyplot as plt


class Yolo5:
    def __init__(self, weight):
        self.weight = weight
        self.weight_path = "./ObjectDetection/weights/"
        self.yolo_path = "./ObjectDetection/yolo5/"
        self.detection = None
        self.df_detection = None
        self.df_detection_normalized = None
        # load model with custom weights, the weights are located in folder ./ObjectDetection/weights'
        # if no value is given, we simply take the most recent custom weight
        if weight is None:
            self.model = torch.hub.load(self.yolo_path, 'custom', path=self.weight_path + 'best_pro_500.pt',
                                        source='local', force_reload=True)
        else:
            self.model = torch.hub.load(self.yolo_path, 'custom', path=self.weight_path + weight, source='local',
                                        force_reload=True)

    # xywhn ist normalisiert weil für den Klickmodus es so benötigt wird.
    # xyxy ist nicht nomralisiert weil Funktion für analytischen Ansatz es so braucht
    def detect_objects(self, img):
        self.detection = self.model(img)
        # self.df_detection = self.detection.pandas().xyxy[0]
        # self.df_detection_normalized = self.detection.pandas().xyxyn[0]
        yoloframe = [self.detection.pandas().xywhn[0],
                     self.detection.pandas().xyxyn[0].iloc[:, :4],
                     self.detection.pandas().xyxy[0].iloc[:, :4].rename(
                         columns={'xmin': 'xmin_abs', 'ymin': 'ymin_abs', 'xmax': 'xmax_abs', 'ymax': 'ymax_abs'}),
                     self.detection.pandas().xywh[0].iloc[:,:2].rename(columns= { 'xcenter' : 'xcenter_abs' , 'ycenter' : 'ycenter_abs' })]
        self.df_detection_normalized = pd.concat(yoloframe, axis=1)

        # Ausgeben aller erkannten Objekten
        #print("All objects")
        #print(len(self.df_detection_normalized))
        #print(self.df_detection_normalized)

        ## Alle Objekte, die eine confidence von unter 0.8 haben, werden zwar angezeigt mit Bounding boxen auf dem Bild,
        ## aber nicht im Yolo df aufgenommen.
        self.df_detection_normalized = self.df_detection_normalized[self.df_detection_normalized["confidence"] >= 0.8]

        # Ausgeben aller Objekten nach confidence threshold
        print("Sure objects")
        print(len(self.df_detection_normalized))
        print(self.df_detection_normalized)


        # print('--- NEW YOLO ---')
        df_optimize_center = self.optimize_center(self.df_detection_normalized, img)

        # print('--- OLD YOLO ---')
        # print(self.df_detection_normalized)

        self.df_detection_normalized["xcenter"] = df_optimize_center["xcenter"]
        self.df_detection_normalized["ycenter"] = df_optimize_center["ycenter"]

        self.df_detection_normalized["ycenter"] = 1 - self.df_detection_normalized["ycenter"]

    # check if cuda is available on the executing machine
    def test_cuda(self):
        return torch.cuda.is_available()

    def optimize_center(self, yolo_detect_df, image):
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        image = np.asarray(image)
        pic_x_max = np.shape(image)[1]
        pic_y_max = np.shape(image)[0]



        ## Reset der benötigen Variabeln
        coordinates = None
        kmeans = None
        centers = None
        y_kmeans = None

        
        # create binary image
        thresh = 225
        im_bw = cv2.threshold(image, thresh, 255, cv2.THRESH_BINARY)[1]
        
        # get objects
        xmin = yolo_detect_df['xmin_abs'].min()
        ymin = yolo_detect_df['ymin_abs'].min()
        xmax = yolo_detect_df['xmax_abs'].max()
        ymax = yolo_detect_df['ymax_abs'].max()
        
        # remove white corners which came from the aruco marks
        for idx_x, x in enumerate(im_bw):
            for idx_y, y in enumerate(x):
                if y == 255 and (idx_y < xmin or idx_y > xmax or idx_x < ymin or idx_x > ymax):
                    im_bw[idx_x, idx_y] = 0
                    
        coordinates = np.argwhere(im_bw == 255 )

        print("len of df")
        print(len(yolo_detect_df))

        ## Berechnen der Clusterpunkte
        kmeans = KMeans(n_clusters=len(yolo_detect_df))
        kmeans.fit(coordinates)
        centers = kmeans.cluster_centers_

        # Debugging falls optimieren des Mittelpunktes fehlgeschlagen ist
        print("check cluster plot")
        y_kmeans = kmeans.predict(coordinates)
        plt.scatter(coordinates[:, 1], coordinates[:, 0], c=y_kmeans, s=50, cmap='viridis')
        plt.scatter(centers[:, 1], centers[:, 0], c='black', s=200, alpha=0.5);
        plt.savefig("ObjectDetection/check_cluster_center.png")

        # reformating the new center points
        points_new = []
        
        for idx, point in enumerate(list(centers)):
            points_new.append((point[1], point[0]))

        print(" all detected centers")
        print(points_new)
            
        for idx, object_yolo in yolo_detect_df.iterrows():
            yolo_point = (object_yolo['xcenter_abs'], object_yolo['ycenter_abs'])
            # print('CHECK distance FROM YOLO points through all points')
            # print(yolo_point)
            distance_temp = 100000
            for i, point in enumerate(points_new):
                if math.dist(yolo_point, point) < distance_temp:
                    distance_temp = math.dist(yolo_point, point)
                    # print(math.dist(yolo_point, point))
                    # print('BEST POINT: ' + str(point)+ ' Distanz:' + str(distance_temp))
                    yolo_detect_df.loc[idx, 'xcenter_abs'] = point[0]
                    yolo_detect_df.loc[idx, 'ycenter_abs'] = point[1]
             
        # normalize values
        yolo_detect_df['xcenter'] = yolo_detect_df['xcenter_abs'] / pic_x_max
        yolo_detect_df['ycenter'] = yolo_detect_df['ycenter_abs'] / pic_y_max

        print("ergebnis der optimize")
        print(yolo_detect_df)
    
        return yolo_detect_df
        
    
    
    
    
    
