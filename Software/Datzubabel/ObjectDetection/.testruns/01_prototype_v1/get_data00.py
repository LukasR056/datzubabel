import os
import torch
from PIL import Image

print("Cuda available: ",torch.cuda.is_available())
print("Current Working Directory", os.getcwd())

# Model
#model = torch.hub.load('ultralytics/yolov5', 'yolov5s')  # with default weight, see also yolov5m, yolov5l, yolov5x, custom
model = torch.hub.load('ultralytics/yolov5', 'custom', path='./weights/bestv3.pt', force_reload=True)

img = Image.open("./img/testimg2.JPG")

results = model(img)

# Results
print(results.pandas().xyxy[0])
results.print()  # or .show(), .save(), .crop(), .pandas(), etc.
#results.save()
results.show()

