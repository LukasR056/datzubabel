import os
import torch


print("Cuda available: ",torch.cuda.is_available())
print("Current Working Directory", os.getcwd())

# Model
#model = torch.hub.load('ultralytics/yolov5', 'yolov5s')  # with default weight, see also yolov5m, yolov5l, yolov5x, custom
model = torch.hub.load('ultralytics/yolov5', 'custom', path='data/custom_best_example.pt', force_reload=True)

img = 'https://ultralytics.com/images/zidane.jpg'  # example image... or file, Path, PIL, OpenCV, numpy, list

# Inference
results = model(img)

# Results
results.print()  # or .show(), .save(), .crop(), .pandas(), etc.
results.save()
results.show()


print(results.pandas().xyxy[0])