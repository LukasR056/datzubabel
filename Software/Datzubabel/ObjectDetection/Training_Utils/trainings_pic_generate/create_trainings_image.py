import os
import numpy as np
import cv2
from cv2 import aruco
import matplotlib as mpl
import matplotlib.pyplot as plt

def load_images_from_folder(folder):
    images = []
    for filename in os.listdir(folder):
        img = cv2.imread(os.path.join(folder,filename))
        if img is not None:
            images.append(img)
    return images

images = load_images_from_folder("original")

print(len(images))


counter = 1
for img in images:

    # gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_50)
    parameters = aruco.DetectorParameters_create()
    corners, ids, _ = aruco.detectMarkers(img, aruco_dict, parameters=parameters)
    frame_markers = aruco.drawDetectedMarkers(img.copy(), corners, ids)

    if len(corners) == 4:

        # Sort array, such that upper left point is first, upper right is second, etc.
        sort_id = np.argsort(sum(ids.tolist(), []))
        sorted_corners = np.empty(np.shape(corners))
        for idx, item in enumerate(sort_id):
            sorted_corners[idx] = corners[item]

        pt_A = sorted_corners[0][0][0]  # upper left point
        pt_D = sorted_corners[1][0][0]  # upper right point
        pt_C = sorted_corners[2][0][0]  # lower right point
        pt_B = sorted_corners[3][0][0]  # lower left point

        # Calculate transformation matrix for image transformation
        width_AD = np.sqrt(((pt_A[0] - pt_D[0]) ** 2) + ((pt_A[1] - pt_D[1]) ** 2))
        width_BC = np.sqrt(((pt_B[0] - pt_C[0]) ** 2) + ((pt_B[1] - pt_C[1]) ** 2))
        maxWidth = max(int(width_AD), int(width_BC))
        height_AB = np.sqrt(((pt_A[0] - pt_B[0]) ** 2) + ((pt_A[1] - pt_B[1]) ** 2))
        height_CD = np.sqrt(((pt_C[0] - pt_D[0]) ** 2) + ((pt_C[1] - pt_D[1]) ** 2))
        maxHeight = max(int(height_AB), int(height_CD))

        input_pts = np.float32([pt_A, pt_B, pt_C, pt_D])
        output_pts = np.float32([[0, 0],
                                 [0, maxHeight - 1],
                                 [maxWidth - 1, maxHeight - 1],
                                 [maxWidth - 1, 0]])

        M = cv2.getPerspectiveTransform(input_pts, output_pts)
        frame = cv2.warpPerspective(img, M, (maxWidth, maxHeight), flags=cv2.INTER_LINEAR)

        # plt.figure()
        # plt.imshow(frame, origin="upper")
        # plt.show()
        cv2.imwrite("cropped/"+ "Bild_" + str(counter) +".jpg", frame)
        counter = counter + 1

print("fertig")