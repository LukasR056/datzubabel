This project was created by Janik Regula and Lukas Reitbauer within "Neuronale
Netze II: Deep Learning" (Data and Information Science - DAT20) 

The aim of the project was creating a multiclass semantic segmentation model 
(using a UNet structure). Further we created a script to generate our own 
colored masks (each color = one class). Those are necessary to train and test 
the model.

The model_summary.txt contains the model summary as well as the progress of
fitting the model. 925 images were used for 4 classes (including the floor).
	--> Cuboid, Prism, Cylinder

IMPORTANT DISCLAIMER:
Paths have to be reset in order to make the scripts work.