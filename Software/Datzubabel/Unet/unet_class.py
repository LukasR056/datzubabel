import matplotlib.pyplot as plt
from Unet.simple_multi_unet_model import multi_unet_model
from tensorflow.keras.utils import normalize
import numpy as np
import pandas as pd
import cv2
from sklearn.cluster import KMeans

class Unet:
    def __init__(self, weight="multiclass_unet_04.hdf5", n_classes=4, img_width=512, img_height=256, img_channels=1):
        self.weight_path = "Unet/" + weight
        self.img = None
        self.img_df = None
        self.model = multi_unet_model(n_classes=n_classes, IMG_HEIGHT=img_height, IMG_WIDTH=img_width,
                                      IMG_CHANNELS=img_channels)
        self.model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
        self.model.load_weights(self.weight_path)

    def predict_image(self, img):
        height, width, channel = img.shape
        img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        img_gray = cv2.resize(img_gray, (512, 256), interpolation=cv2.INTER_NEAREST)

        train_image = np.expand_dims(img_gray, axis=2)
        train_image = normalize(train_image, axis=0)    # oder 0
        train_image = np.expand_dims(train_image, axis=0)

        pred = self.model.predict(train_image)  # needs 4 channeled image (1, height, width, 1)
        self.img = np.argmax(pred, axis=3)[0, :, :]
        self.img = cv2.resize(self.img, (width, height), interpolation=cv2.INTER_NEAREST)
        self.img = np.expand_dims(self.img * 64, axis=2)
        # create 3-channeled image
        # self.img = cv2.cvtColor(self.img, cv2.COLOR_GRAY2RGB)
        self.img = cv2.merge([self.img, self.img, self.img])
        # write and read image to avoid QImage conversion error
        # (searched multiple hours for bugfix | QImage doc hardly existent)
        cv2.imwrite("temp.png", self.img)
        self.img = cv2.imread("temp.png")
        self.img_one_channel = self.img[:, :, 0]
        self.create_unet_df()

    def create_unet_df(self):
        obj_classes = np.unique(self.img)[1:]
        if obj_classes.size == 0:
            self.img_df = None
            return
        coordinates = np.argwhere((self.img_one_channel == 64) | (self.img_one_channel == 128) |
                                  (self.img_one_channel == 192))
        # create the class, number of cluster needs to be known
        kmeans = KMeans(n_clusters=len(obj_classes))
        kmeans.fit(coordinates)
        # get coordinates of cluster centers
        centers = kmeans.cluster_centers_
        self.img_df = pd.DataFrame(columns=["class", "name", "xcenter", "ycenter"])
        for idx, center in enumerate(centers):
            center_x = center[1]
            center_y = center[0]
            center_class = self.img_one_channel[int(center_y), int(center_x)]
            class_name = {
                64: "cuboid",
                128: "cylinder",
                192: "prism"
            }.get(center_class, "")
            self.img_df.loc[idx] = [int(center_class), class_name, center_x / self.img.shape[1],
                                    1 - (center_y / self.img.shape[0])]
        # print("PRINT DEBUG DF: " )
        # print(self.img_df)
        # necessary as for loop mixes center_classes
        # self.img_df.loc[self.img_df["name"] == "cuboid", "class"] = 64
        # self.img_df.loc[self.img_df["name"] == "cylinder", "class"] = 128
        # self.img_df.loc[self.img_df["name"] == "prism", "class"] = 192
        print(self.img_df)
        print(centers)
        # # get clusters
        # y_kmeans = kmeans.predict(coordinates)
        # # Achtung auf Spaltenindex
        # plt.scatter(coordinates[:, 1], coordinates[:, 0], c=y_kmeans, s=50, cmap='viridis')
        # # format points
        # print(list(centers))

        # xcenter ycenter class






