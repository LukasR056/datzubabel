import os
import cv2
import matplotlib.image as mpimg
from PIL import Image
import pandas as pd
import numpy as np
# import tensorflow as tf
import matplotlib.pyplot as plt


def main():
    # create tuple object of images and labels
    data = zip(sorted(os.listdir("Unet\\images")), sorted(os.listdir("Unet\\labels")))
    # function for creating black and white masks (although in PIL short and handsome)
    func = lambda x: 255 if x > threshold else 0
    threshold = 130
    # iteration over zip object
    for img, label in data:
        img_el = Image.open(os.path.join("Unet\\images", img))
        # map pixel to grayscale values and execute func for every point (= pixel)
        mask = img_el.convert("L").point(func)
        # convert PIL image to np array
        np_mask = np.array(mask, dtype=np.int32)

        # load label df for coloring masks
        try:
            # try/except block as mask can contain only the ground in which case the csv is empty
            label_df = pd.read_csv(os.path.join("Unet\\labels", label), sep=" ", header=None)
        except pd.io.common.EmptyDataError:
            # show image for debugging
            plt.imshow(np_mask)
            plt.show()
            # save image (empty mask in that case)
            cv2.imwrite(os.path.join("Unet\\colored_masks", (img + ".png")), np_mask)
            continue

        # prepare label df
        label_df.columns = ["class", "xcenter", "ycenter", "width", "heigth"]
        label_df["xcenter"] = label_df["xcenter"] * np_mask.shape[1]
        label_df["ycenter"] = label_df["ycenter"] * np_mask.shape[0]
        label_df["width"] = label_df["width"] * np_mask.shape[1]
        label_df["heigth"] = label_df["heigth"] * np_mask.shape[0]
        label_df["xstart"] = label_df["xcenter"] - label_df["width"] / 2
        label_df["xend"] = label_df["xcenter"] + label_df["width"] / 2
        label_df["ystart"] = label_df["ycenter"] - label_df["heigth"] / 2
        label_df["yend"] = label_df["ycenter"] + label_df["heigth"] / 2

        # algorithm to change color of masks
        for mask_idx, mask_val in label_df.iterrows():
            for row_idx, row_val in enumerate(np_mask):
                row_idx = row_idx + 1
                for col_idx, col_val in enumerate(row_val):
                    col_idx = col_idx + 1
                    if mask_val["ystart"] <= row_idx <= mask_val["yend"] and \
                            mask_val["xstart"] <= col_idx <= mask_val["xend"] and \
                            col_val == 255:
                        np_mask[row_idx - 1, col_idx - 1] = (mask_val["class"] + 1) * 64
                        col_val = 50    # change col_val to prevent trigger of 2nd if
                    if mask_idx == len(label_df)-1 and \
                            col_val == 255:
                        np_mask[row_idx - 1, col_idx - 1] = 0

        # show for debugging
        # print(np_mask.shape)
        print(np.unique(np_mask))
        # plt.imshow(np_mask, interpolation="nearest")
        plt.imshow(np_mask)
        plt.show()

        # save image
        # adapting jpg quality to 100% to decrease loss of compression
        # cv2.imwrite(os.path.join("Unet\\test_masks", img), np_mask, [cv2.IMWRITE_JPEG_QUALITY, 100])
        # changing to png for better lossless compression
        cv2.imwrite(os.path.join("Unet\\colored_masks", (img + ".png")), np_mask)


if __name__ == "__main__":
    main()
