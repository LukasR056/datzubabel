# PyQt file structure documentation 

- ``gui.py`` contains the actual gui class which is called in ``__main__.py``
- Design / layout for the PyQt GUI is in ``gui_layout.py``  
- ``first_designer_gui.py`` and ``main_window-py`` are an additional debug-files
- ``images`` contains necessary images such as ``stepmotor.png`` which are shown within the gui
- ``camera.py`` contains the ``Camera`` class which is necessary for making the images and track the aruco markers  
