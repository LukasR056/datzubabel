import sys

from PyQt5.QtWidgets import (QWidget, QPushButton, QLabel, QFrame, QGridLayout, QSizePolicy,
                             QTextEdit, QComboBox, QMessageBox, QStyleFactory, QSlider, QScrollArea,
                             QLayout, QHBoxLayout, QVBoxLayout, QStackedLayout, QMenuBar)
from PyQt5.QtGui import QIcon, QPixmap, QImage
from PyQt5.QtCore import Qt, QRect, QEvent, QSize


def init_gui_layout(self):
    # Live Webcam
    self.img_live = QLabel()
    self.img_live.installEventFilter(self)      # necessary for on-click event
    self.img_live.setFrameShape(QFrame.Box)
    self.img_live.setAlignment(Qt.AlignCenter)
    # self.img_live.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
    self.img_live.setScaledContents(True)

    # Images
    motor_pixmap = QPixmap("./GUI/images/stepmotor.png")
    motor_pixmap = motor_pixmap.scaled(110, 110, aspectRatioMode=Qt.KeepAspectRatio)
    self.green_pixmap = QPixmap("./GUI/images/green_circle.png")
    self.green_pixmap = self.green_pixmap.scaled(18, 18, aspectRatioMode=Qt.KeepAspectRatio)

    self.red_pixmap = QPixmap("./GUI/images/red_circle.png")
    self.red_pixmap = self.red_pixmap.scaled(18, 18, aspectRatioMode=Qt.KeepAspectRatio)

    self.black_pixmap = QPixmap("./GUI/images/black_circle.png")
    self.black_pixmap = self.black_pixmap.scaled(16, 16, aspectRatioMode=Qt.KeepAspectRatio)


    # Live Overview in Webcam
    self.layout_overview_live = QVBoxLayout(self.img_live)
    self.layout_overview_live.setAlignment(Qt.AlignRight | Qt.AlignTop)
    self.layout_overview_live.setSpacing(0)
    self.l_overview_x = QLabel("x: 0")
    self.l_overview_x.setStyleSheet("background-color: #f0f0f0; padding: 10px 20px 5px 10px; "
                                    "border-top: 1px solid black; border-left: 1px solid black; "
                                    "border-right: 1px solid black")
    self.l_overview_y = QLabel("y: 0")
    self.l_overview_y.setStyleSheet("background-color: #f0f0f0; padding: 5px 20px 5px 10px; "
                                    "border-left: 1px solid black; border-right: 1px solid black")
    self.l_overview_z = QLabel("z: 0")
    self.l_overview_z.setStyleSheet("background-color: #f0f0f0; padding: 5px 20px 5px 10px; "
                                    "border-left: 1px solid black; border-right: 1px solid black")
    self.l_overview_magnet = QHBoxLayout()
    self.l_overview_magnet_label = QLabel("Magnet:")
    self.l_overview_magnet_label.setStyleSheet("background-color: #f0f0f0; padding: 5px 2px 10px 10px; "
                                               "border-bottom: 1px solid black; border-left: 1px solid black;")
    self.l_overview_magnet_img = QLabel()
    self.l_overview_magnet_img.setStyleSheet("background-color: #f0f0f0; padding: 5px 20px 10px 2px; "
                                             "border-bottom: 1px solid black; border-right: 1px solid black")
    self.l_overview_magnet_img.setPixmap(self.red_pixmap)

    self.l_overview_magnet.addWidget(self.l_overview_magnet_label)
    self.l_overview_magnet.addWidget(self.l_overview_magnet_img)

    self.layout_overview_live.addWidget(self.l_overview_x)
    self.layout_overview_live.addWidget(self.l_overview_y)
    self.layout_overview_live.addWidget(self.l_overview_z)
    self.layout_overview_live.addLayout(self.l_overview_magnet)

    # Output Motor Images
    self.img_motor_x = QLabel()
    self.img_motor_x.setAlignment(Qt.AlignCenter | Qt.AlignHCenter)
    self.img_motor_x.setPixmap(motor_pixmap)
    # img_motor_x.setGeometry(QRect(0, 0, 10, 10))
    # self.img_motor_x.setFrameShape(QFrame.Box)
    # self.img_motor_x.setScaledContents(True)

    self.img_motor_y = QLabel()
    self.img_motor_y.setAlignment(Qt.AlignCenter | Qt.AlignHCenter)
    self.img_motor_y.setPixmap(motor_pixmap)

    self.img_motor_z = QLabel()
    self.img_motor_z.setAlignment(Qt.AlignCenter | Qt.AlignHCenter)
    self.img_motor_z.setPixmap(motor_pixmap)

    # Output Motor x-Achse
    self.layout_motor_x = QGridLayout()
    self.mx_active = QLabel()
    self.mx_active.setPixmap(self.black_pixmap)
    self.mx_min = QLabel()
    self.mx_min.setPixmap(self.black_pixmap)
    self.mx_max = QLabel()
    self.mx_max.setPixmap(self.black_pixmap)
    self.mx_speed = QLabel("0")

    self.layout_motor_x.addWidget(QLabel("Motor x-Achse"),      0, 0, 1, 2)
    self.layout_motor_x.addWidget(QLabel("Aktiv"),              1, 0, 1, 1)
    self.layout_motor_x.addWidget(self.mx_active,               1, 1, 1, 1)
    self.layout_motor_x.addWidget(QLabel("Min"),                2, 0, 1, 1)
    self.layout_motor_x.addWidget(self.mx_min,                  2, 1, 1, 1)
    self.layout_motor_x.addWidget(QLabel("Max"),                3, 0, 1, 1)
    self.layout_motor_x.addWidget(self.mx_max,                  3, 1, 1, 1)
    self.layout_motor_x.addWidget(QLabel("Geschwindigkeit"),    4, 0, 1, 1)
    self.layout_motor_x.addWidget(self.mx_speed,                4, 1, 1, 1)

    # Output Motor y-Achse
    self.layout_motor_y = QGridLayout()
    self.my_active = QLabel()
    self.my_active.setPixmap(self.black_pixmap)
    self.my_min = QLabel()
    self.my_min.setPixmap(self.black_pixmap)
    self.my_max = QLabel()
    self.my_max.setPixmap(self.black_pixmap)
    self.my_speed = QLabel("0")

    self.layout_motor_y.addWidget(QLabel("Motor y-Achse"),      0, 0, 1, 2)
    self.layout_motor_y.addWidget(QLabel("Aktiv"),              1, 0, 1, 1)
    self.layout_motor_y.addWidget(self.my_active,               1, 1, 1, 1)
    self.layout_motor_y.addWidget(QLabel("Min"),                2, 0, 1, 1)
    self.layout_motor_y.addWidget(self.my_min,                  2, 1, 1, 1)
    self.layout_motor_y.addWidget(QLabel("Max"),                3, 0, 1, 1)
    self.layout_motor_y.addWidget(self.my_max,                  3, 1, 1, 1)
    self.layout_motor_y.addWidget(QLabel("Geschwindigkeit"),    4, 0, 1, 1)
    self.layout_motor_y.addWidget(self.my_speed,                4, 1, 1, 1)

    # Output Motor z-Achse
    self.layout_motor_z = QGridLayout()
    self.mz_active = QLabel()
    self.mz_active.setPixmap(self.black_pixmap)
    self.mz_min = QLabel()
    self.mz_min.setPixmap(self.black_pixmap)
    self.mz_max = QLabel()
    self.mz_max.setPixmap(self.black_pixmap)
    self.mz_speed = QLabel("0")

    self.layout_motor_z.addWidget(QLabel("Motor z-Achse"),      0, 0, 1, 2)
    self.layout_motor_z.addWidget(QLabel("Aktiv"),              1, 0, 1, 1)
    self.layout_motor_z.addWidget(self.mz_active,               1, 1, 1, 1)
    self.layout_motor_z.addWidget(QLabel("Min"),                2, 0, 1, 1)
    self.layout_motor_z.addWidget(self.mz_min,                  2, 1, 1, 1)
    self.layout_motor_z.addWidget(QLabel("Max"),                3, 0, 1, 1)
    self.layout_motor_z.addWidget(self.mz_max,                  3, 1, 1, 1)
    self.layout_motor_z.addWidget(QLabel("Geschwindigkeit"),    4, 0, 1, 1)
    self.layout_motor_z.addWidget(self.mz_speed,                4, 1, 1, 1)

    # Output Tower preview
    self.layout_tower_preview = QVBoxLayout()
    self.l_tower_prev_label = QLabel("Turmvorschau")
    self.l_tower_prev = QTextEdit()
    self.l_tower_prev.setReadOnly(True)
    self.l_tower_prev.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
    self.l_tower_prev.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
    self.layout_tower_preview.addWidget(self.l_tower_prev_label,    stretch=20)
    self.layout_tower_preview.addWidget(self.l_tower_prev,          stretch=90)

    # Output List detected objects
    self.layout_logging = QVBoxLayout()
    self.l_log_label = QLabel("Programmausgabe")
    self.l_log = QTextEdit()
    self.l_log.setReadOnly(True)
    self.l_log.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
    self.l_log.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
    self.layout_logging.addWidget(self.l_log_label, stretch=20)
    self.layout_logging.addWidget(self.l_log, stretch=90)


    # Input Section
    # self.layout
    self.b_initialize = QPushButton("Hardware Referenzieren")
    self.b_initialize.clicked.connect(self.reference)
    self.dropdown_mode = QComboBox()
    self.dropdown_mode.addItems(["Gamepad", "Klick Modus", "Automatisierter Modus"])
    self.dropdown_mode.activated.connect(self.switch_page)

    ### Stacked Layout for Game Mode
    self.stacked = QWidget()
    self.stacked_layout = QStackedLayout()

    self.page_gamepad = QWidget()

    self.page_click = QWidget()
    self.layout_page_click = QVBoxLayout()
    self.p_c_detect = QPushButton("Detektieren")
    self.p_c_detect.clicked.connect(self.crop_image)
    self.p_c_reset = QPushButton("Zurücksetzen")
    self.p_c_reset.clicked.connect(self.show_live_image)
    self.p_c_start = QPushButton("Start")
    self.p_c_start.clicked.connect(self.start_building_click_mode)
    self.layout_page_click.addWidget(self.p_c_detect)
    self.layout_page_click.addWidget(self.p_c_reset)
    self.layout_page_click.addWidget(self.p_c_start)
    # self.layout_page_click.addStretch(0)
    self.page_click.setLayout(self.layout_page_click)

    self.page_ki = QWidget()
    self.layout_page_ki = QVBoxLayout()
    self.p_ki_dropdown = QComboBox()
    self.p_ki_dropdown.addItems(["Fläche + Gewicht pro Objekt", "Gewicht pro Objekt", "Umgekehrt", "(Simulation)"])
    self.p_ki_dropdown_area_calc = QComboBox()
    self.p_ki_dropdown_area_calc.addItems(["Bounding Box", "Farbverteilung"])
    self.p_ki_detect = QPushButton("Detektieren")
    self.p_ki_detect.clicked.connect(self.crop_image)
    self.p_ki_reset = QPushButton("Zurücksetzen")
    self.p_ki_reset.clicked.connect(self.show_live_image)
    self.p_ki_review = QPushButton("Vorschau")
    self.p_ki_review.clicked.connect(self.review_building_ki_mode)
    self.p_ki_start = QPushButton("Start")
    self.p_ki_start.clicked.connect(self.start_building_ki_mode)
    # self.layout_page_ki.setSpacing(0)
    # self.layout_page_ki.stretch(0)
    self.layout_page_ki.addWidget(QLabel("Fitnessberechnung"))
    self.layout_page_ki.addWidget(self.p_ki_dropdown)
    self.layout_page_ki.addWidget(QLabel("Flächenberechnung"))
    self.layout_page_ki.addWidget(self.p_ki_dropdown_area_calc)
    self.layout_page_ki.addWidget(self.p_ki_detect)
    self.layout_page_ki.addWidget(self.p_ki_reset)
    self.layout_page_ki.addWidget(self.p_ki_review)
    self.layout_page_ki.addWidget(self.p_ki_start)
    self.page_ki.setLayout(self.layout_page_ki)

    self.stacked_layout.addWidget(self.page_gamepad)
    self.stacked_layout.addWidget(self.page_click)
    self.stacked_layout.addWidget(self.page_ki)
    self.stacked.setLayout(self.stacked_layout)

    # Motor Speed slider
    self.layout_speed_x = QHBoxLayout()
    self.slider_x = QSlider(Qt.Horizontal)
    self.slider_x.setRange(0, 6)
    self.slider_x.setValue(6)
    self.slider_x.setFocusPolicy(Qt.NoFocus)
    self.slider_x.setPageStep(1)
    self.slider_x.valueChanged.connect(self.update_speed_x)
    self.slider_x_ret = QLabel("6")
    self.layout_speed_x.addWidget(QLabel("x-Achse"), stretch=20)
    self.layout_speed_x.addWidget(self.slider_x, stretch=70)
    self.layout_speed_x.addWidget(self.slider_x_ret, stretch=10)

    self.layout_speed_y = QHBoxLayout()
    self.slider_y = QSlider(Qt.Horizontal)
    self.slider_y.setRange(0, 6)
    self.slider_y.setValue(6)
    self.slider_y.setFocusPolicy(Qt.NoFocus)
    self.slider_y.setPageStep(1)
    self.slider_y.valueChanged.connect(self.update_speed_y)
    self.slider_y_ret = QLabel("6")
    self.layout_speed_y.addWidget(QLabel("y-Achse"), stretch=20)
    self.layout_speed_y.addWidget(self.slider_y, stretch=70)
    self.layout_speed_y.addWidget(self.slider_y_ret, stretch=10)

    self.layout_speed_z = QHBoxLayout()
    self.slider_z = QSlider(Qt.Horizontal)
    self.slider_z.setRange(0, 6)
    self.slider_z.setValue(6)
    self.slider_z.setFocusPolicy(Qt.NoFocus)
    self.slider_z.setPageStep(1)
    self.slider_z.valueChanged.connect(self.update_speed_z)
    self.slider_z_ret = QLabel("6")
    self.layout_speed_z.addWidget(QLabel("z-Achse"), stretch=20)
    self.layout_speed_z.addWidget(self.slider_z, stretch=70)
    self.layout_speed_z.addWidget(self.slider_z_ret, stretch=10)


    self.layout_img_input = QHBoxLayout()
    self.layout_input_right = QVBoxLayout()
    self.layout_input_right.setSpacing(20)
    self.layout_input_right.addWidget(self.b_initialize)
    self.layout_input_right.addWidget(self.dropdown_mode)
    self.layout_input_right.addWidget(self.stacked)
    self.layout_input_right.addWidget(QLabel("Motorgeschwindigkeit"))
    self.layout_input_right.addLayout(self.layout_speed_x)
    self.layout_input_right.addLayout(self.layout_speed_y)
    self.layout_input_right.addLayout(self.layout_speed_z)
    self.layout_input_right.addStretch(1)

    self.layout_img_input.addWidget(self.img_live, stretch=80)
    self.layout_img_input.addLayout(self.layout_input_right, stretch=20)

    # menubar
    self.menubar = QMenuBar()
    self.mbar_opt = self.menubar.addMenu("Optionen")
    self.mbar_opt.addAction("Kalibrieren").triggered.connect(self.calibrate_playfield)
    self.mbar_opt.addAction("Logging").triggered.connect(self.open_log_file)
    self.mbar_opt.addAction("Konfiguration").triggered.connect(self.open_json_file)
    self.mbar_opt.addSeparator()
    self.mbar_opt.addAction("Beenden").triggered.connect(self.close_gui)
    self.mbar_help = self.menubar.addMenu("Hilfe")
    self.mbar_help.addAction("Readme").triggered.connect(self.open_readme)
    self.mbar_help.addAction("Über das Projekt").triggered.connect(self.open_about)


    ### MAIN Layout
    main = QVBoxLayout()
    main.setSpacing(10)

    self.layout_output = QHBoxLayout()
    self.layout_output.addWidget(self.img_motor_x,              stretch=9)
    self.layout_output.addLayout(self.layout_motor_x,           stretch=10)
    self.layout_output.addWidget(self.img_motor_y,              stretch=9)
    self.layout_output.addLayout(self.layout_motor_y,           stretch=10)
    self.layout_output.addWidget(self.img_motor_z,              stretch=9)
    self.layout_output.addLayout(self.layout_motor_z,           stretch=10)
    self.layout_output.addLayout(self.layout_tower_preview,     stretch=20)
    self.layout_output.addLayout(self.layout_logging,           stretch=20)

    main.addWidget(self.menubar,            stretch=1)
    main.addLayout(self.layout_img_input,   stretch=80)
    main.addLayout(self.layout_output,      stretch=20)

    self.setLayout(main)

    return self
