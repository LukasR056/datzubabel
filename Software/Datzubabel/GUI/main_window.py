import time

from PyQt5 import QtCore, QtGui, QtWidgets
import sys
import os
import cv2


class UiMainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super(UiMainWindow, self).__init__()
        self.setObjectName("MainWindow")
        self.resize(800, 600)
        self.setup_ui()
        self.show()
    
    def setup_ui(self):
        # MainWindow.setObjectName("MainWindow")
        # MainWindow.resize(800, 600)
        self.counter = 0
        self.localPos = (0,0)
        # self.view = GraphicsView()
        self.centralwidget = QtWidgets.QWidget(self)
        self.centralwidget.setObjectName("centralwidget")
        self.button1 = QtWidgets.QPushButton(self.centralwidget)
        self.button1.setGeometry(QtCore.QRect(300, 410, 150, 50))
        self.button1.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.button1.setObjectName("button1")
        self.button1.clicked.connect(self.button1_clicked)
        
        self.button2 = QtWidgets.QPushButton(self.centralwidget)
        self.button2.setGeometry(QtCore.QRect(100, 410, 150, 50))
        self.button2.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.button2.setObjectName("button2")
        self.button2.clicked.connect(self.button2_clicked)
        
        self.button3 = QtWidgets.QPushButton(self.centralwidget)
        self.button3.setGeometry(QtCore.QRect(100, 490, 150, 50))
        self.button3.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.button3.setObjectName("button3")
        self.button3.clicked.connect(self.button3_clicked)
        
        self.label1 = QtWidgets.QLabel(self.centralwidget)
        self.label1.setGeometry(QtCore.QRect(620, 410, 180, 140))
        self.label1.setFrameShape(QtWidgets.QFrame.Box)
        self.label1.setAlignment(QtCore.Qt.AlignCenter)
        self.label1.setObjectName("label1")
       
        self.label2 = QtWidgets.QLabel(self.centralwidget)
        self.label2.setGeometry(QtCore.QRect(300, 490, 150, 50))
        self.label2.setFrameShape(QtWidgets.QFrame.Box)
        self.label2.setAlignment(QtCore.Qt.AlignCenter)
        self.label2.setObjectName("label2")
        # self.get_motor_x_speed()
        
        self.label3 = QtWidgets.QLabel(self.centralwidget)
        self.label3.setGeometry(QtCore.QRect(480, 490, 100, 50))
        self.label3.setFrameShape(QtWidgets.QFrame.Box)
        self.label3.setAlignment(QtCore.Qt.AlignCenter)
        self.label3.setObjectName("label3")
        
        self.image1 = QtWidgets.QLabel(self.centralwidget)
        self.image1.installEventFilter(self)
        self.image1.setGeometry(QtCore.QRect(10, 0, 781, 391))
        self.image1.setFrameShape(QtWidgets.QFrame.Box)
        self.image1.setScaledContents(True)
        self.image1.setAlignment(QtCore.Qt.AlignCenter)
        self.image1.setMouseTracking(True)
        self.image1.setObjectName("image1")
        self.image1.show()

        self.input1 = QtWidgets.QSpinBox(self.centralwidget)
        self.input1.setGeometry(QtCore.QRect(480, 420, 100, 25))
        self.input1.setValue(9)  # set initial motor speed here
        self.input1.setRange(0, 9)

        self.setCentralWidget(self.centralwidget)
        
        self.menubar = QtWidgets.QMenuBar(self)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 26))
        self.menubar.setObjectName("menubar")
        self.menuFile = QtWidgets.QMenu(self.menubar)
        self.menuFile.setObjectName("menuFile")
        
        self.setMenuBar(self.menubar)
        
        self.statusbar = QtWidgets.QStatusBar(self)
        self.statusbar.setObjectName("statusbar")
        
        self.setStatusBar(self.statusbar)
        
        self.action_ffnen = QtWidgets.QAction(self)
        self.action_ffnen.setObjectName("action_ffnen")
        self.menuFile.addAction(self.action_ffnen)
        self.menubar.addAction(self.menuFile.menuAction())

        self.retranslate_ui(self)
        QtCore.QMetaObject.connectSlotsByName(self)

    def eventFilter(self, source, event):
        if event.type() == QtCore.QEvent.MouseButtonPress:
             # print(event.pos())
             # print(event.x())
             # print(event.y())
             self.localPos = (event.x(), event.y())
             self.label3.setText(str(self.localPos))
        return super().eventFilter(source, event)

    # def mousePressEvent(self, QMouseEvent):
    #     print(QMouseEvent.windowPos())
    #     print(QMouseEvent.pos())
        
    def retranslate_ui(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.button1.setText(_translate("MainWindow", "Stop Video"))
        self.button2.setText(_translate("MainWindow", "Start Video"))
        self.button3.setText(_translate("MainWindow", "Count +1"))
        self.label1.setText(_translate("MainWindow", "Hello this is a Test"))
        self.label2.setText(_translate("MainWindow", str(self.counter)))
        self.label3.setText(_translate("MainWindow", str(self.localPos)))
        self.image1.setText(_translate("MainWindow", "TextLabel"))
        self.menuFile.setTitle(_translate("MainWindow", "File"))
        self.action_ffnen.setText(_translate("MainWindow", "Öffnen"))

    def button1_clicked(self):
        self.label1.setText("No Video running")
        #self.image1.setPixmap(QtGui.QPixmap("papagei.jpg"))
        #self.label1.adjustSize()
        if hasattr(self, "vid"):         
            self.vid.release()
            cv2.destroyAllWindows()
        
    def button2_clicked(self):
        self.vid = cv2.VideoCapture(1, cv2.CAP_DSHOW) # 0 built-in webcam | 1 usb-webcam
        self.label1.setText("Video running")
        while(True):
            ret, frame = self.vid.read()
            if (cv2.waitKey(1) & 0xFF == ord("q")) | (not ret):
                break
            height, width, channel = frame.shape
            bytesPerLine = 3 * width
            qImg = QtGui.QImage(frame.data, width, height, bytesPerLine, 
                                QtGui.QImage.Format_RGB888).rgbSwapped()
            self.image1.setPixmap(QtGui.QPixmap(qImg))
    
        #self.vid.release()
        #cv2.destroyAllWindows()
        
    def button3_clicked(self):
        self.counter += 1
        self.label2.setText(str(self.counter))

    def set_motor_speed(self, speed):
        self.input1.setValue(speed)
