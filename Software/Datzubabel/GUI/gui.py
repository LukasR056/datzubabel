# -*- coding: utf-8 -*-
"""
Created on Fri Nov  5 16:33:34 2021

@author: janik
"""
import os
import sys
import ctypes
import cv2
# from camera import Camera
import numpy as np
import matplotlib.pyplot as plt

from Arduino.arduino_api import DeviceStatus, StepMotorLabel
from PlayfieldTracking import aruco_marker_tracking
from PyQt5.QtWidgets import (QWidget, QPushButton, QLabel, QFrame, QGridLayout, QSizePolicy,
                             QTextEdit, QComboBox, QMessageBox, QStyleFactory, QSlider, QScrollArea,
                             QLayout, QHBoxLayout, QVBoxLayout, QStackedLayout, QMenuBar, QDialog)
from PyQt5.QtGui import QIcon, QPixmap, QImage
from PyQt5.QtCore import Qt, QRect, QEvent, QSize
from threading import Thread
from Utilities.utilitites import JsonWriteReader
from Arduino.arduino_api import StepMotorLabel
from TurmAlgo.turmalgo import select
from GUI.gui_layout import init_gui_layout
from TurmAlgo.environment import get_shapes_from_unet_image, create_environment
from TurmAlgo.object_rotation import optimize_rotation


class Gui(QWidget):
    def __init__(self, camera, yolo_model, unet_model):
        super().__init__()
        self.camera = camera
        self.yolo_model = yolo_model
        self.unet_model = unet_model
        self.yolo_detection_df = None
        self.unet_detection_df = None
        self.detection_df = None
        self.algo_df = None
        # initialize previous stacked layout
        self.previous_window_index = 0
        # initialize clickable state of live image
        self.img_live_clickable = False
        # initialize json parameter
        self.img_live_width = None
        self.img_live_height = None
        self.obj_detection_algorithm = None
        # initialize gui layout using extra file for
        self = init_gui_layout(self)
        # configure main window
        self.setWindowTitle("DAT zu Babel")
        self.setMinimumSize(1920, 1080)
        self.setWindowIcon(QIcon('./GUI/images/turm.png'))
        self.showMaximized()
        # show specific stacked layout and camera
        self.switch_page()
        # set camera attributes
        self.camera.log_label = self.l_log


    # adding attributes after init
    def adding_new_attr(self, attr):
        setattr(self, attr, attr)

    # general functions
    def show_live_image(self):
        if self.camera.track_aruco:
            self.camera.track_aruco = False
            self.thread = Thread(target=self.camera.run, args=(self.img_live,))
            self.thread.start()
            if self.previous_window_index == 1:     # Click Mode
                self.yolo_model.df_detection_normalized = None
                self.reset_click_mode()
            if self.previous_window_index == 2:     # Automated Mode
                self.yolo_model.df_detection_normalized = None
                self.unet_model.img = None
                self.algo_df = None
            # self.camera.run(self.img_live)

    # click mode helper functions
    def manipulate_row_on_click(self, local_pos, row):
        temp_show = (int(local_pos[0] * self.img_live_width),
                     int(self.img_live_height - local_pos[1] * self.img_live_height))
        # append to scrollable text edit area when selected
        self.l_tower_prev.append(str(temp_show) + ": " + row["name"])
        row["selected"] = True
        if self.obj_detection_algorithm == "yolo":
            row["order"] = max(self.yolo_detection_df["order"]) + 1
        elif self.obj_detection_algorithm == "unet":
            row["order"] = max(self.unet_detection_df["order"]) + 1
        row["qlabel"].setText(str(row["order"] + 1))
        row["qlabel"].move(row["xcenter"] * self.img_live_width, (1 - row["ycenter"]) * self.img_live_height)
        row["qlabel"].setStyleSheet("font-size: 32px;")
        row["qlabel"].show()
        return row

    # click mode
    def eventFilter(self, source, event):
        if event.type() == QEvent.MouseButtonPress and self.dropdown_mode.currentText() == "Klick Modus" \
                and (self.yolo_model.df_detection_normalized is not None or self.unet_model.img_df is not None) \
                and self.img_live_clickable:    # image has to be clickable else error can occur
            # check for updated width / height from json and set yolo_detection_df for click mode
            if self.camera.updated_json:
                json_read_writer = JsonWriteReader("Configuration.json")
                if json_read_writer.json_exists():
                    content = json_read_writer.read_json()
                    self.img_live_width = content["Gui"]["scaled-img-width"]
                    self.img_live_height = content["Gui"]["scaled-img-height"]
                    self.obj_detection_algorithm = content["ObjectDetection"]["algorithm"]
                    # adapt height for correct coordinate origin (bottom left)
                    # local_pos = (event.x(), self.img_live_height - event.y())

                if self.obj_detection_algorithm == "yolo":
                    self.yolo_detection_df = self.yolo_model.df_detection_normalized.copy()
                    self.yolo_detection_df[["selected", "order", "qlabel"]] = False, -1, QLabel()
                    self.yolo_detection_df["qlabel"] = [QLabel(self.img_live) for el in self.yolo_detection_df["qlabel"]]
                if self.obj_detection_algorithm == "unet":
                    self.unet_detection_df = self.unet_model.img_df.copy()
                    self.unet_detection_df[["selected", "order", "qlabel"]] = False, -1, QLabel()
                    self.unet_detection_df["qlabel"] = [QLabel(self.img_live) for el in self.unet_detection_df["qlabel"]]
                self.camera.updated_json = False
            # normalize clicked position
            local_pos = (event.x() / self.img_live_width, event.y() / self.img_live_height)
            print(local_pos[0], local_pos[1])
            if self.obj_detection_algorithm == "yolo":
                for index, row in self.yolo_detection_df.iterrows():
                    if row["xmin"] <= local_pos[0] <= row["xmax"] and \
                            row["ymin"] <= local_pos[1] <= row["ymax"] and \
                            row["selected"] is False:
                        row = self.manipulate_row_on_click(local_pos=local_pos, row=row)
                        # write manipulated row into df
                        self.yolo_detection_df.iloc[index, :] = row
                    # un-click object
                    elif row["xmin"] <= local_pos[0] <= row["xmax"] and \
                            row["ymin"] <= local_pos[1] <= row["ymax"] and \
                            row["selected"] is True:
                        for sub_index, sub_row in self.yolo_detection_df.iterrows():
                            if sub_row["order"] > row["order"]:
                                sub_row["order"] = sub_row["order"] - 1
                                sub_row["qlabel"].setText(str(sub_row["order"] + 1))
                                # write into df
                                self.yolo_detection_df.iloc[sub_index, :] = sub_row
                        row["selected"] = False
                        row["order"] = -1
                        row["qlabel"].hide()
                        # write into df
                        self.yolo_detection_df.iloc[index, :] = row
                    # print("OUTPUT\n", self.yolo_detection_df)
            elif self.obj_detection_algorithm == "unet":
                print("LOOOOCAL", local_pos)
                local_pos_abs = (local_pos[0] * self.unet_model.img_one_channel.shape[1],
                                 local_pos[1] * self.unet_model.img_one_channel.shape[0])
                print("LoooCAL Abs", local_pos_abs)
                print(self.unet_detection_df)
                for index, row in self.unet_detection_df.iterrows():
                    if self.unet_model.img_one_channel[int(local_pos_abs[1]), int(local_pos_abs[0])] == row["class"] \
                            and row["selected"] is False:
                        row = self.manipulate_row_on_click(local_pos=local_pos, row=row)
                        # write manipulated row into df
                        self.unet_detection_df.iloc[index, :] = row
                    # un-click object
                    elif self.unet_model.img_one_channel[int(local_pos_abs[1]), int(local_pos_abs[0])] == row["class"] \
                            and row["selected"] is True:
                        for sub_index, sub_row in self.unet_detection_df.iterrows():
                            if sub_row["order"] > row["order"]:
                                sub_row["order"] = sub_row["order"] - 1
                                sub_row["qlabel"].setText(str(sub_row["order"] + 1))
                                # write into df
                                self.unet_detection_df.iloc[sub_index, :] = sub_row
                        row["selected"] = False
                        row["order"] = -1
                        row["qlabel"].hide()
                        # write into df
                        self.unet_detection_df.iloc[index, :] = row

        return super().eventFilter(source, event)

    def start_building_click_mode(self):
        # TODO: send self.yolo_detection_df building order to arduino
        if self.obj_detection_algorithm == "yolo":
            self.detection_df = self.yolo_detection_df
        elif self.obj_detection_algorithm == "unet":
            self.detection_df = self.unet_detection_df
        if self.detection_df is not None:
            self.detection_df = self.detection_df[self.detection_df["order"] != -1]
            self.detection_df = self.detection_df.sort_values(by=["order"])
            print("start_building_click_mode\n", self.detection_df)
            self.play_field.start_game(objects=self.detection_df)
            self.show_live_image()
        else:
            self.l_log.append("Keine detektierten Objekte vorhanden")
            return

    def reset_click_mode(self):
        self.img_live_clickable = False
        if self.unet_detection_df is not None:
            for index, row in self.unet_detection_df.iterrows():
                row["qlabel"].hide()
            self.unet_detection_df = None

        if self.yolo_detection_df is not None:
            for index, row in self.yolo_detection_df.iterrows():
                row["qlabel"].hide()
            self.yolo_detection_df = None

        if self.detection_df is not None:
            for index, row in self.detection_df.iterrows():
                row["qlabel"].hide()
            self.detection_df = None

    # ki mode
    def review_building_ki_mode(self):
        if self.yolo_model.df_detection_normalized is not None:
            self.yolo_detection_df = self.yolo_model.df_detection_normalized.copy()
        elif self.p_ki_dropdown.currentText() == "(Simulation)" and self.unet_model.img is not None:
            pass
        else:
            self.l_log.append("Keine detektierten Objekte vorhanden")
            return
        # map dropdown value to algorithm value
        area_calc = {
            "Bounding Box": "bounding_box",
            "Farbverteilung": "whitespace",
        }.get(self.p_ki_dropdown_area_calc.currentText(), "")

        if self.p_ki_dropdown.currentText() == "Fläche + Gewicht pro Objekt":
            self.algo_df = select(objects=self.yolo_detection_df, algorithm='analytical', an_formula='+area',
                                  area_calc=area_calc, img=None)
        if self.p_ki_dropdown.currentText() == "Gewicht pro Objekt":
            self.algo_df = select(objects=self.yolo_detection_df, algorithm='analytical', an_formula='name',
                                  area_calc=area_calc, img=None)
        if self.p_ki_dropdown.currentText() == "Umgekehrt":
            self.algo_df = select(objects=self.yolo_detection_df, algorithm='analytical', an_formula='yolo_yolo',
                                  area_calc=area_calc, img=None)
        if self.p_ki_dropdown.currentText() == "(Simulation)" and self.unet_model.img is not None:
            fig, ax = plt.subplots()
            plt.xlim([0, np.shape(self.unet_model.img[:, :, 0])[1]])
            plt.ylim([0, np.shape(self.unet_model.img[:, :, 0])[0]])
            simulated_img_prep = get_shapes_from_unet_image(self.unet_model.img[:, :, 0])
            create_environment(simulated_img_prep, np.shape(self.unet_model.img[:, :, 0])[1],
                               np.shape(self.unet_model.img[:, :, 0])[0], ax, level=1)
            fig.canvas.draw()
            simulated_img = np.frombuffer(fig.canvas.tostring_rgb(), dtype=np.uint8)
            simulated_img = simulated_img.reshape(fig.canvas.get_width_height()[::-1] + (3,))
            # print simulated image as QImage
            height, width, channel = simulated_img.shape
            bytes_per_line = 3 * width
            q_img = QImage(simulated_img, width, height, bytes_per_line, QImage.Format_RGB888).rgbSwapped()
            self.img_live.setPixmap(QPixmap(q_img))
        elif self.p_ki_dropdown.currentText() == "(Simulation)" and self.unet_model.img is None:
            self.l_log.append("Simulation benötigt Unet Objekterkennung")
        # create formatted preview
        if self.p_ki_dropdown.currentText() != "(Simulation)":
            self.l_tower_prev.append('[Name (Bewertung)]')
            for i in range(0, len(self.algo_df)):
                temp_str = "   " + str(i+1) + ". " + self.algo_df.name[i] + " (" \
                           + str(round(self.algo_df.score[i], 2)) + ")"
                self.l_tower_prev.append(temp_str)

    def start_building_ki_mode(self):
        if self.algo_df is not None:
            self.play_field.start_game(objects=self.algo_df)
            self.l_log.append("Bau Automatisierter Modus: " + self.p_ki_dropdown.currentText() + " (" +
                              self.p_ki_dropdown_area_calc.currentText() + ")")
            self.show_live_image()  # TODO: check for error?
        elif self.p_ki_dropdown.currentText() == "(Simulation)" and self.unet_model.img is not None:
            print("JAWOHL GEHT")
            if self.unet_model.img is not None:
                fig, ax = plt.subplots()
                plt.xlim([0, np.shape(self.unet_model.img[:, :, 0])[1]])
                plt.ylim([0, np.shape(self.unet_model.img[:, :, 0])[0]])
                simulated_img_prep = get_shapes_from_unet_image(self.unet_model.img[:, :, 0])
                objects, object_rotation = optimize_rotation(simulated_img_prep,
                                                             x_max=np.shape(self.unet_model.img[:, :, 0])[1],
                                                             y_max=np.shape(self.unet_model.img[:, :, 0])[0],
                                                             ax=ax, isImage=True)
                ax.cla()
                plt.gca().invert_yaxis()

                for o in objects:
                    o.print_shape()

                fig.canvas.draw()
                simulated_img = np.frombuffer(fig.canvas.tostring_rgb(), dtype=np.uint8)
                simulated_img = simulated_img.reshape(fig.canvas.get_width_height()[::-1] + (3,))

                # print simulated image as QImage
                height, width, channel = simulated_img.shape
                bytes_per_line = 3 * width
                q_img = QImage(simulated_img, width, height, bytes_per_line, QImage.Format_RGB888).rgbSwapped()
                self.img_live.setPixmap(QPixmap(q_img))
        else:
            self.l_log.append("Turmvorschau muss zuvor getätigt werden")

    def simulate_building(self):
        pass

    def switch_page(self):
        self.stacked_layout.setCurrentIndex(self.dropdown_mode.currentIndex())
        self.show_live_image()
        if self.dropdown_mode.currentIndex() == 0:
            self.stacked.setFixedHeight(self.page_gamepad.minimumSizeHint().height())

        if self.dropdown_mode.currentIndex() == 1:
            self.stacked.setFixedHeight(self.page_click.minimumSizeHint().height())

        if self.dropdown_mode.currentIndex() == 2:
            self.stacked.setFixedHeight(self.page_ki.minimumSizeHint().height())
        # set window index afterward changing layout for previous index for next iteration
        self.previous_window_index = self.dropdown_mode.currentIndex()

    def closeEvent(self, event):
        self.camera.destroy()
        self.thread.join()
        # app.quit()
        # sys.exit(1)

    # functions calling the play field
    def reference(self):
        self.play_field.reference()

    def crop_image(self):
        # self.thread.kill()
        self.camera.track_aruco = True
        self.img_live_clickable = True
        # self.thread.join()
        # print("thread killed?")

    # slider functions
    def update_speed_x(self, value):
        self.slider_x_ret.setText(str(value))

    def update_speed_y(self, value):
        self.slider_y_ret.setText(str(value))

    def update_speed_z(self, value):
        self.slider_z_ret.setText(str(value))

    # Functions for updating GUI according to Arduino
    def live_coordinates(self, x, y, z):
        self.l_overview_x.setText("x: " + str(x))
        self.l_overview_y.setText("y: " + str(y))
        self.l_overview_z.setText("z: " + str(z))

    def update_magnet_state(self, magnet):
        if magnet.status == DeviceStatus.MAGNET_ON:
            self.l_overview_magnet_img.setPixmap(self.green_pixmap)
        if magnet.status == DeviceStatus.MAGNET_OFF:
            self.l_overview_magnet_img.setPixmap(self.red_pixmap)

    def update_motor(self, motor):
        self.m_active = {
            StepMotorLabel.X_AXIS: self.mx_active,
            StepMotorLabel.Y_AXIS: self.my_active,
            StepMotorLabel.Z_AXIS: self.mz_active
        }[motor.label]

        self.m_min = {
            StepMotorLabel.X_AXIS: self.mx_min,
            StepMotorLabel.Y_AXIS: self.my_min,
            StepMotorLabel.Z_AXIS: self.mz_min
        }[motor.label]

        self.m_max = {
            StepMotorLabel.X_AXIS: self.mx_max,
            StepMotorLabel.Y_AXIS: self.my_max,
            StepMotorLabel.Z_AXIS: self.mz_max
        }[motor.label]

        self.m_speed = {
            StepMotorLabel.X_AXIS: self.mx_speed,
            StepMotorLabel.Y_AXIS: self.my_speed,
            StepMotorLabel.Z_AXIS: self.mz_speed
        }[motor.label]

        if motor.status == DeviceStatus.RUN_TO_MIN or motor.status == DeviceStatus.RUN_TO_MAX:
            self.m_active.setPixmap(self.green_pixmap)
            self.m_speed.setText(str(motor.speed))
            self.m_min.setPixmap(self.black_pixmap)
            self.m_max.setPixmap(self.black_pixmap)
        elif motor.status == DeviceStatus.STOPPED_AT_MIN:
            self.m_active.setPixmap(self.black_pixmap)
            self.m_speed.setText("0")
            self.m_min.setPixmap(self.red_pixmap)
        elif motor.status == DeviceStatus.STOPPED_AT_MAX:
            self.m_active.setPixmap(self.black_pixmap)
            self.m_speed.setText("0")
            self.m_max.setPixmap(self.red_pixmap)
        else:
            self.m_active.setPixmap(self.black_pixmap)
            self.m_speed.setText("0")
            self.m_min.setPixmap(self.black_pixmap)
            self.m_max.setPixmap(self.black_pixmap)

    def update_position_overview(self, motor_label, steps):
        if motor_label == StepMotorLabel.X_AXIS:
            self.l_overview_x.setText("x: " + str(int(self.l_overview_x.text()[3:]) + int(steps)))

    # menubar functions
    def calibrate_playfield(self):
        self.play_field.calibrate()

    def open_readme(self):
        os.startfile("Readme.md", "open")

    def open_about(self):
        about_prj = QMessageBox(self)
        about_prj.setWindowTitle("Über das Projekt")
        formatted_text = "Dieses Projekt wurde im Zuge der Lehrveranstaltung \"Projektarbeit\" im Studiengang Data " \
                         "and Information Science, Jahrgang 2020 erstellt.\n\n" \
                         "Copyright © Joachim Komar, Janik Regula, Lukas Reitbauer, Marco Reiter, Alexander Schadler"
        about_prj.setText(formatted_text)
        about_prj_button = about_prj.exec()

        if about_prj_button == QMessageBox.Ok:
            print("JAWOHL DANKE JOACHIM")

    def open_log_file(self):
        os.startfile("datzulabel.log", "open")

    def open_json_file(self):
        os.startfile("Configuration.json", "open")

    def close_gui(self):
        self.close()
        self.camera.destroy()
        self.thread.join()




