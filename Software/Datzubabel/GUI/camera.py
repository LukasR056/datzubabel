import numpy as np
import cv2
import time
from cv2 import aruco
from PyQt5.QtGui import QImage, QPixmap
from matplotlib import pyplot as plt

from Utilities.logger import LoggerManager, Logger
from Utilities.utilitites import JsonWriteReader


class Camera:
    def __init__(self, yolo_model, unet_model):
        self.alive = True
        self.vc = None
        self.img_label = None
        self.log_label = None
        self.track_aruco = True                         # initialize True to show image on start
        self.vc = cv2.VideoCapture(1, cv2.CAP_DSHOW)    # 0 built-in webcam | 1 usb-webcam
        self.yolo_model = yolo_model
        self.unet_model = unet_model
        self.updated_json = False

    def run(self, img_label):
        self.img_label = img_label
        self.vc.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)  # (Full) HD causes lag
        self.vc.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)
        if self.vc.isOpened():  # try to get the first frame
            self.ret, self.frame = self.vc.read()
        else:
            self.ret = False
        # continuous read of video capture
        while self.ret:
            self.ret, self.frame = self.vc.read()
            self.frame = cv2.rotate(self.frame, cv2.ROTATE_180)  # flipping image because camera is mounted backwards

            if cv2.waitKey(1) & 0xFF == ord("q") or not self.ret or self.track_aruco:
                break
            height, width, channel = self.frame.shape
            bytes_per_line = 3 * width
            q_img = QImage(self.frame.data, width, height, bytes_per_line, QImage.Format_RGB888).rgbSwapped()
            self.img_label.setPixmap(QPixmap(q_img))
            # runtime (no cropping) object detection for testing
            # self.detect_objects()

        if self.track_aruco:
            ret = self.track_aruco_marker()     # creation of cropped image (returns True if all aruco markers found)
            if ret:
                json_read_writer = JsonWriteReader("Configuration.json")
                # image shape is altered by setPixmap of camera input (frame)
                self.set_json_image_shape(json_read_writer=json_read_writer)
                if json_read_writer.json_exists():
                    content = json_read_writer.read_json()
                    algorithm_mode = content["ObjectDetection"]["algorithm"]
                    # use yolo5 model for object detection
                    if algorithm_mode == "yolo":
                        self.detect_yolo_objects()
                    # use unet model for object detection
                    if algorithm_mode == "unet":
                        self.detect_unet_objects()
                    # implement window clickable

            else:
                self.log_label.append("Nicht alle Aruco Marker konnten getrackt werden")
                time.sleep(3)   # wait for 5 seconds to indicate trackable aruco markers
                self.track_aruco = False
                self.run(img_label=self.img_label)

        else:
            self.destroy()

    def set_json_image_shape(self, json_read_writer):
        if json_read_writer.json_exists():
            content = json_read_writer.read_json()
            content["Gui"]["scaled-img-width"] = self.img_label.width()
            content["Gui"]["scaled-img-height"] = self.img_label.height()
            json_read_writer.write_json(content)
            self.updated_json = True

    def track_aruco_marker(self):
        log = LoggerManager.getLogger(Logger.DefaultLogger)
        # set video capture to full hd for better detection
        self.vc.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
        self.vc.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)
        self.ret, self.frame = self.vc.read()
        # self.frame = cv2.imread("./GUI/images/aruco_training.jpg")  # use image for testing
        self.frame = cv2.rotate(self.frame, cv2.ROTATE_180)  # flipping the image because camera is mounted backwards

        # Identifying the Aruco markers in the frame
        gray = cv2.cvtColor(self.frame, cv2.COLOR_BGR2GRAY)
        aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_50)
        parameters = aruco.DetectorParameters_create()
        corners, ids, _ = aruco.detectMarkers(gray, aruco_dict, parameters=parameters)

        if len(corners) == 4:
            # Sort array, such that upper left point is first, upper right is second, etc.
            sort_id = np.argsort(sum(ids.tolist(), []))
            sorted_corners = np.empty(np.shape(corners))
            for idx, item in enumerate(sort_id):
                sorted_corners[idx] = corners[item]

            pt_a = sorted_corners[0][0][0]  # upper left point
            pt_d = sorted_corners[1][0][0]  # upper right point
            pt_c = sorted_corners[2][0][0]  # lower right point
            pt_b = sorted_corners[3][0][0]  # lower left point

            # calculate transformation matrix for image transformation
            width_ad = np.sqrt(((pt_a[0] - pt_d[0]) ** 2) + ((pt_a[1] - pt_d[1]) ** 2))
            width_bc = np.sqrt(((pt_b[0] - pt_c[0]) ** 2) + ((pt_b[1] - pt_c[1]) ** 2))
            max_width = max(int(width_ad), int(width_bc))
            height_ab = np.sqrt(((pt_a[0] - pt_b[0]) ** 2) + ((pt_a[1] - pt_b[1]) ** 2))
            height_cd = np.sqrt(((pt_c[0] - pt_d[0]) ** 2) + ((pt_c[1] - pt_d[1]) ** 2))
            max_height = max(int(height_ab), int(height_cd))

            input_pts = np.float32([pt_a, pt_b, pt_c, pt_d])
            output_pts = np.float32([[0, 0],
                                     [0, max_height - 1],
                                     [max_width - 1, max_height - 1],
                                     [max_width - 1, 0]])

            transformation_matrix = cv2.getPerspectiveTransform(input_pts, output_pts)
            # image transformation according to transformation matrix
            self.frame = cv2.warpPerspective(self.frame, transformation_matrix, (max_width, max_height),
                                             flags=cv2.INTER_LINEAR)
            # self.frame = cv2.rotate(self.frame, cv2.ROTATE_180)
            height, width, channel = self.frame.shape
            bytes_per_line = 3 * width
            q_img = QImage(self.frame.data, width, height, bytes_per_line, QImage.Format_RGB888).rgbSwapped()
            self.img_label.setPixmap(QPixmap(q_img))
            return True
        else:
            if len(corners) < 4:
                log.critical("Less than 4 corners of the playfield were detected. Check Aruco markers!")
            elif len(corners) > 4:
                log.critical("More than 4 corners of the playfield were detected. Check Aruco markers!")
            # display circle around detected aruco markers
            for corner in corners:
                center_coord = tuple(np.mean(corner, axis=1)[0])
                cv2.circle(img=self.frame, center=center_coord, radius=50, color=(0, 255, 0), thickness=5)
            # resize image
            dim = (int(self.frame.shape[1] * 0.6), int(self.frame.shape[0] * 0.6))
            self.frame = cv2.resize(self.frame, dim, interpolation=cv2.INTER_AREA)

            height, width, channel = self.frame.shape
            bytes_per_line = 3 * width
            q_img = QImage(self.frame.data, width, height, bytes_per_line, QImage.Format_RGB888).rgbSwapped()
            self.img_label.setPixmap(QPixmap(q_img))
            return False

    def detect_yolo_objects(self):
        self.yolo_model.detect_objects(self.frame)
        # print detected objects in gui
        # print(self.yolo_model.df_detection_normalized)
        if not self.yolo_model.df_detection_normalized.empty:
            objects = "Erkannte Objekte: "
            for obj in np.unique(self.yolo_model.df_detection_normalized["name"]):
                objects += obj + ", "
            self.log_label.append(objects[:-2])
        else:
            self.log_label.append("Keine Objekte detektiert")
        # prepare image with detected objects for display
        self.yolo_detection = self.yolo_model.detection.render()[0]
        height, width, channel = self.yolo_detection.shape
        bytes_per_line = 3 * width
        q_img = QImage(self.yolo_detection, width, height, bytes_per_line, QImage.Format_RGB888).rgbSwapped()
        self.img_label.setPixmap(QPixmap(q_img))

    def detect_unet_objects(self):
        # predict objects using unet class
        self.unet_model.predict_image(self.frame)
        print("unet_model.img.shape:", self.unet_model.img.shape)
        print(np.unique(self.unet_model.img))
        # print detected objects in gui
        if not (np.unique(self.unet_model.img) == [0]).all():
            objects = "Erkannte Objekte: "
            for obj in np.unique(self.unet_model.img):
                temp = {
                    64: "cuboid",
                    128: "cylinder",
                    192: "prism"
                }.get(obj, "")
                if obj != 0:
                    objects += temp + ", "
            self.log_label.append(objects[:-2])
            # cuboid, cylinder, prism
        else:
            self.log_label.append("Keine Objekte detektiert")
        # set Unet prediction to Pixmap
        height, width, channel = self.unet_model.img.shape
        bytes_per_line = 3 * width
        q_img = QImage(self.unet_model.img.data, width, height, bytes_per_line, QImage.Format_RGB888).rgbSwapped()
        self.img_label.setPixmap(QPixmap(q_img))

    def destroy(self):
        if self.alive and self.vc:
            cv2.destroyAllWindows()
            self.vc.release()
            self.alive = False



