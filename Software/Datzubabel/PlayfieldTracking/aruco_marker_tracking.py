import os
import numpy as np
import cv2
from cv2 import aruco
import matplotlib as mpl
import matplotlib.pyplot as plt

from Utilities.logger import LoggerManager, Logger

#mpl.use("Qt5Agg")

# Debugging Code: Manually feeding images if no camera is available #
#script_dir = os.path.dirname(__file__)
#abs_file_path = os.path.join(script_dir, "testdata","file_2.jpg")
#abs_file_path = abs_file_path.replace(os.sep, '/')
#frame = cv2.imread(abs_file_path)

def aruco_marker_tracking():
    print("ES GEHT1")
    log = LoggerManager.getLogger(Logger.DefaultLogger)
    cap = cv2.VideoCapture(1, cv2.CAP_DSHOW) # If Webcam is not recognized, switch back to 0 instead of 1
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)  # Full HD
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)
    rep, frame = cap.read()
    frame = cv2.rotate(frame, cv2.ROTATE_180) # Flipping the image because camera is mounted backwards
    cap.release()

    # Identifying the Aruco markers in the frame
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_50)
    parameters = aruco.DetectorParameters_create()
    corners, ids, _ = aruco.detectMarkers(gray, aruco_dict, parameters=parameters)
    frame_markers = aruco.drawDetectedMarkers(frame.copy(), corners, ids)
    log.info("test")

    if len(corners) == 4:

        # Sort array, such that upper left point is first, upper right is second, etc.
        sort_id = np.argsort(sum(ids.tolist(), []))
        sorted_corners = np.empty(np.shape(corners))
        for idx, item in enumerate(sort_id):
            sorted_corners[idx] = corners[item]

        pt_A = sorted_corners[0][0][0]   # upper left point
        pt_D = sorted_corners[1][0][0]   # upper right point
        pt_C = sorted_corners[2][0][0]   # lower right point
        pt_B = sorted_corners[3][0][0]   # lower left point

        # Calculate transformation matrix for image transformation
        width_AD = np.sqrt(((pt_A[0] - pt_D[0]) ** 2) + ((pt_A[1] - pt_D[1]) ** 2))
        width_BC = np.sqrt(((pt_B[0] - pt_C[0]) ** 2) + ((pt_B[1] - pt_C[1]) ** 2))
        maxWidth = max(int(width_AD), int(width_BC))
        height_AB = np.sqrt(((pt_A[0] - pt_B[0]) ** 2) + ((pt_A[1] - pt_B[1]) ** 2))
        height_CD = np.sqrt(((pt_C[0] - pt_D[0]) ** 2) + ((pt_C[1] - pt_D[1]) ** 2))
        maxHeight = max(int(height_AB), int(height_CD))

        input_pts = np.float32([pt_A, pt_B, pt_C, pt_D])
        output_pts = np.float32([[0, 0],
                                 [0, maxHeight - 1],
                                 [maxWidth - 1, maxHeight - 1],
                                 [maxWidth - 1, 0]])

        M = cv2.getPerspectiveTransform(input_pts, output_pts)
        frame = cv2.warpPerspective(frame, M, (maxWidth, maxHeight), flags=cv2.INTER_LINEAR)
        print("ES GEHT")
        return frame

        # Debugging Code: Saves the cropped picture as a file and displays it in a plot
        # cv2.imwrite('PlayfieldTracking/testdata/output.jpg',frame)
        # plt.figure()
        # plt.imshow(frame)
        # plt.show()
    elif len(corners) < 4:
        log.critical("Less than 4 corners of the playfield were detected. Check Aruco markers!")
        return None
    elif len(corners) > 4:
        log.critical("More than 4 corners of the playfield were detected. Check Aruco markers!")
        return None

# Debugging Code: Displays the identified Aruco markers in the pictures#

# plt.figure()
# plt.imshow(frame_markers, origin="upper")
# if ids is not None:
#    for i in range(len(ids)):
#        c = corners[i][0]
#        plt.plot([c[:, 0].mean()], [c[:, 1].mean()], "+", label = "id={0}".format(ids[i]))
# plt.legend()
# plt.show()
