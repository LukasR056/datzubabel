import os
import numpy as np
import cv2
from cv2 import aruco

MARKER_WIDTH = 300
aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_50)

def generate_markers(kind, id_from, id_to):
    """ Generates a Aruco marker and displays it in a figure one at a time
    """
    for i in range(id_from, id_to+1):
        tag = np.zeros((MARKER_WIDTH, MARKER_WIDTH, 1), dtype="uint8")
        aruco.drawMarker(aruco_dict, i, MARKER_WIDTH, tag)
        cv2.imwrite(f'markers/tag_{kind}_{i}_{MARKER_WIDTH}.png', tag)
        cv2.imshow(f'Tag {i}: ', tag)
        cv2.waitKey(0)

if __name__ == '__main__':
    print(f'Generating markers in folder {os.getcwd()}.')
    generate_markers('corner', 1, 4)
