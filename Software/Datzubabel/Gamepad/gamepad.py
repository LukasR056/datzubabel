from XInput import *
import math
from Utilities.logger import LoggerManager, Logger
from PlayField.play_field import PlayField
from ObjectDetection.yolo5 import Yolo5
from TurmAlgo import turmalgo
from GUI.camera import Camera
from PlayField.action import ActionMeasure, ActionMoveXY, ActionPickObject, ActionStackObject

log = LoggerManager.getLogger(Logger.DefaultLogger)


class GamePadHandler(EventHandler):
    def __init__(self, *controllers, play_field):
        super().__init__(*controllers)
        if isinstance(play_field, PlayField):
            self.play_field = play_field
        self.arduino1 = play_field.get_arduino1()
        self.motor_x = self.arduino1.get_motor_x()
        self.motor_y = self.arduino1.get_motor_y()
        self.motor_z = self.arduino1.get_motor_z()
        self.magnet = self.arduino1.get_magnet()

    def process_button_event(self, event):
        if event.type == EVENT_BUTTON_PRESSED:
            log.debug("Button %s pressed", event.button)
            if event.button == "A":
                log.warning("No mode defined")
            elif event.button == "Y":
                self.play_field.stop_all()
            elif event.button == "X":
                tid = self.arduino1.get_next_transaction_id()
                self.magnet.magnet_on(tid)
                self.arduino1.execute(tid)
            elif event.button == "B":
                tid = self.arduino1.get_next_transaction_id()
                self.magnet.magnet_off(tid)
                self.arduino1.execute(tid)
            elif event.button == "LEFT_SHOULDER":
                tid = self.arduino1.get_next_transaction_id()
                self.motor_z.run_to_min(tid)
                self.arduino1.execute(tid)
            elif event.button == "RIGHT_SHOULDER":
                tid = self.arduino1.get_next_transaction_id()
                self.motor_z.run_to_max(tid)
                self.arduino1.execute(tid)
            elif event.button == "BACK":
                self.play_field.calibrate()
            elif event.button == "START":
                # reference drive
                self.play_field.reference()
            else:
                log.warning("unexpected event")

        if event.type == EVENT_BUTTON_RELEASED:
            log.debug("Button %s released", event.button)
            if event.button == "LEFT_SHOULDER":
                tid = self.arduino1.get_next_transaction_id()
                self.motor_z.stop(tid)
                self.arduino1.execute(tid)

            elif event.button == "RIGHT_SHOULDER":
                tid = self.arduino1.get_next_transaction_id()
                self.motor_z.stop(tid)
                self.arduino1.execute(tid)

    def process_trigger_event(self, event):
        log.debug("process_trigger_event ")
        # event reserved for the two triggers

    def process_stick_event(self, event):
        if event.value > 0:  # radius 0 -> 1
            tid = self.arduino1.get_next_transaction_id()
            angle = math.degrees(math.atan2(event.y, event.x))
            if angle <= -157.5:
                log.debug("move left")
                self.motor_x.run_to_min(tid)
            elif -157.5 < angle <= -112.5:
                log.debug("move left and down")
                self.motor_x.run_to_min(tid)
                self.motor_y.run_to_min(tid)
            elif -112.5 < angle <= -67.5:
                log.debug("move down")
                self.motor_y.run_to_min(tid)
            elif -67.5 < angle <= -22.5:
                log.debug("move right and down")
                self.motor_x.run_to_max(tid)
                self.motor_y.run_to_min(tid)
            elif -22.5 < angle <= 22.5:
                log.debug("move right")
                self.motor_x.run_to_max(tid)
            elif 22.5 < angle <= 67.5:
                log.debug("move right and up")
                self.motor_x.run_to_max(tid)
                self.motor_y.run_to_max(tid)
            elif 67.5 < angle <= 112.5:
                log.debug("move up")
                self.motor_y.run_to_max(tid)
            elif 112.5 < angle <= 157.5:
                log.debug("move left and up")
                self.motor_x.run_to_min(tid)
                self.motor_y.run_to_max(tid)
            elif angle > 157.5:
                log.debug("move left")
                self.motor_x.run_to_min(tid)

            self.arduino1.execute(tid)
        else:
            tid = self.arduino1.get_next_transaction_id()
            self.motor_x.stop(tid)
            self.motor_y.stop(tid)
            self.arduino1.execute(tid)

    def process_connection_event(self, event):
        log.debug("process_connection_event")
        # event related to the gamepad status
