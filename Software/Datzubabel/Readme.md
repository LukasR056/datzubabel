# useful informations

## Install instruction

### Setup python environment
- open anaconda prompt and navigate to the project source 
- create anaconda environment with following command (replace `<env>` by environment name of choice)
  - `conda create --name <env> --file requirements_conda.txt`
- activate new anaconda environment
  - `conda activate <env>`
- install additional python packages with `pip` with following command
  - `pip install -r requirements_pip.txt`

### start python program
- execute following command in the new created environment
  - python.exe `__main__.py`

### Setup Arduino (if adaptions are necassary)
- Download [Arduino IDE](https://www.arduino.cc/en/software)
- Install IDE
- Connect Arduino to PC
- Open Arduino IDE
- Install Libraries
    - copy libraries folder from abacus (`abacus\dat20\team4_turm\03_treiber_3rdparty_assets_sw`) to local sketchbook location
        - location of sketchbook location, see File->Preferences
- Load *.ino configuration file
 
## Configuration
Configuration file is located in the project root named `Configuration.json`
Following parameters are possible:

### Arduino
- com-port
  - Serial COM Port of Arduino
- baudrate
  - baudrate of the serial connection 
- poll-sleep-time
  - poll time for reading messages from arduino in seconds

### PlayField
- poll-sleep-time
  - poll time for loop to handle asyncron events
- max-x-steps
  - maximum steps in x direction (set by calibration mode).
- max-y-steps
  - maximum steps in y direction (set by calibration mode). 
- max-z-steps
  - maximum steps in z direction (set by calibration mode). 
- offset-x-magnet
  - offset in steps in x direction between the magnet and the motor end switch for strobing the objects (needed to adjust the magnet after strobing the objects). 
- offset-z-stack
  - offset in steps which is added to the object height in the stacking process to avoid crashes due to missing gap between the stack and the object.   
- offset-z-magnet
  - offset in z direction in steps to produce a gap between strobing the object and activating the magnet for picking up the object. 

### Gui
- scaled-img-width
- scaled-img-height
  
### ObjectDetection
- algorithm
  - specify the detection mode. possible modes:
    - `yolo` for Yolo5 object detection
    - `unet` for Unet segmantic mask detection

### Analytical
- scaling 
- area_calculation  
- weight_area 
- Weight_Name 
  - cuboid  
  - cube  
  - cylinder  
  - prism 


### Arduino API 
The command always start with "```<```" and end with "```>```"
1. There are different modes for stepper motor and magnet which indicate the first parameter
    - RUN
        - infinity run of motor
        - this mode support multiple motor to run at the same time
    - STOP
        - stop motor
    - SPEED
        - configure speed
    - POS
        - set step size and speed of motor
    - MAGON
        - activate magnet
    - MAGOFF
        - deactivate magnet
    - CALIBRATION
        - calibrate the motor 

2. The second parameter is the name of the motor, like ```m1``` or ```mg1```
3. The third parameter is used for the mode RUN, SPEED and POS.
    - direction for mode RUN (```1``` is forward, ```-1``` is backward)
    - velocity for mode SPEED
    - step size for mode POS
4. The fourth parameter is used for mode RUN and POS and represent the velocity 

#### examples
- ```<RUN m1 -1 10>```
 ``` <RUN m1 -1 10 m2 1 15>```
- ```<STOP m1>```
- ```<SPEED m2 10>```
- ```<POS m2 500 10>```
- ```<MAGON mg1>```
- ```<MAGOFF mg1>```
- ```<CALIBRATION m2>```


#### usage in python
- use class ```ArduinoUno``` in package _Arduino_

## Gamepad with [XInput](https://pypi.org/project/XInput-Python/)
- Windows only
### install XInput lib
- open anaconda promp and type following command 
    - `pip install XInput-Python`
### usage
- define used xbox buttons, sticks, etc.
  - `button_handler.set_filter(STICK_LEFT + BUTTON_START + BUTTON_A + BUTTON_B .. )`
- start Gamepad thread
- Events will be handled with ``GamePadHandler`` class in package _Gamepad_.

### assignment of keys
- LEFT STICK: move in x and y direction
- BUTTON X: activate magnet
- BUTTON B: deactivate magnet 
- BUTTON LB: move magnet to min z direction
- BUTTON RB: move magnet to max z direction
- BUTTON START: initialisation (moving to zero position)

## Logging
- manage central logging
  - class: `LoggerManager`
  - configuration file : `logger_configuration.yml`
  - log every entry in `datzulabel.log`
  - logs with log levels _WARNING_, _ERROR_ and _CRITICAL_ will be logged also directly into console
  
## usage
- import Class 
  - `from Utilities.logger import LoggerManager, Logger`
- init logger
  - `log = LoggerManager.getLogger(Logger.DefaultLogger)`
- use logger
  - `log.debug("This is a %s log entry", debug)`

## Program structure
- The class `PlayField` is working as an observer and inject the class `ArduinoUno`
  - this class has a poll function for observing states
  - has methods for referencing and calibration 
- The `ArduinoClass` has all _Devices_ injected, like the `StepMotor`, `Magnet` ...
  - this class has a poll method to receive messages from the arduino devices
- The `Gamepad` is a side process which has injected the `Playfield` for controlling.
  - maybe should be reduced to Arduino, as the method for the `PlayField` should be handled by GUI

## Object Detection
- Detection mode can be specified by changing the `ObjectDetection` entry `algorithm` in the configuration file to one of the two following values
  - `yolo` for Yolo5 object detection (approved)
  - `unet` for Unet segmantic mask detection (work in progress)

## GUI
- The class acts as an interface for multiple other classes. For use initialize in `Gui.__init__` and run gui object with specific objects   
- Analytical operations can be implemented using the functions
  - `review_building_ki_mode`
  - `start_building_ki_mode`