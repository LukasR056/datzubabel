# program documentation

The main python file (entry point) is the ``gamepad.py``

## classes and their implementations

- **``Device``**
    - generic class for all devices which can be attached to the Arduino
    - attributes:
        - ``name``: name of the device which is referenced in the Arduino code
        - ``commands``: list of commands. a command itself is a dictionary which has the attributes ``transaction_id``, ``mode``( See ``ArduinoMode``) and ``parameters`` (parameters for the Arduino interface)

- **``StepMotor``**
    - inherited class from ``Device`` and implement methods of a step motor

- **``Magnet``**
    - inherited class from ``Device`` and implement methods of a magnet

- **``HW_Interface``**
    - implement a serial interface with a callback and a send method
    - works asyncron on a own thread 
    - copied code from [headrotor]( https://github.com/headrotor/Python-Arduino-example)

- **``ArduinoUno``**
    - inherent from class ``HW-Interface`` and implement the interface to the Arduino
    - attributes:
        - a list of devices (motor, magnet, servo ...)
        - transactions : list of all open transactions
    - methods:
        - ``add_device(Device)``: add device to Arduino which should be handled
        - ``get_next_transaction_id()``: get next transaction id which has to be linked to a device for sending commands
        - ``execute(transaction_id)``: append transaction id to transactions and send command of latest transaction_id to Arduino

## usage

### Initialisation
- initialize **ArduinoUno** object with given COM Port and baudrate
    - f. ex. ``arduino = ArduinoUno(port='COM4', baudrate=115200)``
- register callback function for tracking output of Arduino
    - f. ex. ``arduino.register_callback(arduino_callback)``
- initalize all devices (f. ex. ``StepMotor``, ``Magnet``) which are used in the programm
    - f. ex. ``motor_x = StepMotor("m3", label=StepMotorLabel.X_AXIS)``
- add devices to Arduino object 
    - f. ex. ``arduino.add_device(motor_x)``

### Sending commands to Arduino
- get transaction id to open a transaction
    - f. ex. ``tid = arduino.get_next_transaction_id()``
- perform specific action on device by attaching the transaction_id
    - f.ex. ``motor_x.run_forward(tid)``
- for sending command to Arduino use method ``execute()`` on the Arduino object and close the transaction
    - f.ex ``arduino.execute(tid)``


