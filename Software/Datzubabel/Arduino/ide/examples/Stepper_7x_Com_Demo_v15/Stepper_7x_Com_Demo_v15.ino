

//
// Arduino DAT Code
// 2x Motor Shield
// author: Alexander Nischelwitzer, FHJ IMA/AIM DAT DMT
// last update: 04.10.2021
// 
// HARDWARE:
// 2x Motor Shield ca. 20€
// https://learn.adafruit.com/adafruit-motor-shield-v2-for-arduino
// Grove Shield 10€
// Arduino UNO 20€
// Stackable headers 2€
// RGB Display  ca. 15€
// http://wiki.seeedstudio.com/Grove-LCD_RGB_Backlight/ 
//
// RESERVED PINS:
// D04 D05 >> motor shield 1, I2C Adress 0x60
// D04 D05 >> motor shield 2, I2C Adress 0x61
// D09 D10 >> reserved >> servos
//
// USAGE:
// DC Motor (max 8) M1 its 1, M2 use 2, M3 use 3 and M4 use 4
// Stepper Motor (max 2)
// RC Servos (max 4) 
// Stacking Shields: https://learn.adafruit.com/adafruit-motor-shield-v2-for-arduino/stacking-shields
//
// #######################################

#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"
#include "rgb_lcd.h"
#include <Servo.h>

rgb_lcd mylcd;

// Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 
Adafruit_MotorShield AFMS_1(0x60);
Adafruit_MotorShield AFMS_2(0x61);
 

// M1 its 1, M2 use 2, M3 use 3 and M4 use 4
// Adafruit_DCMotor *myMotorDC = AFMS.getMotor(2);
// https://learn.adafruit.com/adafruit-motor-shield-v2-for-arduino/using-stepper-motors

Adafruit_StepperMotor *myMotor1 = AFMS_1.getStepper(200, 1); // klein 
Adafruit_StepperMotor *myMotor2 = AFMS_1.getStepper(200, 2); // gross

// Adafruit_StepperMotor *myMotor3 = AFMS_2.getStepper(200, 1); // JoyIT Nema14-1
Adafruit_DCMotor      *myDCMotor3a = AFMS_2.getMotor(1);
Adafruit_DCMotor      *myDCMotor3b = AFMS_2.getMotor(2);

Adafruit_StepperMotor *myMotor4 = AFMS_2.getStepper(200, 2); // JoyIT Nema14-1


// Adafruit_DCMotor *myMotor = AFMS.getMotor(1);
Servo servo1;
Servo servo2;
int posServo1 = 0;
int posServo2 = 0;
  
// see https://github.com/adafruit/Adafruit_Motor_Shield_V2_Library/blob/master/Adafruit_MotorShield.h

#define seconds() (millis()/1000)
#define minutes() (millis()/60000)

// ################################################################################
// ################################################################################
// SETUP
// define Input and Outputs  
  
const int ledCtrlPin    = 2; // connector on D2
const int buttonCtrlPin = 3;
int buttonCtrlState = 1;
int buttonCtrlPrev  = 1;

int button1Pin = 4; // Button 1 - Sensorschalter
int button1State = 1;
int button1Prev  = 1;

int button2Pin = 5; // Button 2 (not used at the moment)
int button2State = 1;
int button2Prev  = 1;

const int colorR = 255;
const int colorG = 255;
const int colorB = 0;

const int potiPin = A3;

int  cntButton = -1; // counter Button, first press is 0
char outString[7];   // output String

// Communication
const byte numChars = 32;
char receivedChars[numChars];
boolean newData = false;

char   dataInfo[30]; // for string build

// ################################################################################
// ################################################################################

 
void setup() 
{
    Serial.begin(115200); // 9600 115200
    Serial.println("<Arduino is ready>");

    AFMS_1.begin();  // create with the default frequency 1.6KHz
    AFMS_2.begin();  // create with the default frequency 1.6KHz
    myMotor1->setSpeed(10);  // 10 rpm 
    myMotor2->setSpeed(10);  // 10 rpm 
    // myMotor3->setSpeed(10);  // 10 rpm 
    myDCMotor3a->setSpeed(255);  // 0 to 255 
    myDCMotor3b->setSpeed(255);  // 0 to 255
    myMotor4->setSpeed(10);  // 10 rpm 

    // Attach a servo to pin #10
    servo1.attach(10);
    servo2.attach(9);
  
    pinMode(potiPin,       INPUT);
    pinMode(buttonCtrlPin, INPUT);   
    pinMode(ledCtrlPin,    OUTPUT);
    pinMode(button1Pin,    INPUT);
    pinMode(button2Pin,    INPUT);
     
    mylcd.begin(16, 2);
    mylcd.setRGB(colorR, colorG, colorB);
    mylcd.setCursor(0, 0); 
    mylcd.print("4xStepper V1.5 D");
    mylcd.setCursor(0, 1); 
    mylcd.print("A# S# INFOS    x");

    delay(200);
}


// ################################################################################
// ################################################################################

 
void loop() 
{

    String dataSend;

    recvWithStartEndMarkers();
    showNewData();
            
    // >>>>> BUTTON digital - detect button cnage
    // ---------------------------------------------------

    button1State = digitalRead(button1Pin);
    if (button1State != button1Prev) {
        dataSend = "B"+String(!button1State); // Button 1
        Serial.println(dataSend);  // send to pc/unity
        button1Prev = button1State;
        mylcd.setCursor(15, 0);
        mylcd.print(button1State);
    }

    buttonCtrlState = digitalRead(buttonCtrlPin);
    if (buttonCtrlState != buttonCtrlPrev) {
        dataSend = "A"+String(!buttonCtrlState); // Button A
        Serial.println(dataSend);  // send to pc/unity
        buttonCtrlPrev = buttonCtrlState;
        mylcd.setCursor(1, 1);
        mylcd.print(!buttonCtrlState);

        if (buttonCtrlState == 1) 
        { 
            cntButton ++;     

            int state = cntButton % 14; // 
            
            mylcd.setCursor(4, 1);
            sprintf(dataInfo, "%1x", (int)state); 
            mylcd.print(dataInfo);
        
            switch (state) {
              case 0: // Stepper Motor 1
                mylcd.setCursor(6, 1);
                mylcd.print("Mot1+   ");
                // Stepper Moder 1 ON
                myMotor1->setSpeed(500);
                myMotor1->step(1000, FORWARD,   DOUBLE); 
                myMotor1->release();
                break;
                 
              case 1:  
                mylcd.setCursor(6, 1);
                mylcd.print("Mot1-   ");                
                // Stepper Moder 1 ON
                myMotor1->setSpeed(500);
                myMotor1->step(1000, BACKWARD,  DOUBLE); 
                myMotor1->release();
                break;

              case 2: // Stepper Motor 2
                mylcd.setCursor(6, 1);
                mylcd.print("Mot2+   ");                
                // Stepper Moder 2 ON
                myMotor2->setSpeed(500);
                myMotor2->step(1000, FORWARD,  DOUBLE); 
                myMotor2->release();
                break;

              case 3:  
                mylcd.setCursor(6, 1);
                mylcd.print("Mot2-   ");                
                // Stepper Moder 1 ON
                myMotor2->setSpeed(500);
                myMotor2->step(1000, BACKWARD,  DOUBLE); 
                myMotor2->release();
                break;

             case 4: // RC Motor 3a
                mylcd.setCursor(6, 1);
                mylcd.print("Mot3a+  ");                
                myDCMotor3a->setSpeed(255);
                myDCMotor3a->run(FORWARD);
                // delay(2000);
                // myDCMotor3a->run(RELEASE);
                break;

              case 5:  
                mylcd.setCursor(6, 1);
                mylcd.print("Mot3a-  ");                
                myDCMotor3a->setSpeed(255);
                myDCMotor3a->run(BACKWARD);
                // delay(2000);
                // myDCMotor3a->run(RELEASE);
                break;                

             case 6: // DC Motor 3b
                mylcd.setCursor(6, 1);
                mylcd.print("Mot3b+  ");                
                myDCMotor3b->setSpeed(255);
                myDCMotor3b->run(FORWARD);
                // delay(2000);
                // myDCMotor3b->run(RELEASE);
                break;

              case 7:  
                mylcd.setCursor(6, 1);
                mylcd.print("Mot3b-  ");                
                myDCMotor3b->setSpeed(255);
                myDCMotor3b->run(BACKWARD);
                // delay(2000);
                // myDCMotor3b->run(RELEASE);
                break; 
                
              case 8: // Stepper 4  

                myDCMotor3a->run(RELEASE); // old DC OFF
                myDCMotor3b->run(RELEASE); // old DC OFF
                
                mylcd.setCursor(6, 1);
                mylcd.print("Mot4+   ");                
                myMotor4->setSpeed(500);
                myMotor4->step(1000, FORWARD,  DOUBLE); 
                myMotor4->release();
                break;

              case 9:  
                mylcd.setCursor(6, 1);
                mylcd.print("Mot4-   ");                
                myMotor4->setSpeed(500);
                myMotor4->step(1000, BACKWARD,  DOUBLE); 
                myMotor4->release();
                break;                

              case 10:  
                mylcd.setCursor(6, 1);
                mylcd.print("Srv1+   ");                
                // Servo Motor 1 
                for (posServo1 = 0; posServo1 <= 100; posServo1 += 2) { 
                  servo1.write(posServo1);              
                  delay(15);                       
                }
                break;

              case 11:  
                mylcd.setCursor(6, 1);
                mylcd.print("Srv1-   ");                
                // Servo Motor 1 
                for (posServo1 = 100; posServo1 >= 0; posServo1 -= 2) { 
                  servo1.write(posServo1);              
                  delay(15);                       
                }
                break;

              case 12:  
                mylcd.setCursor(6, 1);
                mylcd.print("Srv2+   ");                
                // Servo Motor 2 0-170
                for (posServo2 = 0; posServo2 <= 170; posServo2 += 2) { 
                  servo2.write(posServo2);              
                  delay(15);                       
                }
                break;

              case 13:  
                mylcd.setCursor(6, 1);
                mylcd.print("Srv2-   ");                
                // Servo Motor 2 
                for (posServo2 = 170; posServo2 >= 0; posServo2 -= 2) { 
                  servo2.write(posServo2);              
                  delay(15);                       
                }
                break;
                
              default:
                mylcd.setCursor(6, 1);
                mylcd.print("ERROR");                
                break;
            }

            digitalWrite(ledCtrlPin, false);
       }
       else 
       {
            digitalWrite(ledCtrlPin, true);
       }
    }

    // ###################################################
    // ###################################################
    // ###################################################
    // helper functions - time 
    
    delay(50);  // slow it down a little bit    

    int mySec = seconds() % 60;
    int myMin = minutes();

    int mySecPuls = seconds() % 2;
    mylcd.setCursor(15, 1);
    
    if (mySecPuls == 1)
    {
      mylcd.print("x");
      digitalWrite(ledCtrlPin, true);
    }
    else
    {
      mylcd.print("o");  
      digitalWrite(ledCtrlPin, false);
    }
    delay(100);
}



void recvWithStartEndMarkers() {
    static boolean recvInProgress = false;
    static byte ndx = 0;
    char startMarker = '<';
    char endMarker = '>';
    char rc;
 
    while (Serial.available() > 0 && newData == false) {
        rc = Serial.read();

        if (recvInProgress == true) {
            if (rc != endMarker) {
                receivedChars[ndx] = rc;
                ndx++;
                if (ndx >= numChars) {
                    ndx = numChars - 1;
                }
            }
            else {
                receivedChars[ndx] = '\0'; // terminate the string
                recvInProgress = false;
                ndx = 0;
                newData = true;
            }
        }

        else if (rc == startMarker) {
            recvInProgress = true;
        }
    }
}

void showNewData() {
    if (newData == true) {
        Serial.print("Received: ... ");
        Serial.println(receivedChars);
        comControl(receivedChars);
        newData = false;
    }
}

void comControl(char receivedChars[numChars])
{
  char *cmdGot = NULL;
  cmdGot = strtok(receivedChars," ");

  String cmdMain = cmdGot;
  cmdGot = strtok(NULL, " "); 
  int cmdPar1 = atoi(cmdGot);
  cmdGot = strtok(NULL, " "); 
  int cmdPar2 = atoi(cmdGot);

  Serial.print("Cmd: #> ");
  Serial.println(cmdMain);
  Serial.print("Parameter1: #> ");
  Serial.println(cmdPar1);
  Serial.print("Parameter2: #> ");
  Serial.println(cmdPar2);

  if (cmdMain=="m1")
  {
        mylcd.setCursor(6, 1);
        mylcd.print("Mot1=COM");                
        // Stepper Moder 1 ON
        myMotor1->setSpeed(cmdPar2);
        if (cmdPar1 > 0)
          myMotor1->step(cmdPar1, FORWARD,  DOUBLE); 
        else
          myMotor1->step(abs(cmdPar1), BACKWARD,  DOUBLE); 
        myMotor1->release();          
  }
  else
  if (cmdMain=="m2")
  {
        mylcd.setCursor(6, 1);
        mylcd.print("Mot2=COM");                
        // Stepper Moder 2
        myMotor2->setSpeed(cmdPar2);
        if (cmdPar1 > 0)
          myMotor2->step(cmdPar1, FORWARD,  DOUBLE); 
        else
          myMotor2->step(abs(cmdPar1), BACKWARD,  DOUBLE); 
        myMotor2->release();          
  } 
  
  if (cmdMain=="m3a")
  {
    mylcd.setCursor(6, 1);
    mylcd.print("Mot3a=CM");                
    // DC Motor 3a 
    myDCMotor3a->setSpeed(cmdPar2);
    if (cmdPar1 > 0)
      myDCMotor3a->run(FORWARD); 
    else
    if (cmdPar1 < 0)
      myDCMotor3a->run(BACKWARD);
    else
      myDCMotor3a->run(RELEASE);
  } 


  if (cmdMain=="m3b")
  {
    mylcd.setCursor(6, 1);
    mylcd.print("Mot3b=CM");                
    // DC Motor 3b 
    myDCMotor3b->setSpeed(cmdPar2);
    if (cmdPar1 > 0)
      myDCMotor3b->run(FORWARD); 
    else
    if (cmdPar1 < 0)
      myDCMotor3b->run(BACKWARD);
    else
      myDCMotor3b->run(RELEASE);         
  } 



  if (cmdMain=="m4")
  {
        mylcd.setCursor(6, 1);
        mylcd.print("Mot4=COM");                
        // Stepper Moder 2
        myMotor4->setSpeed(cmdPar2);
        if (cmdPar1 > 0)
          myMotor4->step(cmdPar1, FORWARD,  DOUBLE); 
        else
          myMotor4->step(abs(cmdPar1), BACKWARD,  DOUBLE); 
        myMotor4->release();          
  }
  else
  if (cmdMain=="s1")
  {
        mylcd.setCursor(6, 1);
        mylcd.print("Srv1=COM");                
        // Servo1 POS
        servo1.write(cmdPar1);             
  }
  else
  if (cmdMain=="s2")
  {
        mylcd.setCursor(6, 1);
        mylcd.print("Srv2=COM");                
        // Servo2 POS
        servo2.write(cmdPar1);             
  }  
  else  
  {
    mylcd.setCursor(6, 1);
    mylcd.print(cmdMain); 
  }
}

// EOF - EOP - NIS
// ;)
