//
// Arduino DAT Code
// Motor Shield
// author: Alexander Nischelwitzer, FHJ IMA/AIM DAT DMT
// last update: 03.05.2021
// 
// HARDWARE:
// Motor Shield ca. 20€
// https://learn.adafruit.com/adafruit-motor-shield-v2-for-arduino
// Grove Shield 10€
// Arduino UNO 20€
// Stackable headers 2€
// RGB ca. 15€
// http://wiki.seeedstudio.com/Grove-LCD_RGB_Backlight/ 
//
// RESERVED PINS:
// D04 D05 >> motor shield, I2C Adress 0x60
// D09 D10 >> reserved >> servos
//
// USAGE:
// DC Motor (max 4) M1 its 1, M2 use 2, M3 use 3 and M4 use 4
// Stepper Motor (max 2)
// RC Servos (max 2)
//
// #######################################

#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"
#include "rgb_lcd.h"
#include <Servo.h>

rgb_lcd mylcd;
Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 

// M1 its 1, M2 use 2, M3 use 3 and M4 use 4
// Adafruit_DCMotor *myMotorDC = AFMS.getMotor(2);

Adafruit_StepperMotor *myMotor1 = AFMS.getStepper(200, 1); // klein 
Adafruit_StepperMotor *myMotor2 = AFMS.getStepper(200, 2); // gross

// see https://github.com/adafruit/Adafruit_Motor_Shield_V2_Library/blob/master/Adafruit_MotorShield.h

// Servo myservo09; 
// Servo myservo10; 
// int pos = 0;    // variable to store the servo position

#define seconds() (millis()/1000)
#define minutes() (millis()/60000)

// ################################################################################
// ################################################################################
// SETUP
// define Input and Outputs  
  
const int ledCtrlPin    = 2; // connector on D2
const int buttonCtrlPin = 3;
int buttonCtrlState = 1;
int buttonCtrlPrev  = 1;

int button1Pin = 4; // Motor 1 - X small
int button1State = 1;
int button1Prev  = 1;

int button2Pin = 5; // Motor 2 - Y  big
int button2State = 1;
int button2Prev  = 1;

int button3Pin = 6; // for electro magnet - later endstop 
int button3State = 1;
int button3Prev  = 1;
int magnetPin = 7; 

const int colorR = 255;
const int colorG = 255;
const int colorB = 0;

const int potiPin = A3;

int  cntButton = 0; // counter Button
char outString[7];  // output String
  
// ################################################################################
// ################################################################################

 
void setup() 
{
    Serial.begin(115200); // 9600 115200
    // myservo09.attach(9);
    // myservo10.attach(10);

    AFMS.begin();  // create with the default frequency 1.6KHz
    myMotor1->setSpeed(10);  // 10 rpm 
    myMotor2->setSpeed(10);  // 10 rpm 
  
    pinMode(potiPin,       INPUT);
    pinMode(buttonCtrlPin, INPUT);   
    pinMode(ledCtrlPin,    OUTPUT);
    pinMode(button1Pin,    INPUT);
    pinMode(button2Pin,    INPUT);

    pinMode(button3Pin,    INPUT);
    pinMode(magnetPin,    OUTPUT);
     
    mylcd.begin(16, 2);
    mylcd.setRGB(colorR, colorG, colorB);
    mylcd.setCursor(0, 0); 
    mylcd.print("PKran_V0.3 ### D");
    mylcd.setCursor(0, 1); 
    mylcd.print("A# M1# M2# ### x");

    delay(200);
}


// ################################################################################
// ################################################################################

 
void loop() 
{

    String dataSend;

    // >>>>> Analog
    // ---------------------------------------------------

    int potValue = 0;
    potValue = analogRead(potiPin);
    if (potValue > 1000) potValue = 999;
    sprintf(outString,"%03d",potValue); 
    mylcd.setCursor(11, 0);
    mylcd.print(outString);
            
    // >>>>> BUTTON digital - detect button cnage
    // ---------------------------------------------------
 
    buttonCtrlState = digitalRead(buttonCtrlPin);
    if (buttonCtrlState != buttonCtrlPrev) {
        dataSend = "A"+String(!buttonCtrlState); // Button A
        Serial.println(dataSend);  // send to pc/unity
        buttonCtrlPrev = buttonCtrlState;
        mylcd.setCursor(1, 1);
        mylcd.print(!buttonCtrlState);

        if (buttonCtrlState == 1) 
        { 
            cntButton ++;     
            digitalWrite(ledCtrlPin, false);
       }
       else 
       {
            digitalWrite(ledCtrlPin, true);
       }
    }

    // ###################################################

    // BUTTON 1 MOTOR 1
    // ------------------------

    button1State = digitalRead(button1Pin);
    if (button1State != button1Prev) {
        dataSend = "M1"+String(!button1State); // Button B
        Serial.println(dataSend);  // send to pc/unity
        button1Prev = button1State;
        mylcd.setCursor(5, 1);
        mylcd.print(!button1State);

        if (button1State == 1) 
        { 
            // Stepper Moder 1 ON
            myMotor1->setSpeed(potValue+1);
            myMotor1->step(1000, FORWARD,   DOUBLE); 
            myMotor1->step(1000, BACKWARD,  DOUBLE); 
            myMotor1->release();
       }
    }
    
    // BUTTON 2 MOTOR 2
    // ------------------------

    button2State = digitalRead(button2Pin);
    if (button2State != button2Prev) {
        dataSend = "M2"+String(!button2State); // Button 2 M2
        Serial.println(dataSend);  // send to pc/unity
        button2Prev = button2State;
        mylcd.setCursor(9, 1);
        mylcd.print(!button2State);

        if (button2State == 1) 
        { 
            // Stepper Moder 2 ON
            myMotor2->setSpeed(potValue+1);
            myMotor2->step(1000, FORWARD,  DOUBLE); 
            myMotor2->step(1000, BACKWARD,  DOUBLE); 
            myMotor2->release();
       }
    }

    // BUTTON 3 - Electro Magnet
    // ------------------------

    button3State = digitalRead(button3Pin);
    if (button3State != button3Prev) {
        dataSend = "E1"+String(!button2State); // Button 3
        Serial.println(dataSend);  // send to pc/unity
        button3Prev = button3State;
        mylcd.setCursor(11, 1);
        mylcd.print(button3State);

        digitalWrite(magnetPin, button3State);
    }
    
    // ###################################################
    // Stepper Motors

    // myMotor->step(100, BACKWARD, MICROSTEP); // OK
    // myMotor->step(100, BACKWARD, SINGLE);  // BAD
    // myMotor->step(200, FORWARD,  INTERLEAVE); // BAD

    // ###################################################
    // DC Motors
           
    // myMotorDC->setSpeed(100);
    // myMotorDC->run(FORWARD);
            
    // ###################################################
    // SERVO
    
    /*
    if (pos == 170) pos=0;
    pos += 2;
    myservo10.write(pos);              // tell servo to go to position in variable 'pos'
    sprintf(outString,"%03d",pos);   
    mylcd.setCursor(11, 0);         
    mylcd.print(outString);
    */
    
    // ###################################################

    delay(50);  // slow it down a little bit    

    int mySec = seconds() % 60;
    int myMin = minutes();
    // sprintf(timeString,"%04d:%02d",myMin,mySec); 
    // mylcd.print(timeString);

    int mySecPuls = seconds() % 2;
    mylcd.setCursor(15, 1);
    
    if (mySecPuls == 1)
    {
      mylcd.print("x");
      digitalWrite(ledCtrlPin, true);
    }
    else
    {
      mylcd.print("o");  
      digitalWrite(ledCtrlPin, false);
    }

 
    delay(100);
}
