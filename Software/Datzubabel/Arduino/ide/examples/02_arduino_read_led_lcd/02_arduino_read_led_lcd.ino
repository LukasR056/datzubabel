
//
//
// python communication example
//
//

// LCD
// https://wiki.seeedstudio.com/I2C_LCD/

#include <Wire.h>
#include <I2C_LCD.h>
I2C_LCD LCD;
uint8_t I2C_LCD_ADDRESS = 0x51;

// -----------------------------------------------------

const byte ledPin    = 2; // cable yellow (first) 
const byte buttonPin = 3; // cable white (second)

bool buttonState = 1;
bool buttonPrev  = 1;
bool toggleState = false;

int incoming_num;

// ##############################################################

void setup() {
  Serial.begin(115200);
  Serial.setTimeout(1);


  Wire.begin();         //I2C controller initialization.
  LCD.CleanAll(WHITE);    //Clean the screen with black or white.
  LCD.FontModeConf(Font_6x8, FM_ANL_AAA, BLACK_BAC); 
  LCD.CharGotoXY(0,0);       //Set the start coordinate.
  LCD.print("DAT Python2Arduino");  
  delay(1000);            //Delay for 1s.
    
  pinMode(buttonPin, INPUT);    
  pinMode(ledPin,   OUTPUT);    
}

// ##############################################################


void loop() {
  
  String dataSend;
  char   dataInfo[7];

  // while (!Serial.available());
  if (Serial.available() > 0) {

    toggleState = !toggleState;
    digitalWrite(ledPin, toggleState); 
    delay(200);
    
    incoming_num = Serial.readString().toInt();
    Serial.print(incoming_num + 1); // send increment back to pc
    LCD.CharGotoXY(0,10);       //Set the start coordinate.
    LCD.print(incoming_num);
  }
  
  // ###################################################
  // LED Test-BUTTON (optional: with LED)
      
  buttonState = digitalRead(buttonPin);
  if (buttonState != buttonPrev) {
    dataSend = "A"+String(buttonState); // Button A
    Serial.println(dataSend);  // send to pc/unity
    digitalWrite(ledPin, buttonState); 
    buttonPrev = buttonState;
  }

  // ###################################################
  // ################################################### 

}
