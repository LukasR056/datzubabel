//
// Arduino Project DATzuBabel
// Motor Shield
// author: Alexander Schadler, FHJ DAT20
// adapted script from Alexander Nischelwitzer, FHJ IMA/AIM DAT DMT 
// last update: 28.09.2021

// HARDWARE:
// Motor Shield ca. 20€
// https://learn.adafruit.com/adafruit-motor-shield-v2-for-arduino
// Grove Shield 10€
// Arduino UNO 20€
// Stackable headers 2€

//
// RESERVED PINS:
// D04 D05 >> motor shield, I2C Adress 0x60

#include "protothreads.h"
#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include <Adafruit_VL53L0X.h>

Adafruit_MotorShield AFMS_1(0x60);

Adafruit_VL53L0X lox = Adafruit_VL53L0X();

// https://learn.adafruit.com/adafruit-motor-shield-v2-for-arduino/using-stepper-motors
Adafruit_StepperMotor *myMotor1 = AFMS_1.getStepper(200, 1);

// Communication
const byte numChars = 32;
char receivedChars[numChars];
boolean newData = false;
boolean m1Run = false;
boolean m1Forward = true;


// Position control
int m1StepsToGo = -1;

// VL53L0X Time of Flight Distance Sensor
boolean readLox = false;

// ################################################################################
// ################################################################################
// SETUP
// define Input and Outputs  

long debounceDelay = 50;    // the debounce time; increase if the output flickers

int motorswitch1MaxPin = 3; //endstop 
int motorswitch1MaxState = HIGH;
int motorswitch1MaxPrev = HIGH;
long lastDebounceTimeMS1Max = 50;  // the last time the output pin was toggled
boolean chflagMS1Max = false;

long lastDebounceTimeLox = 0;

int countStepsM1 = 0;
boolean calibration_m1 = false;
boolean calibration_laser = false;

int lox_measurement = 0; 

// ### PROTOTHREAD


pt ptRange;
int rangeThread(struct pt* pt) {
  PT_BEGIN(pt);

  // Loop forever
  for(;;) {
    if (calibration_m1) {
      lox_measurement = lox.readRange();
    }
    PT_SLEEP(pt, 200);
  }

  PT_END(pt);
}


// ################################################################################
// ################################################################################

void setup() 
{
  Serial.begin(115200); // 9600 115200
  Serial.println("<Arduino is ready>");

  AFMS_1.begin();  // create with the default frequency 1.6KHz
 
  myMotor1->setSpeed(10);  // 10 rpm
   
  pinMode(motorswitch1MaxPin,    INPUT);

  if (!lox.begin()) {
    Serial.println(F("Failed to boot VL53L0X"));
    while(1);
  }

   PT_INIT(&ptRange);
}



void loop() {
    PT_SCHEDULE(rangeThread(&ptRange));
    permanentRun();
    recvWithStartEndMarkers();
    showNewData();
}


void permanentRun(){
  // No Serial command is receiving ... 
  if ( Serial.available() <= 0)
    {
      VL53L0X_RangingMeasurementData_t measure;
      if (readLox){
        lox.rangingTest(&measure, false);
        if (measure.RangeStatus != 4) {  // phase failures have incorrect data
          Serial.println("STATUS lox measured " + String(measure.RangeMilliMeter));
        } else {
          Serial.println("STATUS lox measured -1");
        }
       readLox = false; 
      }

      if (calibration_laser == true) {
        lox.rangingTest(&measure, false);
        Serial.println("STATUS lox calibration-done " + String(measure.RangeMilliMeter));
        calibration_laser = false;
      }


/*      if (lastDebounceTimeLox != 0) {
        if ((millis() - lastDebounceTimeLox) > 100) {
          int measure = lox.readRange();
          //lox.rangingTest(&measure, false);
          lastDebounceTimeLox = millis();
        }
      }
      else {
          lox.rangingTest(&measure, false);
          lastDebounceTimeLox = millis();
          }   
*/
      
      if (m1Run || (m1StepsToGo > 0 && countStepsM1 <= m1StepsToGo)){
        if (m1Forward == true && motorswitch1MaxState == HIGH) {
          myMotor1->step(1, FORWARD,  DOUBLE);
          if (m1StepsToGo > 0 ) {
            countStepsM1 ++;
          }
        }
        else if ( m1Forward == false && (lox_measurement == 0 || (lox_measurement > 30 && calibration_m1))) {
          myMotor1->step(1, BACKWARD,  DOUBLE);
          if (m1StepsToGo > 0 || calibration_m1)
            countStepsM1 ++;
        }
      }

      if ( lox_measurement != 0 && lox_measurement <= 30 && calibration_m1 ) {
          myMotor1->step(0, FORWARD,  DOUBLE);
          myMotor1->release();
          m1Run = false;
          Serial.println("STATUS m1 calibration-done " + String(countStepsM1));
          Serial.println("STATUS m1 min-motor-switch-changed " + String(0));
          countStepsM1 = 0;
          calibration_m1 = false;
        } 
       
      if (m1StepsToGo == countStepsM1){
        Serial.println("STATUS m1 m1StepsToGo " + String(m1StepsToGo));
        Serial.println("STATUS m1 countStepsM1 " + String(countStepsM1));
        if (m1Forward == false) {
            // Convert back to relative negative value for status message
            countStepsM1 = countStepsM1 * -1;
        }
        Serial.println("STATUS m1 position-reached " + String(countStepsM1));
        if (motorswitch1MaxState != LOW ) {
          Serial.println("STATUS m1 motor-state " + String(0));
        }
        countStepsM1 = 0;
        m1StepsToGo = -1;
        m1Run = false;
        myMotor1->release();
      }

    
       motorswitch1MaxState = digitalRead(motorswitch1MaxPin);
  
      if (motorswitch1MaxState != motorswitch1MaxPrev) {
        lastDebounceTimeMS1Max = millis();
        chflagMS1Max = true;
        motorswitch1MaxPrev = motorswitch1MaxState;
      }
      
       if ((millis() - lastDebounceTimeMS1Max) > debounceDelay) {
        if (chflagMS1Max) {
          chflagMS1Max = false;
          if (motorswitch1MaxState == LOW) {
            m1Run = false;
            myMotor1->step(0, FORWARD,  DOUBLE);
            myMotor1->release();
          }
          Serial.println("STATUS m1 max-motor-switch-changed " + String(motorswitch1MaxState));
        }
      }
    }
}

void recvWithStartEndMarkers() {
  static boolean recvInProgress = false;
  static byte ndx = 0;
  char startMarker = '<';
  char endMarker = '>';
  char rc;

  while (Serial.available() > 0 && newData == false) {
      rc = Serial.read();

      if (recvInProgress == true) {
          if (rc != endMarker) {
              receivedChars[ndx] = rc;
              ndx++;
              if (ndx >= numChars) {
                  ndx = numChars - 1;
              }
          }
          else {
              receivedChars[ndx] = '\0'; // terminate the string
              recvInProgress = false;
              ndx = 0;
              newData = true;
          }
      }

      else if (rc == startMarker) {
          recvInProgress = true;
      }
   }
}

void showNewData() {
  if (newData == true) {
      //Serial.print("Received: ... ");
      //Serial.println(receivedChars);
      comControl(receivedChars);
      newData = false;
  }
}

// API control
// 1. param -> mode: RUN (run infinity), 
//                   STOP (stop motor), 
//                   SPEED (set speed),
//                   POS (set step and speed size)
//                   MAGON (switch magnet on)
//                   MAGOFF (switch magnet off)
//                   CALIBRATION
// 2. param -> choose motor/magnet: like m1, m2, mg1
// 3. param -> for mode RUN its the direction (1 for forward, -1 for backward), for mode SPEED its the speed size, for mode POS its the step size
// 4. param -> for mode RUN and POS its the speed size 
void comControl(char receivedChars[numChars])
{
  char *cmdGot = NULL;
  cmdGot = strtok(receivedChars," ");
  String cmdMode = cmdGot;

  while(cmdGot != NULL)
  {
    cmdGot = strtok(NULL, " ");
    String cmdMain = cmdGot;
    cmdGot = strtok(NULL, " ");
    int cmdPar1 = atoi(cmdGot);
    cmdGot = strtok(NULL, " "); 
    int cmdPar2 = atoi(cmdGot);

    if (cmdMode == "RUN")
    {
      actionRun(cmdMain, cmdPar1, cmdPar2);
    }
    else
    if (cmdMode == "STOP")
    {
      actionStop(cmdMain);
    }
    else
    if (cmdMode == "SPEED")
    {
      actionSetSpeed(cmdMain, cmdPar1);
    }
    else
    if (cmdMode == "POS")
    {
      actionSetPos(cmdMain, cmdPar1, cmdPar2);
    }
    if(cmdMode == "MEASUREMENT")
    {
      actionGetDistance();
    }
    if(cmdMode == "CALIBRATION")
    {
      actionCalibrate(cmdMain);
    }
  }
}

void actionGetDistance() {
  readLox = true;
}

void actionCalibrate(String motor) {
   if (motor == "lox") {
    calibration_laser = true;
   }
   
   if (motor == "m1") {
    calibration_m1 = true;
    countStepsM1 = 0;
   }
}

void actionRun(String motor, int directive, int mSpeed) {

  if (motor == "m1") {
    m1Run = true;
    if (directive == -1) {
      m1Forward = false;
      Serial.println("STATUS m1 motor-state " + String(-1));
    }
    else
    {
      m1Forward = true;
      if (motorswitch1MaxState == HIGH) {
        Serial.println("STATUS m1 motor-state " + String(1));
      }      
    }
  }
  actionSetSpeed(motor, mSpeed);
}

void actionStop(String motor){
  if (motor == "m1") {
    m1Run = false;
    myMotor1->step(0, FORWARD,  DOUBLE);
    myMotor1->release();
    if (motorswitch1MaxState != LOW) {
      Serial.println("STATUS m1 motor-state " + String(0));
    }
  }
}

void actionSetSpeed(String motor, int mSpeed){
  if (motor == "m1") {
    myMotor1->setSpeed(mSpeed);
  }
}


void actionSetPos(String motor, int mStep, int mSpeed){
  
  if (motor == "m1") {
    // Stepper Moder 1 ON
    lox_measurement = 0;
    countStepsM1 = 0;
    myMotor1->setSpeed(mSpeed);
    if (mStep > 0){
      m1StepsToGo = mStep;
      m1Forward = true;
    } else {
      m1StepsToGo = abs(mStep);
      m1Forward = false;
    }
     m1Run = true;
  }
}
