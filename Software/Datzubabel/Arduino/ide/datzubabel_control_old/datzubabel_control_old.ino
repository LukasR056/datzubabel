//
// Arduino Project DATzuBabel
// Motor Shield
// author: Alexander Schadler, FHJ DAT20
// adapted script from Alexander Nischelwitzer, FHJ IMA/AIM DAT DMT 
// last update: 28.09.2021

// HARDWARE:
// Motor Shield ca. 20€
// https://learn.adafruit.com/adafruit-motor-shield-v2-for-arduino
// Grove Shield 10€
// Arduino UNO 20€
// Stackable headers 2€

//
// RESERVED PINS:
// D04 D05 >> motor shield, I2C Adress 0x60

#include <Wire.h>
#include <Adafruit_MotorShield.h>

Adafruit_MotorShield AFMS_1(0x60);
Adafruit_MotorShield AFMS_2(0x61);

// https://learn.adafruit.com/adafruit-motor-shield-v2-for-arduino/using-stepper-motors
Adafruit_StepperMotor *myMotor1 = AFMS_1.getStepper(200, 1);
Adafruit_StepperMotor *myMotor2 = AFMS_2.getStepper(200, 1);
Adafruit_StepperMotor *myMotor3 = AFMS_2.getStepper(200, 2);



// Communication
const byte numChars = 32;
char receivedChars[numChars];
boolean newData = false;
boolean m1Run = false;
boolean m1Forward = true;
boolean m2Run = false;
boolean m2Forward = true;
boolean m3Run = false;
boolean m3Forward = true;

// Position control
int m1StepsToGo = -1;
int m2StepsToGo = -1;
int m3StepsToGo = -1;


// ################################################################################
// ################################################################################
// SETUP
// define Input and Outputs  

int magnet1Pin = 7;

long debounceDelay = 50;    // the debounce time; increase if the output flickers

int motorswitch1MaxPin = 3; //endstop 
int motorswitch1MaxState = HIGH;
int motorswitch1MaxPrev = HIGH;
long lastDebounceTimeMS1Max = 50;  // the last time the output pin was toggled
boolean chflagMS1Max = false;

int motorswitch2MinPin = 8; //endstop 
int motorswitch2MinState = HIGH;
int motorswitch2MinPrev = HIGH;
long lastDebounceTimeMS2Min = 50;  // the last time the output pin was toggled
boolean chflagMS2Min = false;

int motorswitch2MaxPin = 6; //endstop 
int motorswitch2MaxState = HIGH;
int motorswitch2MaxPrev = HIGH;
long lastDebounceTimeMS2Max = 50;  // the last time the output pin was toggled
boolean chflagMS2Max = false;

int motorswitch3MinPin = 5; //endstop 
int motorswitch3MinState = HIGH;
int motorswitch3MinPrev = HIGH;
long lastDebounceTimeMS3Min = 50;  // the last time the output pin was toggled
boolean chflagMS3Min = false;

int motorswitch3MaxPin = 4; //endstop 
int motorswitch3MaxState = HIGH;
int motorswitch3MaxPrev = HIGH;
long lastDebounceTimeMS3Max = 50;  // the last time the output pin was toggled
boolean chflagMS3Max = false;

boolean calibration_m2 = false;
boolean calibration_m3 = false;
int countStepsM1 = 0;
int countStepsM2 = 0;
int countStepsM3 = 0;

// ################################################################################
// ################################################################################

void setup() 
{
  Serial.begin(115200); // 9600 115200
  Serial.println("<Arduino is ready>");

  AFMS_1.begin();  // create with the default frequency 1.6KHz
  AFMS_2.begin();  // create with the default frequency 1.6KHz

  myMotor1->setSpeed(10);  // 10 rpm
  myMotor2->setSpeed(10);  // 10 rpm
  myMotor3->setSpeed(10);  // 10 rpm
   
  pinMode(magnet1Pin,    OUTPUT);
  pinMode(motorswitch1MaxPin,    INPUT);
  pinMode(motorswitch2MaxPin,    INPUT);
  pinMode(motorswitch2MinPin,    INPUT);
  pinMode(motorswitch3MinPin,    INPUT);
  pinMode(motorswitch3MaxPin,    INPUT);
}



void loop() {
    permanentRun();
    recvWithStartEndMarkers();
    showNewData();
}

void readMotorEndSwitch(){
  
}

void permanentRun(){
  // No Serial command is receiving ... 
  if ( Serial.available() <= 0)
    {
      if (m1Run || (m1StepsToGo > 0 && countStepsM1 <= m1StepsToGo)){
        if (m1Forward == true && motorswitch1MaxState == HIGH)
          myMotor1->onestep(FORWARD,  DOUBLE);
          if (m1StepsToGo > 0 )
            countStepsM1 ++;
        else if ( m1Forward == false)
          myMotor1->onestep(BACKWARD,  DOUBLE);
          if (m1StepsToGo > 0 )
            countStepsM1 ++;
      }
      
      if (m1StepsToGo == countStepsM1){
        if (m1Forward == false) {
            // Convert back to relative negative value for status message
            countStepsM1 = countStepsM1 * -1;
        }
        Serial.println("STATUS m1 position-reached " + String(countStepsM1));
        if (motorswitch1MaxState != LOW ) {
          Serial.println("STATUS m1 motor-state " + String(0));
        }
        countStepsM1 = 0;
        m1StepsToGo = -1;
        myMotor1->release();
      }

      if (m2Run || (m2StepsToGo > 0 && countStepsM2 <= m2StepsToGo)){
        if (m2Forward == true && motorswitch2MaxState == HIGH) {
          myMotor2->step(1, BACKWARD,  DOUBLE);
          //myMotor2->onestep(BACKWARD,  DOUBLE);
          //if (calibration_m2 == true || m2StepsToGo > 0 )
            countStepsM2 ++;
        }
        else if (m2Forward == false && motorswitch2MinState == HIGH)
        {
          myMotor2->step(1, FORWARD,  DOUBLE);
          //myMotor2->onestep(FORWARD,  DOUBLE);
          //if ( m2StepsToGo > 0 )
            countStepsM2 ++;
        }       
      }

      if (m2StepsToGo == countStepsM2 ){
        //if (m2Forward == false) {
            // Convert back to relative negative value for status message
        //    countStepsM2 = countStepsM2 * -1;
        //}
        Serial.println("STATUS m2 position-reached " + String(countStepsM2));
        if (motorswitch2MinState != LOW && motorswitch2MaxState != LOW ) {
          Serial.println("STATUS m2 motor-state " + String(0));
        }
        countStepsM2 = 0;
        m2StepsToGo = -1;
        m2Run = false;
        myMotor2->release();
        
      }

      if (m3Run || (m3StepsToGo > 0 && countStepsM3 <= m3StepsToGo)){
        if (m3Forward == true && motorswitch3MaxState == HIGH) {
          myMotor3->step(1, FORWARD,  DOUBLE);
          //if (calibration_m3 == true || m3StepsToGo > 0)
            countStepsM3 ++;
        }
          
        else if (m3Forward == false && motorswitch3MinState == HIGH) {
          myMotor3->step(1, BACKWARD,  DOUBLE);
          //if ( m3StepsToGo > 0 )
            countStepsM3 ++;
        }
      }

      if (m3StepsToGo == countStepsM3){
        //if (m3Forward == false) {
            // Convert back to relative negative value for status message
       //     countStepsM3 = countStepsM3 * -1;
       // }
        Serial.println("STATUS m3 position-reached " + String(countStepsM3));
        if (motorswitch3MinState != LOW && motorswitch3MaxState != LOW ) {
          Serial.println("STATUS m3 motor-state " + String(0));
        }
        countStepsM3 = 0;
        m3StepsToGo = -1;
        m3Run = false;
        myMotor3->release();
      }

  //     motorswitch1MaxState = digitalRead(motorswitch1MaxPin);
       motorswitch2MinState = digitalRead(motorswitch2MinPin);
       motorswitch2MaxState = digitalRead(motorswitch2MaxPin);
       motorswitch3MinState = digitalRead(motorswitch3MinPin);
       motorswitch3MaxState = digitalRead(motorswitch3MaxPin);

/*
      if (motorswitch1MaxState != motorswitch1MaxPrev) {
        lastDebounceTimeMS1Max = millis();
        chflagMS1Max = true;
        motorswitch1MaxPrev = motorswitch1MaxState;
      }
 */     
      if (motorswitch2MaxState != motorswitch2MaxPrev) {
        lastDebounceTimeMS2Max = millis();
        chflagMS2Max = true;
        motorswitch2MaxPrev = motorswitch2MaxState;
      }

      if (motorswitch2MinState != motorswitch2MinPrev) {
        lastDebounceTimeMS2Min = millis();
        chflagMS2Min = true;
        motorswitch2MinPrev = motorswitch2MinState;
      }

      if (motorswitch3MinState != motorswitch3MinPrev) {
        lastDebounceTimeMS3Min = millis();
        chflagMS3Min = true;
        motorswitch3MinPrev = motorswitch3MinState;
      }

       if (motorswitch3MaxState != motorswitch3MaxPrev) {
        lastDebounceTimeMS3Max = millis();
        chflagMS3Max = true;
        motorswitch3MaxPrev = motorswitch3MaxState;
      }

/*
       if ((millis() - lastDebounceTimeMS1Max) > debounceDelay) {
        if (chflagMS1Max) {
          chflagMS1Max = false;
          if (motorswitch1MaxState == LOW) {
            m1Run = false;
            myMotor1->step(0, FORWARD,  DOUBLE);
            myMotor1->release();
          }
          Serial.println("STATUS m1 max-motor-switch-changed " + String(motorswitch1MaxState));
        }
      }
*/
       if ((millis() - lastDebounceTimeMS2Min) > debounceDelay) {
        if (chflagMS2Min) {
          chflagMS2Min = false;
          if (motorswitch2MinState == LOW) {
            myMotor2->step(0, FORWARD,  DOUBLE);
            myMotor2->release();
            Serial.println("STATUS m2 position-reached " + String(countStepsM2));
          }
          Serial.println("STATUS m2 min-motor-switch-changed " + String(motorswitch2MinState));
        }
      }

      if ((millis() - lastDebounceTimeMS2Max) > debounceDelay) {
        if (chflagMS2Max) {
          chflagMS2Max = false;
          if (motorswitch2MaxState == LOW) {
            myMotor2->step(0, FORWARD,  DOUBLE);
            myMotor2->release();
            Serial.println("STATUS m2 position-reached " + String(countStepsM2));
            if (calibration_m2 == true) {
              Serial.println("STATUS m2 calibration-done " + String(countStepsM2));
              calibration_m2 = false;
              countStepsM2 = 0;
            }
          }
          Serial.println("STATUS m2 max-motor-switch-changed " + String(motorswitch2MaxState));
        }
      }

       if ((millis() - lastDebounceTimeMS3Min) > debounceDelay) {
        if (chflagMS3Min) {
          chflagMS3Min = false;
          if (motorswitch3MinState == LOW) {
            myMotor3->step(0, FORWARD,  DOUBLE);
            myMotor3->release();
            Serial.println("STATUS m3 position-reached " + String(countStepsM3));
          }
          Serial.println("STATUS m3 min-motor-switch-changed " + String(motorswitch3MinState));
        }
      }

      if ((millis() - lastDebounceTimeMS3Max) > debounceDelay) {
        if (chflagMS3Max) {
          chflagMS3Max = false;
          if (motorswitch3MaxState == LOW) {
            myMotor3->step(0, FORWARD,  DOUBLE);
            myMotor3->release();
            Serial.println("STATUS m3 position-reached " + String(countStepsM3));
             if (calibration_m3 == true) {
              Serial.println("STATUS m3 calibration-done " + String(countStepsM3));
              calibration_m3  = false;
              countStepsM3 = 0;
             }
          }
          Serial.println("STATUS m3 max-motor-switch-changed " + String(motorswitch3MaxState));
        }
      }
      
    }
}

void recvWithStartEndMarkers() {
  static boolean recvInProgress = false;
  static byte ndx = 0;
  char startMarker = '<';
  char endMarker = '>';
  char rc;

  while (Serial.available() > 0 && newData == false) {
      rc = Serial.read();

      if (recvInProgress == true) {
          if (rc != endMarker) {
              receivedChars[ndx] = rc;
              ndx++;
              if (ndx >= numChars) {
                  ndx = numChars - 1;
              }
          }
          else {
              receivedChars[ndx] = '\0'; // terminate the string
              recvInProgress = false;
              ndx = 0;
              newData = true;
          }
      }

      else if (rc == startMarker) {
          recvInProgress = true;
      }
   }
}

void showNewData() {
  if (newData == true) {
      //Serial.print("Received: ... ");
      //Serial.println(receivedChars);
      comControl(receivedChars);
      newData = false;
  }
}

// API control
// 1. param -> mode: RUN (run infinity), 
//                   STOP (stop motor), 
//                   SPEED (set speed),
//                   POS (set step and speed size)
//                   MAGON (switch magnet on)
//                   MAGOFF (switch magnet off)
//                   CALIBRATION
// 2. param -> choose motor/magnet: like m1, m2, mg1
// 3. param -> for mode RUN its the direction (1 for forward, -1 for backward), for mode SPEED its the speed size, for mode POS its the step size
// 4. param -> for mode RUN and POS its the speed size 
void comControl(char receivedChars[numChars])
{
  char *cmdGot = NULL;
  cmdGot = strtok(receivedChars," ");
  String cmdMode = cmdGot;

  while(cmdGot != NULL)
  {
    cmdGot = strtok(NULL, " ");
    String cmdMain = cmdGot;
    cmdGot = strtok(NULL, " ");
    int cmdPar1 = atoi(cmdGot);
    cmdGot = strtok(NULL, " "); 
    int cmdPar2 = atoi(cmdGot);

    if (cmdMode == "RUN")
    {
      actionRun(cmdMain, cmdPar1, cmdPar2);
    }
    else
    if (cmdMode == "STOP")
    {
      actionStop(cmdMain);
    }
    else
    if (cmdMode == "SPEED")
    {
      actionSetSpeed(cmdMain, cmdPar1);
    }
    else
    if (cmdMode == "POS")
    {
      actionSetPos(cmdMain, cmdPar1, cmdPar2);
    }
    if (cmdMode == "MAGON")
    {
      actionMagnet(cmdMain, true);
    }
    if (cmdMode == "MAGOFF")
    {
      actionMagnet(cmdMain, false);
    }
    if(cmdMode == "CALIBRATION")
    {
      actionCalibrate(cmdMain);
    }
  }
}

void actionCalibrate(String motor) {
   if (motor == "m2") {
    calibration_m2 = true;
    countStepsM2 = 0;
   }
   else if (motor == "m3") {
    calibration_m3 = true;
    countStepsM3 = 0;
   }
}

void actionRun(String motor, int directive, int mSpeed) {

  actionSetSpeed(motor, mSpeed);
  
  if (motor == "m1") {
    m1Run = true;
    if (directive == -1) {
      m1Forward = false;
      Serial.println("STATUS m1 motor-state " + String(-1));
    }
    else
    {
      m1Forward = true;
      if (motorswitch1MaxState == HIGH) {
        Serial.println("STATUS m1 motor-state " + String(1));
      }      
    }
  }
  else
  if (motor == "m2") {
    countStepsM2 = 0;
    m2Run = true;
    if (directive == -1) {
      m2Forward = false;
      if (motorswitch2MinState == HIGH) {
        Serial.println("STATUS m2 motor-state " + String(-1));
      }
    }
    else
    {
      m2Forward = true;
      if (motorswitch2MaxState == HIGH) {
        Serial.println("STATUS m2 motor-state " + String(1));
      }
    }
  }
  else
  if (motor == "m3") {
    countStepsM3 = 0;
    m3Run = true;
    if (directive == -1) {
      m3Forward = false;
      if (motorswitch3MinState == HIGH) {
        Serial.println("STATUS m3 motor-state " + String(-1));
      }
    }
    else
    {
      m3Forward = true;
      if (motorswitch3MaxState == HIGH) {
        Serial.println("STATUS m3 motor-state " + String(1));
      }
    }
  }
}

void actionStop(String motor){
  if (motor == "m1") {
    m1Run = false;
    myMotor1->step(0, FORWARD,  DOUBLE);
    myMotor1->release();
    if (motorswitch1MaxState != LOW) {
      Serial.println("STATUS m1 motor-state " + String(0));
    }
  }
  else
  if (motor == "m2") {
    m2Run = false;
    Serial.println("STATUS m2 position-reached " + String(countStepsM2));
    myMotor2->step(0, FORWARD,  DOUBLE);
    myMotor2->release();
    if (motorswitch2MinState != LOW && motorswitch2MaxState != LOW ) {
      Serial.println("STATUS m2 motor-state " + String(0));
    }
  }
  else
  if (motor == "m3") {
    m3Run = false;
    Serial.println("STATUS m3 position-reached " + String(countStepsM3));
    myMotor3->step(0, FORWARD,  DOUBLE);
    myMotor3->release();
    if (motorswitch3MinState != LOW && motorswitch3MaxState != LOW ) {
      Serial.println("STATUS m3 motor-state " + String(0));
    }
  }
}

void actionSetSpeed(String motor, int mSpeed){
  if (motor == "m1") {
    myMotor1->setSpeed(mSpeed);
  }
  else
  if (motor == "m2") {
    myMotor2->setSpeed(mSpeed);
  }
  else
  if (motor == "m3") {
    myMotor3->setSpeed(mSpeed);
  }
}

void actionSetPos(String motor, int mStep, int mSpeed){
  
  if (motor == "m1") {
    // Stepper Moder 1 ON
    myMotor1->setSpeed(mSpeed);
    if (mStep > 0){
      m1StepsToGo = mStep;
      m1Forward = true;
    }
      //myMotor1->step(mStep, FORWARD,  DOUBLE); 
    else {
      m1StepsToGo = abs(mStep);
      m1Forward = false;
      //myMotor1->step(abs(mStep), BACKWARD,  DOUBLE); 
      //myMotor1->release(); 
    }        
  }
  else
   if (motor == "m2") {
    // Stepper Moder 1 ON
    countStepsM2 = 0;
    myMotor2->setSpeed(mSpeed);
    if (mStep > 0) {
      m2StepsToGo = mStep;
      m2Forward = true;
    }
      //myMotor2->step(mStep, FORWARD,  DOUBLE); 
    else {
      m2StepsToGo = abs(mStep);
      m2Forward = false;
      //myMotor2->step(abs(mStep), BACKWARD,  DOUBLE); 
      //myMotor2->release();
    }          
  }
  else
   if (motor == "m3") {
    // Stepper Moder 1 ON
    myMotor3->setSpeed(mSpeed);
    countStepsM3 = 0;
    if (mStep > 0) {
      m3StepsToGo = mStep;
      m3Forward = true;
      //myMotor3->step(mStep, FORWARD,  DOUBLE); 
    }
    else {
      m3StepsToGo = abs(mStep);
      m3Forward = false;
      //myMotor3->step(abs(mStep), BACKWARD,  DOUBLE); 
      //myMotor3->release();
    }       
  }
}

void actionMagnet(String magnet, boolean active){
  
  if (magnet == "mg1") {
    // Magnet
    if (active == true) {
      digitalWrite(magnet1Pin, HIGH);
      Serial.println("STATUS mg1 magnet-state " + String(HIGH));
    }
    else
    {
      digitalWrite(magnet1Pin, LOW);
      Serial.println("STATUS mg1 magnet-state "+ String(LOW));
    }             
  }
  else
  {
    Serial.print("Wrong magnet selected!");
  }
}
