import logging

import serial
import Arduino.dummy_serial as dummy_serial

from enum import Enum, auto
from Utilities.utilitites import StringBuilder
from Arduino.hw_threads import HWInterface
from Utilities.logger import LoggerManager, Logger

log = LoggerManager.getLogger(Logger.DefaultLogger)

PLACEHOLDER = "X"


class DeviceType(Enum):
    STEP = auto()
    SERVO = auto()
    MAGNET = auto()
    LASER = auto()


class ArduinoResponse(Enum):
    STATUS = "STATUS"


class DeviceMeters(Enum):
    min_motor_switch_changed = "min-motor-switch-changed"
    max_motor_switch_changed = "max-motor-switch-changed"
    calibration_done = "calibration-done"
    motor_state = "motor-state"
    magnet_state = "magnet-state"
    position_reached = "position-reached"
    measured = "measured"


class ArduinoMode(Enum):
    RUN_PERMANENT = "RUN"
    MOVE = "POS"
    SPEED = "SPEED"
    STOP = "STOP"
    MAGNET_ON = "MAGON"
    MAGNET_OFF = "MAGOFF"
    CALIBRATION = "CALIBRATION"
    MEASUREMENT = "MEASUREMENT"
    OBJECT_ATTACHED = "OBJTRUE"
    OBJECT_RELEASED = "OBJFALSE"


class StepMotorDirection(Enum):
    ZERO = 0
    FORWARD = 1
    BACKWARD = -1


class DeviceStatus(Enum):
    STANDBY = "standby"
    ACTIVE = "active"   # not relevant
    RUN_TO_MIN = "running to min"   # "direction" of step motor
    RUN_TO_MAX = "running to max"
    STOPPED_AT_MIN = "stopped at min"
    STOPPED_AT_MAX = "stopped at max"
    MAGNET_ON = "on"
    MAGNET_OFF = "off"


class StepMotorLabel(Enum):
    X_AXIS = "x-axis"
    Y_AXIS = "y-axis"
    Z_AXIS = "z-axis"


def arduino_callback(ardunio, response):
    """ callback function to use with HW_interface class.
     Called when the target sends a byte """
    log.debug('got Arduino response "%s"' % response)
    message = response.split()

    if message[0] == ArduinoResponse.STATUS.value:
        for device in ardunio.devices:
            if device.name == message[1]:
                device.new_status = True
                if message[2] == DeviceMeters.min_motor_switch_changed.value:
                    if isinstance(device, StepMotor):
                        if int(message[3]) == 0:
                            device.set_status(DeviceStatus.STOPPED_AT_MIN)
                        elif int(message[3]) == 1:
                            device.set_status(DeviceStatus.RUN_TO_MAX)

                if message[2] == DeviceMeters.max_motor_switch_changed.value:
                    if isinstance(device, StepMotor):
                        if int(message[3]) == 0:
                            device.set_status(DeviceStatus.STOPPED_AT_MAX)
                        elif int(message[3]) == 1:
                            device.set_status(DeviceStatus.RUN_TO_MIN)

                if message[2] == DeviceMeters.calibration_done.value:
                    if isinstance(device, StepMotor):
                        device.set_max_steps(message[3])
                    if isinstance(device, Laser):
                        device.set_max_distance(int(message[3]))

                if message[2] == DeviceMeters.motor_state.value:
                    if isinstance(device, StepMotor):
                        if int(message[3]) == -1:
                            device.set_status(DeviceStatus.RUN_TO_MIN)
                        elif int(message[3]) == 0:
                            device.set_status(DeviceStatus.STANDBY)
                        elif int(message[3]) == 1:
                            device.set_status(DeviceStatus.RUN_TO_MAX)

                if message[2] == DeviceMeters.position_reached.value:
                    if isinstance(device, StepMotor):
                        device.set_last_steps(int(message[3]))

                if message[2] == DeviceMeters.magnet_state.value:
                    if isinstance(device, Magnet):
                        if int(message[3]) == 0:
                            device.set_status(DeviceStatus.MAGNET_OFF)
                        elif int(message[3]) == 1:
                            device.set_status(DeviceStatus.MAGNET_ON)

                if message[2] == DeviceMeters.measured.value:
                    if isinstance(device, Laser):
                        device.update_distance(int(message[3]))


def init_command(transaction_id):
    return {"transaction_id": transaction_id, "mode": None, "parameter": None}


def set_mode(command, mode):
    if isinstance(mode, ArduinoMode):
        command["mode"] = mode
    else:
        log.warning("mode " + mode + " is not defined")
    return command


def add_parameter(command, param1=None, param2=None):
    cmd = StringBuilder()

    if param1 is not None:
        cmd.Add(" ")
        cmd.Add(str(param1))
    if param2 is not None:
        cmd.Add(" ")
        cmd.Add(str(param2))

    command["parameter"] = cmd
    return command


class Device:
    def __init__(self, d_type, name, label=None):
        self.d_type = d_type
        self.label = label
        self.name = name
        self.commands = []
        self.status = DeviceStatus.STANDBY
        self.new_status = False

    def set_status(self, status):
        self.status = status

    def get_status(self):
        return self.status


class StepMotor(Device):
    def __init__(self, name, label):
        m_type = DeviceType.STEP
        self.speed = 6  # default speed
        self.max_steps = 0
        self.last_steps = 0
        self.direction = StepMotorDirection.FORWARD
        self.position_reached = False
        self.gui = None
        Device.__init__(self, m_type, name, label)

    def set_max_steps(self, steps):
        self.max_steps = steps

    def get_max_steps(self):
        return self.max_steps

    def set_current_speed(self, speed):
        self.speed = speed

    def get_current_speed(self):
        return self.speed

    def set_last_steps(self, steps):
        log.debug("Set last steps %d", steps)
        self.position_reached = True
        self.last_steps = steps

    def run_to_max(self, transaction_id, speed=None):
        self.direction = StepMotorDirection.FORWARD
        cmd = init_command(transaction_id)
        if speed is not None:
            self.set_current_speed(speed)

        cmd = set_mode(cmd, ArduinoMode.RUN_PERMANENT)
        cmd = add_parameter(cmd, StepMotorDirection.FORWARD.value, self.speed)
        self.commands.append(cmd)
        # TODO: implement / optimize gui update position overview
        #self.gui.update_position_overview(motor_label=self.label, steps=StepMotorDirection.FORWARD.value)

    def run_to_min(self, transaction_id, speed=None):
        self.direction = StepMotorDirection.BACKWARD
        cmd = init_command(transaction_id)
        if speed is not None:
            self.set_current_speed(speed)
        cmd = set_mode(cmd, ArduinoMode.RUN_PERMANENT)
        cmd = add_parameter(cmd, StepMotorDirection.BACKWARD.value, self.speed)
        self.commands.append(cmd)
        # TODO: implement / optimize gui update position overview
        #self.gui.update_position_overview(motor_label=self.label, steps=StepMotorDirection.BACKWARD.value)

    def stop(self, transaction_id):
        cmd = init_command(transaction_id)
        cmd = set_mode(cmd, ArduinoMode.STOP)
        cmd = add_parameter(cmd, PLACEHOLDER, PLACEHOLDER)
        self.commands.append(cmd)

    def set_speed(self, transaction_id, speed):
        cmd = init_command(transaction_id)
        self.set_current_speed(speed)
        cmd = set_mode(cmd, ArduinoMode.SPEED)
        cmd = add_parameter(cmd, self.speed, PLACEHOLDER)
        self.commands.append(cmd)

    def increase_speed(self, transaction_id):
        self.set_speed(transaction_id, self.speed + 1)

    def decrease_speed(self, transaction_id):
        self.set_speed(transaction_id, self.speed - 1)

    def move_steps(self, transaction_id, step_size, speed=None):
        if step_size < 0:
            self.direction = StepMotorDirection.BACKWARD
        else:
            self.direction = StepMotorDirection.FORWARD

        cmd = init_command(transaction_id)
        if speed is not None:
            self.set_current_speed(speed)
        cmd = set_mode(cmd, ArduinoMode.MOVE)
        cmd = add_parameter(cmd, step_size, self.speed)
        self.commands.append(cmd)
        # TODO: implement / optimize gui update position overview

    def start_calibrate(self, transaction_id):
        cmd = init_command(transaction_id)
        cmd = set_mode(cmd, ArduinoMode.CALIBRATION)
        cmd = add_parameter(cmd, PLACEHOLDER, PLACEHOLDER)
        self.commands.append(cmd)

    def __str__(self):
        return "Motor " + self.name + " [" + self.label.value + "] state: " + self.status.value


class Magnet(Device):
    def __init__(self, name):
        m_type = DeviceType.MAGNET
        self.name = name
        Device.__init__(self, m_type, name)
        self.status = DeviceStatus.MAGNET_OFF

    def magnet_on(self, transaction_id):
        cmd = init_command(transaction_id)
        set_mode(cmd, ArduinoMode.MAGNET_ON)
        self.commands.append(cmd)

    def magnet_off(self, transaction_id):
        cmd = init_command(transaction_id)
        cmd = set_mode(cmd, ArduinoMode.MAGNET_OFF)
        self.commands.append(cmd)

    def __str__(self):
        return "Magnet " + self.name + " state: " + self.status.value


class Laser(Device):
    def __init__(self, name, offset_z):
        m_type = DeviceType.LASER
        self.name = name
        Device.__init__(self, m_type, name)
        self.max_distance = -1
        self.distance = -1
        self.offset_z = offset_z
        self.calibration_status = None
        self.is_measured = False

    def update_distance(self, distance):
        self.distance = distance #- self.offset_z
        self.is_measured = True
        log.warning("Measured value: %d", distance)

    def set_max_distance(self, distance):
        self.max_distance = distance + self.offset_z
        self.calibration_status = None

    def get_max_distance(self):
        return self.max_distance

    def start_calibrate(self, transaction_id):
        self.calibration_status = None
        cmd = init_command(transaction_id)
        cmd = set_mode(cmd, ArduinoMode.CALIBRATION)
        cmd = add_parameter(cmd, PLACEHOLDER, PLACEHOLDER)
        self.commands.append(cmd)

    def get_current_distance(self, transaction_id):
        cmd = init_command(transaction_id)
        cmd = set_mode(cmd, ArduinoMode.MEASUREMENT)
        cmd = add_parameter(cmd, PLACEHOLDER, PLACEHOLDER)
        self.commands.append(cmd)


class ArduinoUno(HWInterface):
    def __init__(self, port, baudrate, sleep_time):
        try:
            ser = serial.Serial(port=port, baudrate=baudrate, timeout=.1)
        except serial.SerialException as e:
            log.critical(e, exc_info=True)
            # create dummy Serial
            ser = dummy_serial.Serial(port=port, baudrate=baudrate, timeout=.1)

        super().__init__(ser, sleep_time)
        self.start_char = "<"
        self.end_char = ">"
        self.devices = []
        self.last_cmd_sent = None
        self.transaction_id = 0
        self.transactions = []

    def get_next_transaction_id(self):
        self.transaction_id = self.transaction_id + 1
        return self.transaction_id

    def add_device(self, device):
        if isinstance(device, Device):
            self.devices.append(device)
        else:
            log.error("Device %s is not defined", device)

    def get_devices(self, device_type):
        return [device for device in self.devices if device.d_type == device_type]

    def get_motor(self, label):
        motors = self.get_devices(DeviceType.STEP)
        motor = [motor for motor in motors if motor.label == label]

        if len(motor) == 1:
            if isinstance(motor[0], StepMotor):
                return motor[0]
            else:
                log.error("motor %s isn't a instance of StepMotor.", label.name)
                return None
        elif len(motor) == 0:
            log.error("specific motor for label %s not found.", label.name)
            return None
        else:
            log.error("found more then one motor for label %s ", label.name)
            return None

    def get_motor_x(self):
        return self.get_motor(StepMotorLabel.X_AXIS)

    def get_motor_y(self):
        return self.get_motor(StepMotorLabel.Y_AXIS)

    def get_motor_z(self):
        return self.get_motor(StepMotorLabel.Z_AXIS)

    def get_magnet(self):
        magnet = self.get_devices(DeviceType.MAGNET)

        if len(magnet) == 1:
            if isinstance(magnet[0], Magnet):
                return magnet[0]
            else:
                log.error("magnet isn't a instance of Magnet.")
                return None
        elif len(magnet) == 0:
            log.error("no magnets found.")
            return None
        else:
            log.error("found more then one magnet")
            return None

    def get_laser(self):
        laser = self.get_devices(DeviceType.LASER)

        if len(laser) == 1:
            if isinstance(laser[0], Laser):
                return laser[0]
            else:
                log.error("laser isn't a instance of Laser.")
                return None
        elif len(laser) == 0:
            log.error("no lasers found.")
            return None
        else:
            log.error("found more then one laser")
            return None

    def execute(self, transaction_id):
        self.transactions.append(transaction_id)

        # get first element (probably exists more transactions)
        tid = self.transactions.pop(0)
        log.debug("Send command to arduino with transaction_id %d", tid)
        # consider only devices which have the same current transaction id
        devices = [device for device in self.devices for command in device.commands if command["transaction_id"] == tid]

        if len(devices) == 0:
            log.warning("No Devices with transaction_id %d found!", tid)
            return None

        is_gamepad_mode = False
        cmd = StringBuilder()
        cmd.Add(self.start_char)

        for i, device in enumerate(devices):
            command = [command for command in device.commands if command["transaction_id"] == tid][0]

            if i == 0:  # set mode just initial
                if command["mode"] is not None:
                    is_gamepad_mode = command["mode"] == ArduinoMode.RUN_PERMANENT
                    cmd.Add(command["mode"].value)
                else:
                    log.warning("No Mode on device: %s set", device.name)
                    return None
            cmd.Add(" ")
            cmd.Add(device.name)
            if command["parameter"] is not None:
                cmd.Add(command["parameter"].__str__())

            # remove sent command from device
            device.commands.remove(command)
        cmd.Add(self.end_char)

        # Don't send command to arduino if its the same which was send recently
        if not is_gamepad_mode or cmd.__str__() != self.last_cmd_sent:
            log.debug("Send command to arduino: %s", cmd.__str__())
            self.write_HW(bytes(cmd.__str__(), 'utf-8'))
        # save latest used command
        self.last_cmd_sent = cmd.__str__()
