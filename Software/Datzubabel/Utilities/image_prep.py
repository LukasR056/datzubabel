from PIL import Image
import os
import sys

print("Current Working Directory: ", os.getcwd())


def rename_directory():
    index = 1
    valid_images = [".jpg", ".gif", ".png", ".jpeg"]

    for pic in os.listdir():
        ext = os.path.splitext(pic)[1]
        if ext.lower() not in valid_images:
            continue
        name = "bild" + str(index) + ext.lower()
        index = index + 1
        os.rename(pic,name)


def downsize_directory():
    os.mkdir('downsized')
    valid_images = [".jpg", ".gif", ".png", ".jpeg"]

    for i in os.listdir():
        name = os.path.splitext(i)[0]
        ext = os.path.splitext(i)[1]
        if ext.lower() not in valid_images:
            continue
        img = Image.open(i)
        print(img.size)
        img = img.resize((round(img.size[0]/2.5), round(img.size[1]/2.5)), Image.ANTIALIAS)
        img.save('./downsized/' + name + '_ds_' + ext,quality=90)


def crop_directory():
    os.mkdir('cropped')
    valid_images = [".jpg", ".gif", ".png", ".jpeg"]

    for i in os.listdir():
        name = os.path.splitext(i)[0]
        ext = os.path.splitext(i)[1]
        if ext.lower() not in valid_images:
            continue
        img = Image.open(i)
        width, height = img.size

        # Setting the points for cropped image
        left = 5
        top = height / 4
        right = 164
        bottom = 3 * height / 4

        img1 = img.crop_directory((left, top, right, bottom))
        img1.save('./cropped/'+name + '_cropped_' +ext,quality=90)

if __name__ == '__main__':
    try:
        globals()[sys.argv[1]]()
    except IndexError:
        print("You need to specify the function that you want to execute.")