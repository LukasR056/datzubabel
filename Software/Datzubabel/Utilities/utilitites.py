from io import StringIO
import json
import os.path

class StringBuilder:
    string = None

    def __init__(self):
        self.string = StringIO()

    def Add(self, str):
        self.string.write(str)

    def __str__(self):
        return self.string.getvalue()


class JsonWriteReader:
    def __init__(self, file_name):
        self.file_name = file_name

    def json_exists(self):
        return os.path.exists(self.file_name)

    def read_json(self):
        with open(self.file_name, 'r') as openfile:
            # Reading from json file
            return json.load(openfile)

    def write_json(self, json_object):
        with open(self.file_name, "w") as outfile:
            outfile.write(json.dumps(json_object, indent=2))