import logging.config
import yaml
from enum import Enum


class Logger(Enum):
    FileLogger = "filelogger"
    ConsoleLogger = "consolelogger"
    DefaultLogger = "defaultlogger"


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances.keys():
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class LoggerManager(object):
    __metaclass__ = Singleton

    _loggers = {}

    def __init__(self, *args, **kwargs):
        pass

    @staticmethod
    def getLogger(name=None):
        if not name:
            logging.basicConfig()
            return logging.getLogger()
        elif name.name not in LoggerManager._loggers.keys():
            with open('logger_configuration.yml', 'r') as stream:
                config = yaml.load(stream, Loader=yaml.FullLoader)

            logging.config.dictConfig(config)
            LoggerManager._loggers[name.name] = logging.getLogger(str(name.name))
        return LoggerManager._loggers[name.name]
