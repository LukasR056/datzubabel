# https://youtu.be/XyX5HNuv-xE

"""
Source: https://github.com/bnsreenu/python_for_microscopists/blob/master/208_multiclass_Unet_sandstone.py
Author: Dr. Sreenivas Bhattiprolu

Multiclass semantic segmentation using U-Net

adapted by Janik Regula and Lukas Reitbauer DAT20
"""


from tensorflow.keras.utils import normalize
import os
import glob
import cv2
import numpy as np
from matplotlib import pyplot as plt

os.chdir(r"D:\datzubabel\Software\datzubabel") 

from Unet.simple_multi_unet_model import multi_unet_model 
# Uses softmax instead of sigmoid
# and gives number of classes

# Resizing images, if needed
SIZE_X = 512 # 512 / 256   # w -> width
SIZE_Y = 256 # 256 / 128    # h -> heigth   
n_classes = 4   # Number of classes for segmentation

# Capture training image info as a list
train_images = []

for directory_path in glob.glob("Unet/images/"):
    for img_path in glob.glob(os.path.join(directory_path, "*.jpg")):
        img = cv2.imread(img_path, 0)
        img = cv2.resize(img, (SIZE_X, SIZE_Y))
        train_images.append(img)

# plt.imshow(train_images[0])

# Convert list to array for machine learning processing
train_images = np.array(train_images)


# Capture mask/label info as a list
train_masks = []
# for directory_path in glob.glob("Unet/colored_masks/"):
for directory_path in glob.glob("Unet/colored_masks/"):
    for mask_path in glob.glob(os.path.join(directory_path, "*.png")):
        mask = cv2.imread(mask_path, cv2.IMREAD_UNCHANGED)
        mask = cv2.resize(mask, (SIZE_X, SIZE_Y), interpolation = cv2.INTER_NEAREST)  #Otherwise ground truth changes due to interpolation
        train_masks.append(mask)


# Convert list to array for machine learning processing
train_masks = np.array(train_masks)

# Review unique labels of masks
np.unique(train_masks)



###############################################
# Encode labels... but multi dim array so need to flatten, encode and reshape
from sklearn.preprocessing import LabelEncoder

labelencoder = LabelEncoder()
n, h, w = train_masks.shape
train_masks_reshaped = train_masks.reshape(-1, 1)
train_masks_reshaped_encoded = labelencoder.fit_transform(train_masks_reshaped)
train_masks_encoded_original_shape = train_masks_reshaped_encoded.reshape(n, h, w)

np.unique(train_masks_encoded_original_shape)

#################################################
train_images = np.expand_dims(train_images, axis=3)
train_images = normalize(train_images, axis=1)

train_masks_input = np.expand_dims(train_masks_encoded_original_shape, axis=3)
# train_masks_input = np.expand_dims(train_masks, axis=3)

# Create a subset of data for testing
# Picking 10% for testing and remaining for training
from sklearn.model_selection import train_test_split

# X1, X_test, y1, y_test = train_test_split(train_images, train_masks_input, test_size=0.05, random_state=0)
# Further split training data t a smaller subset for quick testing of models
# X_train, X_do_not_use, y_train, y_do_not_use = train_test_split(X1, y1, test_size=0.01, random_state=0)
# X_train, X_do_not_use, y_train, y_do_not_use = train_test_split(X1, y1, test_size=0.5, random_state=0)

X_train, X_test, y_train, y_test = train_test_split(train_images, train_masks_input, test_size=0.1, random_state=0)

print("Class values in the dataset are ... ", np.unique(y_train))  # 0 is the background/few unlabeled

# Convert to categorial (kind of the same as One-Hot-Encoding)
from tensorflow.keras.utils import to_categorical

train_masks_cat = to_categorical(y_train, num_classes=n_classes)
y_train_cat = train_masks_cat.reshape((y_train.shape[0], y_train.shape[1], y_train.shape[2], n_classes))

test_masks_cat = to_categorical(y_test, num_classes=n_classes)
y_test_cat = test_masks_cat.reshape((y_test.shape[0], y_test.shape[1], y_test.shape[2], n_classes))

###############################################################
from sklearn.utils import class_weight
 # to balance background in relation to objects 
 # (too much background would else end in unbalanced dataset)
class_weights = class_weight.compute_class_weight(class_weight='balanced',
                                                   classes=np.unique(train_masks_reshaped_encoded),
                                                   y=train_masks_reshaped_encoded)
print("Class weights are...:", class_weights)
# -> was not neccesary

IMG_HEIGHT = X_train.shape[1]
IMG_WIDTH = X_train.shape[2]
IMG_CHANNELS = X_train.shape[3]


def get_model():
    return multi_unet_model(n_classes=n_classes, IMG_HEIGHT=IMG_HEIGHT, IMG_WIDTH=IMG_WIDTH, IMG_CHANNELS=IMG_CHANNELS)

# compile model --> categorical crossentropy as this is not binary data
model = get_model()
model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
model.summary()

# If starting with pre-trained weights.
# model.load_weights('Unet/multiclass_unet_01.hdf5')

# (Train model in Google Colab Pro) -> was actually not working bc Colab is shit
history = model.fit(X_train, y_train_cat,
                    batch_size=32,
                    verbose=1,
                    epochs=50,
                    validation_data=(X_test, y_test_cat),
                    # class_weight=class_weights,
                    shuffle=False)


model.save('Unet/multiclass_unet_04.hdf5') # --> with 512 x 256 epochs = 50 batch size = 32


# model.load_weights("Unet/multiclass_unet_01.hdf5") 
# model.load_weights("Unet/multiclass_unet_02.hdf5")  # --> 256x128
#model.load_weights("Unet/multiclass_unet_03.hdf5")   # --> 512x256
# model.load_weights("Unet/multiclass_unet_04.hdf5")  # with 512 x 256 epochs = 50 batch size = 32



############################################################

# Evaluate model
_, acc = model.evaluate(X_test, y_test_cat)
print("Accuracy is = ", (acc * 100.0), "%")

# plot training and validation accuracy and loss at each epoch
loss = history.history['loss']
val_loss = history.history['val_loss']
epochs = range(1, len(loss) + 1)
plt.plot(epochs, loss, 'y', label='Training loss')
plt.plot(epochs, val_loss, 'r', label='Validation loss')
plt.title('Training and validation loss')
plt.xlabel('Epochs')
plt.ylabel('Loss')
plt.legend()
plt.show()

acc = history.history['accuracy']
val_acc = history.history['val_accuracy']

plt.plot(epochs, acc, 'y', label='Training Accuracy')
plt.plot(epochs, val_acc, 'r', label='Validation Accuracy')
plt.title('Training and validation Accuracy')
plt.xlabel('Epochs')
plt.ylabel('Accuracy')
plt.legend()
plt.show()

############################################################

# Predict on a few images
import random

y_pred = model.predict(X_test)
y_pred_argmax = np.argmax(y_pred, axis=3)

test_img_number = random.randint(0, len(X_test))
test_img = X_test[test_img_number]
ground_truth = y_test[test_img_number]
test_img_norm = test_img[:, :, 0][:, :, None]
test_img_input = np.expand_dims(test_img_norm, 0)
prediction = (model.predict(test_img_input))
predicted_img = np.argmax(prediction, axis=3)[0, :, :]

plt.figure(figsize=(12, 8))
plt.subplot(231)
plt.title('Testing Image')
plt.imshow(test_img[:, :, 0], cmap='gray')
plt.subplot(232)
plt.title('Testing Label')
plt.imshow(ground_truth[:, :, 0], cmap='jet')
plt.subplot(233)
plt.title('Prediction on test image')
plt.imshow(predicted_img, cmap='jet')
plt.show()



########### Testing new pic


new_pic = cv2.imread('Unet/new/new_pic.jpg', 0)

new_pic = cv2.resize(new_pic, (SIZE_X, SIZE_Y))

plt.imshow(new_pic, cmap='gray')

new_pic = np.expand_dims(new_pic, axis=2)
new_pic = np.expand_dims(new_pic, axis=0)


new_pic = normalize(new_pic, axis=1)

new_pic = np.array(new_pic)


np.shape(new_pic)

new_pred = model.predict(new_pic)
new_pic_img =  np.argmax(new_pred, axis=3)[0, :, :]

plt.imshow(new_pic_img, cmap='jet')



#######


# Using built in keras function
from keras.metrics import MeanIoU

n_classes = 4
IOU_keras = MeanIoU(num_classes=n_classes)
IOU_keras.update_state(y_test[:, :, :, 0], y_pred_argmax)
print("Mean IoU =", IOU_keras.result().numpy())

# To calculate I0U for each class...
values = np.array(IOU_keras.get_weights()).reshape(n_classes, n_classes)
print(values)
class1_IoU = values[0, 0] / (
            values[0, 0] + values[0, 1] + values[0, 2] + values[0, 3] + values[1, 0] + values[2, 0] + values[3, 0])
class2_IoU = values[1, 1] / (
            values[1, 1] + values[1, 0] + values[1, 2] + values[1, 3] + values[0, 1] + values[2, 1] + values[3, 1])
class3_IoU = values[2, 2] / (
            values[2, 2] + values[2, 0] + values[2, 1] + values[2, 3] + values[0, 2] + values[1, 2] + values[3, 2])
class4_IoU = values[3, 3] / (
            values[3, 3] + values[3, 0] + values[3, 1] + values[3, 2] + values[0, 3] + values[1, 3] + values[2, 3])

print("IoU for class1 is: ", class1_IoU)
print("IoU for class2 is: ", class2_IoU)
print("IoU for class3 is: ", class3_IoU)
print("IoU for class4 is: ", class4_IoU)

plt.imshow(train_images[0, :, :, 0])
plt.imshow(train_masks[0], cmap='gray')


